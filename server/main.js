import { Meteor } from 'meteor/meteor';
import { Logs } from "../imports/api/_logs.js";
import { DraftDeviations } from "../imports/api/draftDeviations.js";
import { DevsDeclarations } from "../imports/api/devsDeclarations.js";
import { DevsInvestigations } from "../imports/api/devsInvestigations.js";
import { DevsConclusions } from "../imports/api/devsConclusions.js";
import { Processes } from "../imports/api/process.js";
import { Localities } from "../imports/api/localities.js";
import { Equipments } from "../imports/api/equipments.js";
import { Methods } from "../imports/api/methods.js";
import { Products } from "../imports/api/products.js";
import { Objectives } from "../imports/api/objectives.js";
import { CapaDrafts } from "../imports/api/capaDrafts.js";
import { CapaPlanning } from "../imports/api/capaPlanning.js";
import { CapaImplementations } from "../imports/api/capaImplementations.js";
import { CapaReviews } from "../imports/api/capaReviews.js";

import  '../imports/api/dataDevs/categoriesAgg.js';

import  "../imports/api/users.js";
import  "../imports/api/roles.js";

Meteor.startup(() => {
  // code to run on server at startup
  process.env.MAIL_URL = 'smtp://optidevdz:***REMOVED***@smtp.gmail.com:587';
  //process.env.MAIL_URL = 'smtp://optidevdz:***REMOVED***@smtp.gmail.com:587';
});
