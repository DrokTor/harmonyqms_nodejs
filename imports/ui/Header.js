import React, { Component } from 'react';


import { Link } from 'react-router-dom'


import Logout from './Logout.js';


// App component - represents the whole app

export default class App extends Component {





  render() {

    return (

      <div className="row">
        <nav className="main-header navbar navbar-expand-lg col-12  ">
          <a className="offset-1 navbar-brand" href="#">
            <img src=  "/assets/images/logo.png" width="120"  />
          </a>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>

          <div className="float-right collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav offset-8 ">
              <li className="nav-item dropdown">
                <span className="nav-link pointer dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Modules
                </span>
                <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                  {/* <a onClick={()=>{this.props.onClick('deviations')}} className="dropdown-item" href="#">Deviation & CAPA</a> */}
                  {/* <a onClick={()=>{this.props.onClick('administration')}} className="dropdown-item" href="#">Administration</a> */}
                  <Link to="/deviations" className="dropdown-item" >Deviations </Link>
                  <Link to="/capas" className="dropdown-item" >CAPAs </Link>
                  <Link to="/administration" className="dropdown-item" >Administration </Link>

                </div>
              </li>
            </ul>
            <Logout/>
          </div>
        </nav>
      </div>
    );

  }

}
