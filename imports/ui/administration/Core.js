import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';



import User from './User.js';

import Process from './Process.js';
import Locality from './Locality.js';
import Equipment from './Equipment.js';
import Method from './Method.js';
import Product from './Product.js';
import Objectives from './Objectives.js';
import Logs from './Logs.js';
//import Dashboard from './Dashboard.js';
//import Drafts from './Drafts.js';
//import Modules from './Modules.js';



// App component - represents the whole app

export  class Core extends Component {


  constructor(props){
    super(props);

    this.state = {
      page : 'modules',
    }

    this.loadPage = this.loadPage.bind(this);
  }

  loadPage(page){
    this.setState({page});
  }

  render() {

    return (

      <div className="col-12  ">
        {
          (()=>{
            switch (this.props.content) {

              case 'users':
                return <User  />
                break;
              case 'processes':
                return <Process  />
                break;
              case 'localities':
                return <Locality  />
                break;
              case 'equipments':
                return <Equipment  />
                break;
              case 'methods':
                return <Method  />
                break;
              case 'products':
                return <Product  />
                break;
              case 'objectives':
                return <Objectives  />
                break;
              case 'logs':
                return <Logs  />
                break;
              //default:
              //return <Dashboard/>

            }
          })()
        }
      </div>

    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();



return {
user: user,

};
}
)(Core);
