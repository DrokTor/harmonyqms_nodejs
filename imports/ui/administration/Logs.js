import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import Alert from 'react-s-alert';
import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/slide.css';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css' // Import css
//import Anime from 'react-anime';
//import { CSSTransition } from 'react-transition-group';
import {TransitionMotion, spring} from 'react-motion';
import {Link} from 'react-router-dom';

import { Logs } from '../../api/_logs.js';
//import Login from './Login.js';
//import Modules from './Modules.js';



// App component - represents the whole app

export  class LogsInterface extends Component {


  constructor(props){
    super(props);

    this.state = {

    }

   }







  render() {


    return (

      <div className="col-12  ">
        <div className="row">

          <div className="col-8 offset-2">

            <table className="col-12  mt-5 ">
              {/* <thead><tr><th className="col-9">Name</th><th className="col-3"></th></tr></thead> */}
              <tbody>

                {this.props.logs.map((elem,index) => {
                  return <tr key={elem._id}>
                    <td className="col-1">{elem.module}</td>
                    <td className="col-2">{moment(elem.date).format('DD-MM-YYYY | HH:mm')}</td>
                    <td className="col-1">{elem.user.username}</td>
                    <td className="text-center">{elem.info}</td>
                    <td className="col-1">{elem.link ? <Link to={elem.link} target='_blank'><i className="fa fa-external-link-square"></i></Link>:'-'}</td>
                   </tr>;
                })}
              </tbody>
            </table>
           </div>

        </div>
      </div>

    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();

  Meteor.subscribe('logs');
//console.log(_.findWhere(Objectives.find({}).fetch(),{indicator : "Number of deviations"}));
  const logs = Logs.find({},{sort : {date : -1}}).fetch();

return {
user,
logs
};
}
)(LogsInterface);
