import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import Alert from 'react-s-alert';
import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/slide.css';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css' // Import css
//import Anime from 'react-anime';
//import { CSSTransition } from 'react-transition-group';
import {TransitionMotion, spring} from 'react-motion';

import { Localities } from '../../api/localities.js';
//import Login from './Login.js';
//import Modules from './Modules.js';



// App component - represents the whole app

export  class Locality extends Component {


  constructor(props){
    super(props);

    this.state = {
      locality : '',
      updatedLocality : '',
      animationDirection : 'normal',
      in: true,
    }

    this.addLocality = this.addLocality.bind(this);
    this.removeLocality = this.removeLocality.bind(this);
    this.updateLocality = this.updateLocality.bind(this);
    this.closeUpdateLocality = this.closeUpdateLocality.bind(this);
  }

  addLocality(){
    if(this.state.locality!='')
    {
      Meteor.call('locality.insert',this.state.locality,()=>{
        this.setState({locality:''});
      });
    }
    else{
      Alert.error("Locality should not be empty !");
    }
  }

  updateInterface(updatedLocality){

    this.setState({updatedLocality});
  }

  updateLocality(){
    if(this.state.updatedLocality.name!='')
    {
      Meteor.call('locality.update',this.state.updatedLocality,()=>{
        //this.setState({animationDirection:'reverse'});
        this.setState({updatedLocality:''});
        Alert.success("Locality updated.");
      });
    }
    else{
      Alert.error("Locality should not be empty !");
    }
  }

  closeUpdateLocality(){
    this.setState({updatedLocality:''});
    Alert.info("update canceled.");
  }

  removeLocality(localityID){

    confirmAlert({
      title: 'Confirm to delete',
      message: 'Are you sure you want to delete this item ?',
      buttons: [
        {
          label: 'Yes',
          onClick: () => {

            Meteor.call('locality.remove',localityID,()=>{
              Alert.success("Item deleted.");
            });
          }
        },
        {
          label: 'No',
          onClick: () => {
            Alert.info("Canceled.");
          }
        }
      ]
    })

  }

  render() {


    return (

      <div className="col-12  ">
        <div className="row">

          {
             this.state.updatedLocality &&


            <div

              className="col-8 offset-2 mt-5">
              <label className="col-10">Update Locality</label>
              <input
                className="col-10 ml-3"
                type = "text"
                value = {this.state.updatedLocality.name}
                onChange={ (event) =>  { let updatedLocality = {...this.state.updatedLocality}
                      updatedLocality.name = event.target.value;
                      this.setState({updatedLocality})
                    }}
                onKeyPress={ e => { e.key==='Enter' ? this.updateLocality() : ''} }
              />
              <button className="btn btn-light" onClick={ this.updateLocality }>Update</button>
              <button className="btn btn-light" onClick={ this.closeUpdateLocality }><i className="fa fa-times"></i></button>
            </div>

           }
          <div className="col-8 offset-2">
            <div className="col-8 offset-2 mt-5">
              <label className="col-10">Add a Locality</label>
              <input
                className="col-10 ml-3"
                type = "text"
                value = {this.state.locality}
                onChange={ event =>  this.setState({locality : event.target.value}) }
                onKeyPress={ (e)=>{ if(e.key === 'Enter') this.addLocality()} }
              />
              <button className="btn btn-light" onClick={ this.addLocality }><i className="fa  fa-plus"></i></button>
            </div>
            <table className="col-12  mt-5 ">
              <thead><tr><th className="col-9">Name</th><th className="col-3"></th></tr></thead>
              <tbody>
            {
              (() => {return this.props.localities.map((locality,index)=>{
                return <tr key={index} className=""   >
                   <td className="col-6">{locality.name}</td>
                   <td className="float-right">
                     <button className="btn btn-light" onClick={ () => {this.updateInterface(locality) } }><i className="fa fa-pencil"></i></button>
                     <button className="btn btn-light" onClick={ () => {this.removeLocality(locality._id) } }><i className="fa fa-trash"></i></button>
                   </td></tr>
              })})()
            }
            </tbody>
            </table>
           </div>
        </div>
      </div>

    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();

  Meteor.subscribe('locality');

return {
user: user,
localities:  Localities.find({}).fetch(),
};
}
)(Locality);
