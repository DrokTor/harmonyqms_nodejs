import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import Alert from 'react-s-alert';
import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/slide.css';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css' // Import css
//import Anime from 'react-anime';
//import { CSSTransition } from 'react-transition-group';
import {TransitionMotion, spring} from 'react-motion';

import { Methods } from '../../api/methods.js';
//import Login from './Login.js';
//import Modules from './Modules.js';



// App component - represents the whole app

export  class Method extends Component {


  constructor(props){
    super(props);

    this.state = {
      method : '',
      updatedMethod : '',
      animationDirection : 'normal',
      in: true,
    }

    this.addMethod = this.addMethod.bind(this);
    this.removeMethod = this.removeMethod.bind(this);
    this.updateMethod = this.updateMethod.bind(this);
    this.closeUpdateMethod = this.closeUpdateMethod.bind(this);
  }

  addMethod(){
    if(this.state.method!='')
    {
      Meteor.call('method.insert',this.state.method,()=>{
        this.setState({method:''});
      });
    }
    else{
      Alert.error("Method should not be empty !");
    }
  }

  updateInterface(updatedMethod){

    this.setState({updatedMethod});
  }

  updateMethod(){
    if(this.state.updatedMethod.name!='')
    {
      Meteor.call('method.update',this.state.updatedMethod,()=>{
        //this.setState({animationDirection:'reverse'});
        this.setState({updatedMethod:''});
        Alert.success("Method updated.");
      });
    }
    else{
      Alert.error("Method should not be empty !");
    }
  }

  closeUpdateMethod(){
    this.setState({updatedMethod:''});
    Alert.info("update canceled.");
  }

  removeMethod(methodID){

    confirmAlert({
      title: 'Confirm to delete',
      message: 'Are you sure you want to delete this item ?',
      buttons: [
        {
          label: 'Yes',
          onClick: () => {

            Meteor.call('method.remove',methodID,()=>{
              Alert.success("Item deleted.");
            });
          }
        },
        {
          label: 'No',
          onClick: () => {
            Alert.info("Canceled.");
          }
        }
      ]
    })

  }

  render() {


    return (

      <div className="col-12  ">
        <div className="row">

          {
             this.state.updatedMethod &&


            <div

              className="col-8 offset-2 mt-5">
              <label className="col-10">Update Method</label>
              <input
                className="col-10 ml-3"
                type = "text"
                value = {this.state.updatedMethod.name}
                onChange={ (event) =>  { let updatedMethod = {...this.state.updatedMethod}
                      updatedMethod.name = event.target.value;
                      this.setState({updatedMethod})
                    }}
                onKeyPress={ e => { e.key==='Enter' ? this.updateMethod() : ''} }
              />
              <button className="btn btn-light" onClick={ this.updateMethod }>Update</button>
              <button className="btn btn-light" onClick={ this.closeUpdateMethod }><i className="fa fa-times"></i></button>
            </div>

           }
          <div className="col-8 offset-2">
            <div className="col-8 offset-2 mt-5">
              <label className="col-10">Add a Method</label>
              <input
                className="col-10 ml-3"
                type = "text"
                value = {this.state.method}
                onChange={ event =>  this.setState({method : event.target.value}) }
                onKeyPress={ (e)=>{ if(e.key === 'Enter') this.addMethod()} }
              />
              <button className="btn btn-light" onClick={ this.addMethod }><i className="fa  fa-plus"></i></button>
            </div>
            <table className="col-12  mt-5 ">
              <thead><tr><th className="col-9">Name</th><th className="col-3"></th></tr></thead>
              <tbody>
            {
              (() => {return this.props.methods.map((method,index)=>{
                return <tr key={index} className=""   >
                   <td className="col-6">{method.name}</td>
                   <td className="float-right">
                     <button className="btn btn-light" onClick={ () => {this.updateInterface(method) } }><i className="fa fa-pencil"></i></button>
                     <button className="btn btn-light" onClick={ () => {this.removeMethod(method._id) } }><i className="fa fa-trash"></i></button>
                   </td></tr>
              })})()
            }
            </tbody>
            </table>
           </div>
        </div>
      </div>

    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();

  Meteor.subscribe('method');

return {
user: user,
methods:  Methods.find({}).fetch(),
};
}
)(Method);
