import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import Alert from 'react-s-alert';
import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/slide.css';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css' // Import css
//import Anime from 'react-anime';
//import { CSSTransition } from 'react-transition-group';
import {TransitionMotion, spring} from 'react-motion';

import { Products } from '../../api/products.js';
//import Login from './Login.js';
//import Modules from './Modules.js';



// App component - represents the whole app

export  class Product extends Component {


  constructor(props){
    super(props);

    this.state = {
      product : '',
      updatedProduct : '',
      animationDirection : 'normal',
      in: true,
    }

    this.addProduct = this.addProduct.bind(this);
    this.removeProduct = this.removeProduct.bind(this);
    this.updateProduct = this.updateProduct.bind(this);
    this.closeUpdateProduct = this.closeUpdateProduct.bind(this);
  }

  addProduct(){
    if(this.state.product!='')
    {
      Meteor.call('product.insert',this.state.product,()=>{
        this.setState({product:''});
      });
    }
    else{
      Alert.error("Product should not be empty !");
    }
  }

  updateInterface(updatedProduct){

    this.setState({updatedProduct});
  }

  updateProduct(){
    if(this.state.updatedProduct.name!='')
    {
      Meteor.call('product.update',this.state.updatedProduct,()=>{
        //this.setState({animationDirection:'reverse'});
        this.setState({updatedProduct:''});
        Alert.success("Product updated.");
      });
    }
    else{
      Alert.error("Product should not be empty !");
    }
  }

  closeUpdateProduct(){
    this.setState({updatedProduct:''});
    Alert.info("update canceled.");
  }

  removeProduct(productID){

    confirmAlert({
      title: 'Confirm to delete',
      message: 'Are you sure you want to delete this item ?',
      buttons: [
        {
          label: 'Yes',
          onClick: () => {

            Meteor.call('product.remove',productID,()=>{
              Alert.success("Item deleted.");
            });
          }
        },
        {
          label: 'No',
          onClick: () => {
            Alert.info("Canceled.");
          }
        }
      ]
    })

  }

  render() {


    return (

      <div className="col-12  ">
        <div className="row">

          {
             this.state.updatedProduct &&


            <div

              className="col-8 offset-2 mt-5">
              <label className="col-10">Update Product</label>
              <input
                className="col-10 ml-3"
                type = "text"
                value = {this.state.updatedProduct.name}
                onChange={ (event) =>  { let updatedProduct = {...this.state.updatedProduct}
                      updatedProduct.name = event.target.value;
                      this.setState({updatedProduct})
                    }}
                onKeyPress={ e => { e.key==='Enter' ? this.updateProduct() : ''} }
              />
              <button className="btn btn-light" onClick={ this.updateProduct }>Update</button>
              <button className="btn btn-light" onClick={ this.closeUpdateProduct }><i className="fa fa-times"></i></button>
            </div>

           }
          <div className="col-8 offset-2">
            <div className="col-8 offset-2 mt-5">
              <label className="col-10">Add a Product</label>
              <input
                className="col-10 ml-3"
                type = "text"
                value = {this.state.product}
                onChange={ event =>  this.setState({product : event.target.value}) }
                onKeyPress={ (e)=>{ if(e.key === 'Enter') this.addProduct()} }
              />
              <button className="btn btn-light" onClick={ this.addProduct }><i className="fa  fa-plus"></i></button>
            </div>
            <table className="col-12  mt-5 ">
              <thead><tr><th className="col-9">Name</th><th className="col-3"></th></tr></thead>
              <tbody>
            {
              (() => {return this.props.products.map((product,index)=>{
                return <tr key={index} className=""   >
                   <td className="col-6">{product.name}</td>
                   <td className="float-right">
                     <button className="btn btn-light" onClick={ () => {this.updateInterface(product) } }><i className="fa fa-pencil"></i></button>
                     <button className="btn btn-light" onClick={ () => {this.removeProduct(product._id) } }><i className="fa fa-trash"></i></button>
                   </td></tr>
              })})()
            }
            </tbody>
            </table>
           </div>
        </div>
      </div>

    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();

  Meteor.subscribe('product');

return {
user: user,
products:  Products.find({}).fetch(),
};
}
)(Product);
