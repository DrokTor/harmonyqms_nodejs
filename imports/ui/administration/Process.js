import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import Alert from 'react-s-alert';
import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/slide.css';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css' // Import css
//import Anime from 'react-anime';
//import { CSSTransition } from 'react-transition-group';
import {TransitionMotion, spring} from 'react-motion';

import { Processes } from '../../api/process.js';
//import Login from './Login.js';
//import Modules from './Modules.js';



// App component - represents the whole app

export  class Process extends Component {


  constructor(props){
    super(props);

    this.state = {
      process : '',
      updatedProcess : '',
      animationDirection : 'normal',
      in: true,
    }

    this.addProcess = this.addProcess.bind(this);
    this.removeProcess = this.removeProcess.bind(this);
    this.updateProcess = this.updateProcess.bind(this);
    this.closeUpdateProcess = this.closeUpdateProcess.bind(this);
  }

  addProcess(){
    if(this.state.process!='')
    {
      Meteor.call('process.insert',this.state.process,()=>{
        this.setState({process:''});
      });
    }
    else{
      Alert.error("Process should not be empty !");
    }
  }

  updateInterface(updatedProcess){

    this.setState({updatedProcess});
  }

  updateProcess(){
    if(this.state.updatedProcess.name!='')
    {
      Meteor.call('process.update',this.state.updatedProcess,()=>{
        //this.setState({animationDirection:'reverse'});
        this.setState({updatedProcess:''});
        Alert.success("Process updated.");
      });
    }
    else{
      Alert.error("Process should not be empty !");
    }
  }

  closeUpdateProcess(){
    this.setState({updatedProcess:''});
    Alert.info("update canceled.");
  }

  removeProcess(processID){

    confirmAlert({
      title: 'Confirm to delete',
      message: 'Are you sure you want to delete this item ?',
      buttons: [
        {
          label: 'Yes',
          onClick: () => {

            Meteor.call('process.remove',processID,()=>{
              Alert.success("Item deleted.");
            });
          }
        },
        {
          label: 'No',
          onClick: () => {
            Alert.info("Canceled.");
          }
        }
      ]
    })

  }

  render() {


    return (

      <div className="col-12  ">
        <div className="row">

          {
             this.state.updatedProcess &&


            <div

              className="col-8 offset-2 mt-5">
              <label className="col-10">Update Process</label>
              <input
                className="col-10 ml-3"
                type = "text"
                value = {this.state.updatedProcess.name}
                onChange={ (event) =>  { let updatedProcess = {...this.state.updatedProcess}
                      updatedProcess.name = event.target.value;
                      this.setState({updatedProcess})
                    }}
                onKeyPress={ e => { e.key==='Enter' ? this.updateProcess() : ''} }
              />
              <button className="btn btn-light" onClick={ this.updateProcess }>Update</button>
              <button className="btn btn-light" onClick={ this.closeUpdateProcess }><i className="fa fa-times"></i></button>
            </div>

           }
          <div className="col-8 offset-2">
            <div className="col-8 offset-2 mt-5">
              <label className="col-10">Add a Process</label>
              <input
                className="col-10 ml-3"
                type = "text"
                value = {this.state.process}
                onChange={ event =>  this.setState({process : event.target.value}) }
                onKeyPress={ (e)=>{ if(e.key === 'Enter') this.addProcess()} }
              />
              <button className="btn btn-light" onClick={ this.addProcess }><i className="fa  fa-plus"></i></button>
            </div>
            <table className="col-12  mt-5 ">
              <thead><tr><th className="col-9">Name</th><th className="col-3"></th></tr></thead>
              <tbody>
            {
              (() => {return this.props.processes.map((process,index)=>{
                return <tr key={index} className=""   >
                   <td className="col-6">{process.name}</td>
                   <td className="float-right">
                     <button className="btn btn-light" onClick={ () => {this.updateInterface(process) } }><i className="fa fa-pencil"></i></button>
                     <button className="btn btn-light" onClick={ () => {this.removeProcess(process._id) } }><i className="fa fa-trash"></i></button>
                   </td></tr>
              })})()
            }
            </tbody>
            </table>
           </div>
        </div>
      </div>

    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();

  Meteor.subscribe('process');

return {
user: user,
processes:  Processes.find({}).fetch(),
};
}
)(Process);
