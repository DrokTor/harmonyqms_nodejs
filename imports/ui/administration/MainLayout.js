import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';

import { Route,Switch,Link,Redirect } from 'react-router-dom'


import Header from './Header.js';
import Core from './Core.js';
import Logs from './Logs.js';
import Objectives from './Objectives.js';
import Product from './Product.js';
import Locality from './Locality.js';
import Equipment from './Equipment.js';
import Process from './Process.js';
import Method from './Method.js';
import User from './User.js';
// import Modules from './Modules.js';



// App component - represents the whole app

export  class MainLayout extends Component {


  constructor(props){
    super(props);

    this.state = {
      content : 'processes',
    }

    this.loadContent = this.loadContent.bind(this);
  }

  loadContent(content){
    this.setState({content});
  }

  render() {

    return (

      <div className="row ">
        <Header onClick={ this.loadContent }/>
        {/* <Core onClick={ this.loadContent } content={this.state.content}    /> */}
        <Switch>
          {/* <Route exact path="/deviations/" component={Dashboard}/> */}
          <Route path="/administration/logs" component={Logs}/>
          <Route path="/administration/objectives" component={Objectives}/>
          <Route path="/administration/products" component={Product}/>
          <Route path="/administration/localities" component={Locality}/>
          <Route path="/administration/equipments" component={Equipment}/>
          <Route path="/administration/processes" component={Process}/>
          <Route path="/administration/methods" component={Method}/>
          <Route path="/administration/users" component={User}/>
          <Route
            path='/administration/'
            render={(props) => <Core {...props} onClick={ this.loadContent } content={this.state.content}  />}
          />
        {/* <Core onClick={ this.loadContent } content={this.state.content} deviation={this.state.deviation} deviationId={this.state.deviationId}   /> */}
        </Switch>
      </div>

    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();

return {
user: user,
};
}
)(MainLayout);
