import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import Alert from 'react-s-alert';
import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/slide.css';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css' // Import css
//import Anime from 'react-anime';
//import { CSSTransition } from 'react-transition-group';
import {TransitionMotion, spring} from 'react-motion';

import { Equipments } from '../../api/equipments.js';
//import Login from './Login.js';
//import Modules from './Modules.js';



// App component - represents the whole app

export  class Equipment extends Component {


  constructor(props){
    super(props);

    this.state = {
      equipment : '',
      updatedEquipment : '',
      animationDirection : 'normal',
      in: true,
    }

    this.addEquipment = this.addEquipment.bind(this);
    this.removeEquipment = this.removeEquipment.bind(this);
    this.updateEquipment = this.updateEquipment.bind(this);
    this.closeUpdateEquipment = this.closeUpdateEquipment.bind(this);
  }

  addEquipment(){
    if(this.state.equipment!='')
    {
      Meteor.call('equipment.insert',this.state.equipment,()=>{
        this.setState({equipment:''});
      });
    }
    else{
      Alert.error("Equipment should not be empty !");
    }
  }

  updateInterface(updatedEquipment){

    this.setState({updatedEquipment});
  }

  updateEquipment(){
    if(this.state.updatedEquipment.name!='')
    {
      Meteor.call('equipment.update',this.state.updatedEquipment,()=>{
        //this.setState({animationDirection:'reverse'});
        this.setState({updatedEquipment:''});
        Alert.success("Equipment updated.");
      });
    }
    else{
      Alert.error("Equipment should not be empty !");
    }
  }

  closeUpdateEquipment(){
    this.setState({updatedEquipment:''});
    Alert.info("update canceled.");
  }

  removeEquipment(equipmentID){

    confirmAlert({
      title: 'Confirm to delete',
      message: 'Are you sure you want to delete this item ?',
      buttons: [
        {
          label: 'Yes',
          onClick: () => {

            Meteor.call('equipment.remove',equipmentID,()=>{
              Alert.success("Item deleted.");
            });
          }
        },
        {
          label: 'No',
          onClick: () => {
            Alert.info("Canceled.");
          }
        }
      ]
    })

  }

  render() {


    return (

      <div className="col-12  ">
        <div className="row">

          {
             this.state.updatedEquipment &&


            <div

              className="col-8 offset-2 mt-5">
              <label className="col-10">Update Equipment</label>
              <input
                className="col-10 ml-3"
                type = "text"
                value = {this.state.updatedEquipment.name}
                onChange={ (event) =>  { let updatedEquipment = {...this.state.updatedEquipment}
                      updatedEquipment.name = event.target.value;
                      this.setState({updatedEquipment})
                    }}
                onKeyPress={ e => { e.key==='Enter' ? this.updateEquipment() : ''} }
              />
              <button className="btn btn-light" onClick={ this.updateEquipment }>Update</button>
              <button className="btn btn-light" onClick={ this.closeUpdateEquipment }><i className="fa fa-times"></i></button>
            </div>

           }
          <div className="col-8 offset-2">
            <div className="col-8 offset-2 mt-5">
              <label className="col-10">Add an Equipment</label>
              <input
                className="col-10 ml-3"
                type = "text"
                value = {this.state.equipment}
                onChange={ event =>  this.setState({equipment : event.target.value}) }
                onKeyPress={ (e)=>{ if(e.key === 'Enter') this.addEquipment()} }
              />
              <button className="btn btn-light" onClick={ this.addEquipment }><i className="fa  fa-plus"></i></button>
            </div>
            <table className="col-12  mt-5 ">
              <thead><tr><th className="col-9">Name</th><th className="col-3"></th></tr></thead>
              <tbody>
            {
              (() => {return this.props.equipmentes.map((equipment,index)=>{
                return <tr key={index} className=""   >
                   <td className="col-6">{equipment.name}</td>
                   <td className="float-right">
                     <button className="btn btn-light" onClick={ () => {this.updateInterface(equipment) } }><i className="fa fa-pencil"></i></button>
                     <button className="btn btn-light" onClick={ () => {this.removeEquipment(equipment._id) } }><i className="fa fa-trash"></i></button>
                   </td></tr>
              })})()
            }
            </tbody>
            </table>
           </div>
        </div>
      </div>

    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();

  Meteor.subscribe('equipment');

return {
user: user,
equipmentes:  Equipments.find({}).fetch(),
};
}
)(Equipment);
