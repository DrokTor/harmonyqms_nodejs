import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import Alert from 'react-s-alert';
import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/slide.css';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css' // Import css
//import Anime from 'react-anime';
//import { CSSTransition } from 'react-transition-group';
import {TransitionMotion, spring} from 'react-motion';

import { Objectives } from '../../api/objectives.js';
//import Login from './Login.js';
//import Modules from './Modules.js';



// App component - represents the whole app

export  class ObjetivesInterface extends Component {


  constructor(props){
    super(props);

    this.state = {
      product : '',
      updatedObjetives : '',
      animationDirection : 'normal',
      in: true,
    }

    this.updateObj = this.updateObj.bind(this);
  }



  updateObj(e,indicator){

       Meteor.call('objectives.update',indicator,e.target.value,(err,res)=>{
         //this.setState({animationDirection:'reverse'});
         if(res){
           Alert.success("Objetives updated.");
         }
       });
       if (e.key === 'Enter') {
     }


  }



  render() {


    return (

      <div className="col-12  ">
        <div className="row">

          {
             this.props.maxDevs &&



          <div className="col-8 offset-2">
            {/* <div className="col-8 offset-2 mt-5">
              <label className="col-10">Add a Objetives</label>
              <input
                className="col-10 ml-3"
                type = "text"
                value = {this.state.product}
                onChange={ event =>  this.setState({product : event.target.value}) }
                onKeyPress={ (e)=>{ if(e.key === 'Enter') this.addObjetives()} }
              />
              <button className="btn btn-light" onClick={ this.addObjetives }><i className="fa  fa-plus"></i></button>
            </div> */}
            <table className="col-12  mt-5 ">
              <thead><tr><th className="col-9">Name</th><th className="col-3"></th></tr></thead>
              <tbody>
                {/* <tr><td>Number of deviations</td><td><input type="number" value={this.props.maxDevs.obj} onChange = {e => this.updateObj(e, this.props.maxDevs.indicator)  }/></td></tr>
                <tr><td>Closing rate</td><td><input type="number" value={this.props.closingRt.obj} onChange = {e => this.updateObj(e, this.props.closingRt.indicator)  }/></td></tr>
                <tr><td>Average closing time</td><td><input type="number" value={this.props.avgClosing.obj} onChange = {e => this.updateObj(e, this.props.avgClosing.indicator)  }/></td></tr>
                <tr><td>Deviations overdues</td><td><input type="number" value={this.props.devsOver.obj} onChange = {e => this.updateObj(e, this.props.devsOver.indicator)  }/></td></tr>
                <tr><td>Number of CAPAs</td><td><input type="number" value={this.props.maxCAPAs.obj} onChange = {e => this.updateObj(e, this.props.maxCAPAs.indicator)  }/></td></tr> */}

                {this.props.objectives.map((elem,index) => {
                  return <tr key={elem._id}><td>{elem.indicator}</td><td><input type="number" value={elem.obj} onChange = {e => this.updateObj(e, elem.indicator)  }/></td></tr>;
                })}
              </tbody>
            </table>
           </div>
              }
        </div>
      </div>

    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();

  Meteor.subscribe('objectives');
//console.log(_.findWhere(Objectives.find({}).fetch(),{indicator : "Number of deviations"}));
  const objectives = Objectives.find({}).fetch();

return {
user,
objectives,
maxDevs:  _.findWhere(objectives,{indicator : "Number of deviations"}),
closingRt:  _.findWhere(objectives,{indicator : "Closing rate"}),
avgClosing:  _.findWhere(objectives,{indicator : "Average closing time"}),
devsOver:  _.findWhere(objectives,{indicator : "Deviations overdues"}),
maxCAPAs:  _.findWhere(objectives,{indicator : "Number of CAPAs"}),
};
}
)(ObjetivesInterface);
