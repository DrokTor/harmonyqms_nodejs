import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import Alert from 'react-s-alert';
import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/slide.css';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css' // Import css
//import Anime from 'react-anime';
//import { CSSTransition } from 'react-transition-group';
import {TransitionMotion, spring} from 'react-motion';

import { Processes } from '../../api/process.js';
//import Login from './Login.js';
//import Modules from './Modules.js';



// App component - represents the whole app

export  class User extends Component {


  constructor(props){
    super(props);

    this.state = {
      content: "usersList",
      username: '',
      email: '',
      user:'',
    }

    this.addAccount = this.addAccount.bind(this);
    this.removeProcess = this.removeProcess.bind(this);
    this.updateProcess = this.updateProcess.bind(this);
    this.closeUpdateProcess = this.closeUpdateProcess.bind(this);
    this.showRoles = this.showRoles.bind(this);
    this.checkRole = this.checkRole.bind(this);
  }

  addAccount(){
    if(this.state.username.trim()!='' && this.state.email.trim()!='')
    {
      Meteor.call('user.create',{username: this.state.username,email: this.state.email},(err,res)=>{

        if(err){
          Alert.error(err.reason);
        }else {
          this.setState({content:"usersList"});
          Alert.success("Account created.");
        }

      });
    }
    else{
      Alert.error("Username and Email should not be empty !");
    }
  }

  updateInterface(updatedProcess){

    this.setState({updatedProcess});
  }

  updateProcess(){
    if(this.state.updatedProcess.name!='')
    {
      Meteor.call('process.update',this.state.updatedProcess,()=>{
        //this.setState({animationDirection:'reverse'});
        this.setState({updatedProcess:''});
        Alert.success("Process updated.");
      });
    }
    else{
      Alert.error("Process should not be empty !");
    }
  }

  closeUpdateProcess(){
    this.setState({updatedProcess:''});
    Alert.info("update canceled.");
  }

  removeProcess(processID){

    confirmAlert({
      title: 'Confirm to delete',
      message: 'Are you sure you want to delete this item ?',
      buttons: [
        {
          label: 'Yes',
          onClick: () => {

            Meteor.call('process.remove',processID,()=>{
              Alert.success("Item deleted.");
            });
          }
        },
        {
          label: 'No',
          onClick: () => {
            Alert.info("Canceled.");
          }
        }
      ]
    })

  }


  showRoles(user){
    this.setState({content:'userRoles',user: user});
  }

  checkRole(role){
    let roles = Roles.getRolesForUser(this.state.user._id);
    //console.log(roles[0])
    //console.log(this.state.user)

    if(Roles.userIsInRole(this.state.user._id,role)){
      roles.splice(_.indexOf(roles, role),1);
      //console.log('spliced');
    }else {
      roles.push(role);
      //console.log('pushed');
    }
    //console.log(roles)
    Meteor.call('role.set',this.state.user._id,roles);
    //Roles.setUserRoles(this.state.user,roles);
  }


  render() {

    return (

      <div className="col-12  ">
        <div className="row">

          <div className="col-8 offset-2">

            {( ()=>
              {
              switch (this.state.content) {
              case "createAccount":

              return(
              <div className="col-8 offset-2 mt-5">
                <h3 className="col-10">Create a new account</h3>
                <label className="col-10 mt-3">Username</label>
                <input
                  className="col-10 ml-3"
                  type = "text"
                  value = {this.state.username}
                  onChange={ event =>  this.setState({username : event.target.value}) }
                  //onKeyPress={ (e)=>{ if(e.key === 'Enter') this.addAccount()} }
                />
                <label className="col-10 mt-3">Email</label>
                <input
                  className="col-10 ml-3"
                  type = "text"
                  value = {this.state.email}
                  onChange={ event =>  this.setState({email : event.target.value}) }
                  //onKeyPress={ (e)=>{ if(e.key === 'Enter') this.addAccount()} }
                />
                <button className="col-3 offset-7  mt-3 btn btn-light " onClick={ this.addAccount }>Create <i className="fa  fa-plus"></i></button>
            </div>)
            break;
           case "usersList":
            return(
            <div className="col-12" >
              <button className="btn btn-light" onClick={()=>this.setState({content : "createAccount"})} >Create a new Account</button>
              <table className="col-12  mt-5 ">
                <thead><tr><th className="col-9">Name</th><th className="col-3"></th></tr></thead>
                <tbody>
                  {
                    (() => {return this.props.users.map((user,index)=>{
                      return <tr key={index} className=""   >
                        <td className="col-6">{user.username}</td>
                        <td className="float-right">
                          <button className="btn btn-light" onClick={ () => {this.updateInterface(user) } }><i className="fa fa-pencil"></i></button>
                          <button className="btn btn-light" onClick={ () => {this.removeProcess(user._id) } }><i className="fa fa-trash"></i></button>
                          <button className="btn btn-light" onClick={ () => {this.showRoles(user) } }><i className="fa fa-lock"></i></button>
                        </td></tr>
                      })})()
                    }
                  </tbody>
                </table>
              </div>)
              break;
              case "userRoles":
              return(
              <div className="col-12" >
                <button className="col-1 btn btn-light" onClick={()=>this.setState({content : "usersList"})} > <i className="fa fa-long-arrow-left"></i> </button>
                <div className="row  mt-5 ">
                   <h3 className="col-12 mb-3">Roles</h3>

                    {
                      (() => {
                        const roles = Roles.getRolesForUser(this.state.user);
                        return Roles.getAllRoles().map((role,index)=>{
                          return <div key={index} className="pointer user-role col-4" onClick={ ()=> this.checkRole(role.name) }>{role.name}<i className={Roles.userIsInRole(this.state.user._id,role.name)? "col-2 fa fa-check-square-o fa-2x  my-green  float-right " : " float-right col-2 fa fa-square-o fa-2x  "} /></div>

                          }
                      )})()
                      }
                  </div>
                </div>)
                break;
            }
          }
          )()
          }
           </div>
        </div>
      </div>

    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();

  //console.log(Meteor.users.find({}).fetch());
Meteor.subscribe('users');
//console.log(Roles.getAllRoles().fetch());
//console.log(Roles.getUsersInRole('admin-all').fetch());

return {
user: user,
users: Meteor.users.find({}).fetch(),
//roles: Roles.getAllRoles().fetch(),
};
}
)(User);
