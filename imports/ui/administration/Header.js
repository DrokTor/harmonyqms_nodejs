import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';

import {  Link  } from 'react-router-dom'


//import Login from './Login.js';
//import Modules from './Modules.js';



// App component - represents the whole app

export  class Header extends Component {


  constructor(props){
    super(props);

    this.state = {
      page : 'modules',
    }

    this.loadPage = this.loadPage.bind(this);
  }

  loadPage(page){
    this.setState({page});
  }

  render() {

    return (

      <div className="col-12 admin-header ">
        <div className="row">
          <nav className="navbar navbar-expand-lg col-10 offset-1  ">
          <span className="col-3 ">Administration</span>
          <div className="col-10 row ">
            <div className=" collapse navbar-collapse col-3 offset-4" id="navbarSupportedContent">
              <ul className="navbar-nav   ">
                <li className="nav-item dropdown">
                  <span className="nav-link pointer dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Accounts management
                  </span>
                  <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                    <Link to="/administration/users" className="no-decor" ><span   className="pointer dropdown-item" href="#">Users</span></Link>
                    {/* <Link to="/administration/roles" className="no-decor" ><span   className="pointer dropdown-item" href="#">Roles</span></Link> */}

                  </div>
                </li>
              </ul>
            </div>
            <div className="  collapse navbar-collapse col-3 " id="navbarSupportedContent">
              <ul className="navbar-nav   ">
                <li className="nav-item dropdown">
                  <span className="nav-link pointer dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Global configuration
                  </span>
                  <div className="dropdown-menu" aria-labelledby="navbarDropdown">

                    <Link to="/administration/products" className="no-decor" ><span   className="pointer dropdown-item" href="#">Products</span></Link>
                    <Link to="/administration/processes" className="no-decor" ><span   className="pointer dropdown-item" href="#">Processes</span></Link>
                    <Link to="/administration/localities" className="no-decor" ><span   className="pointer dropdown-item" href="#">Localities</span></Link>
                    <Link to="/administration/methods" className="no-decor" ><span   className="pointer dropdown-item" href="#">Methods</span></Link>
                    <Link to="/administration/equipments" className="no-decor" ><span   className="pointer dropdown-item" href="#">Equipments</span></Link>

                  </div>
                </li>
              </ul>
            </div>
            <div className="  collapse navbar-collapse col-2 " id="navbarSupportedContent">
              <ul className="navbar-nav   ">
                <li className="nav-item dropdown">
                  <span className="nav-link pointer dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Tracking
                  </span>
                  <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                    {/* <span onClick={()=>{this.props.onClick('objectives')}} className="pointer dropdown-item" href="#">Objectives</span> */}
                    <Link to="/administration/objectives" className="no-decor" ><span /*onClick={()=>{this.props.onClick('logs')}}*/ className="pointer dropdown-item" href="#">Objectives</span></Link>
                    <Link to="/administration/logs" className="no-decor" ><span /*onClick={()=>{this.props.onClick('logs')}}*/ className="pointer dropdown-item" href="#">Logs</span></Link>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </nav>

        </div>
      </div>

    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();

  Meteor.subscribe('draftDeviations');


return {
user: user,
//drafts:  DraftDeviations.find({}).count(),
};
}
)(Header);
