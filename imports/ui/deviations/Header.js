import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';

import { Link } from 'react-router-dom';

import {DraftDeviations} from '../../api/draftDeviations.js';
import {DevsDeclarations} from '../../api/devsDeclarations.js';
//import Login from './Login.js';
//import Modules from './Modules.js';



// App component - represents the whole app

export  class Header extends Component {


  constructor(props){
    super(props);

    this.state = {
      page : 'modules',
    }

    this.loadPage = this.loadPage.bind(this);
  }

  loadPage(page){
    this.setState({page});
  }

  render() {

    return (

      <div className="col-12 deviations-header ">
        <div className="row">
          <span className="col-3 offset-1">Deviation management</span>
          <div className="col-5 offset-3">
            {this.props.drafts? <Link to="/deviations/drafts"><button className="btn "   >Drafts <span className="badge badge-secondary">{this.props.drafts}</span></button></Link> : ''}
            {this.props.incomplete? <Link to="/deviations/incompletes"><button className="btn ml-1"   >Incompletes <span className="badge badge-secondary">{this.props.incomplete}</span></button></Link> : ''}
            {this.props.sent? <Link to="/deviations/sents"><button className="btn ml-1"   >Sents <span className="badge badge-secondary">{this.props.sent}</span></button></Link> : ''}
            {this.props.approvals? <Link to="/deviations/approvals"><button className="btn ml-1"   >Approvals <span className="badge badge-secondary">{this.props.approvals}</span></button></Link> : ''}
            {this.props.expertises? <Link to={"/deviations/list?state=Declaration&expertise.expertId="+Meteor.userId()}><button className="btn ml-1"     >Expertises <span className="badge badge-secondary">{this.props.expertises}</span></button></Link> : ''}
            {this.props.investigations? <Link to={"/deviations/list?state=Investigation&investigation.investigatorId="+Meteor.userId()}><button className="btn ml-1"  >Investigations <span className="badge badge-secondary">{this.props.investigations}</span></button></Link> : ''}
            {/* <Link to={"/capas/list?state=Planned&implementation.operatorId="+Meteor.userId()}></Link> */}
          </div>
        </div>
      </div>

    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();

  Meteor.subscribe('draftDeviations');
  Meteor.subscribe('devsDeclarations');

  if( Roles.userIsInRole(user,['approve-deviation']) )
  {
    Meteor.subscribe('approveDeviations');
  }

  if( Roles.userIsInRole(user,['launch-investigation']) )
  {
    Meteor.subscribe('expertises');
  }


  //console.log(DevsDeclarations.find({'expertise.expertId' : Meteor.userId(),state: 'Declaration'}).count());
//console.log(Roles.userIsInRole(user,['approve-deviation']));
//console.log(Roles.getRolesForUser(user));
//console.log(DraftDeviations.find({userId : Meteor.userId(),state: 'Approval'}).fetch());
//console.log(DevsDeclarations.find({'investigation.investigatorId' : Meteor.userId(),state: 'Investigation'}).count());
return {
user: user,
drafts:  DraftDeviations.find({'user._id' : Meteor.userId(),state: 'Draft'}).count(),
approvals:  Roles.userIsInRole(user,['approve-deviation'])? DraftDeviations.find({"approbation.approbatorId":Meteor.userId(), state: 'Approval'}).count():0,
incomplete:  DraftDeviations.find({'user._id' : Meteor.userId(),state: 'MissingInfo'}).count(),
sent:  DraftDeviations.find({'user._id' : Meteor.userId(),state: 'Approval'}).count(),
expertises:  DevsDeclarations.find({'expertise.expertId' : Meteor.userId(),state: 'Declaration'}).count(),
investigations:  DevsDeclarations.find({'investigation.investigatorId' : Meteor.userId(),state: 'Investigation'}).count(),
};
}
)(Header);
