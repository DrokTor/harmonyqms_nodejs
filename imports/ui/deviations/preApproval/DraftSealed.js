import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';

import DatePicker from 'react-date-picker';
import TimePicker from 'react-time-picker';
import Select, {Option, OptGroup} from 'rc-select';
import 'rc-select/assets/index.css';

import Alert from 'react-s-alert';
//import Header from './Header.js';
//import Login from './Login.js';
//import Modules from './Modules.js';
// import { Processes } from '../../api/process';
// import { Products } from '../../api/products';
// import { Equipments } from '../../api/equipments';
// import { Methods } from '../../api/methods';
import { DraftDeviations } from '../../../api/draftDeviations';

// import  ApprobatorModal  from './ApprobatorModal';
// import  RequestInfoModal  from './RequestInfoModal';
// import  CloseDeviationModal  from './CloseDeviationModal';


// App component - represents the whole app

export  class DraftSealedInterface extends Component {


  constructor(props){
    super(props);

    this.state = {
      _id:'',
      userId: Meteor.userId(),
      title : '',
      detectionDate : '',
      detectionTime : '',
      type : '',
      planification : '',
      process : '',
      locality : '',
      equipment : '',
      product : '',
      batch : '',
      what : '',
      how : '',
      impact : '',
      immediateActions : '',
      suspectedRootCauses : '',
      approbatorId : '',
      approbation : '',
      state : '',
    }

    //this.save = this.save.bind(this);
    //this.sendForApproval = this.sendForApproval.bind(this);
    //this.requestInfo = this.requestInfo.bind(this);
    //this.closeDeviation = this.closeDeviation.bind(this);
  }

  componentDidMount(){
    if(this.props.draft != '' )
    {
      this.setState(this.props.draft)
    }
    //console.log(this.state.approbation.approbatorId);
    this.props.draft && Meteor.call('user.findOne',this.props.draft.approbation.approbatorId,(error, result)=>{
      if(result)
      this.setState({approbator : result.username})
      //console.log(result);
    })
  }




  render() {
    //console.log(this.props.draft);
    if(this.props.draft)
    {
    return (

      <div className="col-10  core">
        <div className="row p-4 border text-center mb-3">
            <div className="col-6   ">
            <label className="col-7">Sending date</label>
            <p className="col-12">
              {moment(this.props.draft.approbation.sentDate).format('DD-MM-YYYY') }
            </p>
          </div>
          <div className="col-6   ">
            <label className="col-7">Approbator</label>
            <p className="col-12">
              {this.props.draft.approbator }
            </p>
          </div>
        </div>
        <h3 className="collapseLabel col-md-12" data-toggle="collapse" data-target="#genInfo">General information </h3>
        <div id="genInfo" className='collapse show  form_field row no-gutters'>
          <div className="col-12 mb-3  ">
            <label className="col-7">Deviation title</label>
            <p className="col-12">
              {this.props.draft.title}
            </p>
          </div>
          <div className="col-3  ">
            <label className="col-12" >Detection date</label>
            <p className="col-12">
              {moment(this.props.draft.detectionDate).format('DD-MM-YYYY')}
            </p>
          </div>
          <div className="col-3  ">
            <label className="col-12" >Detection time</label>
            <p className="col-12">
              {this.props.draft.detectionTime}
            </p>
          </div>
          <div className="col-3  ">
            <label className="col-12" >Type</label>
            <p className="col-12">
              {this.props.draft.type}
            </p>
          </div>
          <div className="col-3  ">
            <label className="col-12" >Planification</label>
            <p className="col-12">
              {this.props.draft.planification}
            </p>
          </div>
        </div>

        {/* targetting */}
        <h3 className="collapseLabel col-md-12  mt-5" data-toggle="collapse" data-target="#targetting">Targetting </h3>
        <div id="targetting" className='collapse show  form_field row no-gutters'>
          <div className="col-3 ">
            <label className="col-12" >Process</label>
            <p className="col-12">
              {this.props.draft.process}
            </p>
          </div>
          <div className="col-3 ">
            <label className="col-12" >Locality</label>
            <p className="col-12">
              {this.props.draft.locality}
            </p>
          </div>
          <div className="col-3 ">
            <label className="col-12" >Equipment</label>
            <p className="col-12">
              {this.props.draft.equipment}
            </p>
          </div>
          <div className="col-3 ">
            <label className="col-12" >Product Code</label>
            <p className="col-12">
              {this.props.draft.product}
            </p>
          </div>
          <div className="col-12 ">
            <label className="col-12" >Batch Number</label>
            <p className="col-12">
              {this.props.draft.batch && this.props.draft.batch.join(', ')}
            </p>
          </div>
        </div>

        {/* details */}
        <h3 className="collapseLabel col-md-12  mt-5" data-toggle="collapse" data-target="#detail">Details </h3>
        <div id="detail" className='collapse show  form_field row  '>
          <div className="col-6 mb-3 ">
            <label className="col-12">What</label>
            <p className="col-12">
              {this.props.draft.what}
            </p>
          </div>
          <div className="col-6 mb-3 ">
            <label className="col-12 ">How</label>
            <p className="col-12">
              {this.props.draft.how}
            </p>
          </div>
          <div className="col-6 mb-3 ">
            <label className="col-12 ">Impact</label>
            <p className="col-12">
              {this.props.draft.impact}
            </p>
          </div>
          <div className="col-6 mb-3 ">
            <label className="col-12 ">Immediate Actions</label>
            <p className="col-12">
              {this.props.draft.immediateActions}
            </p>
          </div>
          <div className="col-6 mb-3 ">
            <label className="col-12 ">Suspected root cause</label>
            <p className="col-12">
              {this.props.draft.suspectedRootCauses}
            </p>
          </div>
        </div>


      </div>

    );
  }else {
    return '';
  }

  }

}


export default DraftSealed = withTracker( ({match}) => {
const user=Meteor.userId();

Meteor.subscribe('draftDeviations');

const draftId = match.params._id;

//console.log(DraftDeviations.findOne({_id : new Mongo.ObjectID(draftId)}));
//Meteor.subscribe('users');
Meteor.subscribe('usersInRoles',['approve-deviation']);
//console.log(Meteor.users.find({}).fetch());
//console.log(Roles.getUsersInRole(['approve-deviation'],null).fetch());
return {
user: user,
draft: DraftDeviations.findOne({_id : new Mongo.ObjectID(draftId)}),
// localities : Localities.find({}).fetch(),
// methods : Methods.find({}).fetch(),
// equipments : Equipments.find({}).fetch(),
// products : Products.find({}).fetch(),
};
}
)(DraftSealedInterface);
