import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';



import {DraftDeviations} from '../../../api/draftDeviations.js';
//import Login from './Login.js';
//import Modules from './Modules.js';



// App component - represents the whole app

export  class MissingInfo extends Component {


  constructor(props){
    super(props);

    this.state = {
      page : 'modules',
    }

   }


  render() {

    return (

      <div className="col-9 core  ">
        <div className="row">
          <h3>Incomplete deviations ({this.props.missingInfo.length})</h3>
          <table className="col-12  mt-5 ">
            <thead><tr><th className="col-3">Title</th><th className="col-3">Detection date</th><th className="col-3">Type</th><th className="col-3">Planification</th></tr></thead>
            <tbody>
          {
            (() => {return this.props.missingInfo.map((draft,index)=>{
              return <tr key={index} className="pointer" onClick={()=>{this.props.history.push('/deviations/new/'+draft._id)}} ><td className="col-3">{draft.title}</td><td className="col-3">{moment(draft.detectionDate).format('DD-MM-YYYY')}</td><td className="col-3">{draft.type}</td><td>{draft.planification}</td></tr>
            })})()
          }
          </tbody>
          </table>
        </div>
      </div>

    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();

Meteor.subscribe('draftDeviations');
//console.log(DraftDeviations.find({}).fetch());
return {
user: user,
missingInfo:  DraftDeviations.find({'user._id' : Meteor.userId(),state : 'MissingInfo'}).fetch(),
};
}
)(MissingInfo);
