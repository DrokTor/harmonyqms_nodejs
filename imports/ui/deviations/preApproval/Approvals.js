import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';



import {DraftDeviations} from '../../../api/draftDeviations.js';
//import Login from './Login.js';
//import Modules from './Modules.js';



// App component - represents the whole app

export  class Approvals extends Component {


  constructor(props){
    super(props);

    this.state = {
      page : 'modules',
    }

   }


  render() {

    return (

      <div className="col-9 core   ">
        <div className="row">
          <h3>Deviations waiting for approval ({this.props.approvals.length})</h3>
          <table className="col-12  mt-5 ">
            <thead><tr><th className="col-3">Title</th><th className="col-3">Detection date</th><th className="col-3">Type</th><th className="col-3">Planification</th></tr></thead>
            <tbody>
          {   this.props.approvals ?
            (() => {return this.props.approvals.map((draft,index)=>{
              return <tr key={index} className="pointer" onClick={()=>{this.props.onClick('newDeviation',draft)}} ><td className="col-3">{draft.title}</td><td className="col-3">{moment(draft.detectionDate).format('DD-MM-YYYY')}</td><td className="col-3">{draft.type}</td><td>{draft.planification}</td></tr>
            })})()
            :''
          }
          </tbody>
          </table>
        </div>
      </div>

    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();

if( Roles.userIsInRole(user,['approve-deviation']) )
{
  Meteor.subscribe('approveDeviations');
}

return {
user: user,
approvals:  Roles.userIsInRole(Meteor.userId(),['approve-deviation'])? DraftDeviations.find({"approbation.approbatorId":Meteor.userId(), state: 'Approval'}).fetch():0,
};
}
)(Approvals);
