import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';

import DatePicker from 'react-date-picker';
import TimePicker from 'react-time-picker';
import Select, {Option, OptGroup} from 'rc-select';
import 'rc-select/assets/index.css';

import Alert from 'react-s-alert';
//import Header from './Header.js';
//import Login from './Login.js';
//import Modules from './Modules.js';
import { Processes } from '../../api/process';
import { Products } from '../../api/products';
import { Equipments } from '../../api/equipments';
import { Methods } from '../../api/methods';
import { Localities } from '../../api/localities';
import { DraftDeviations } from '../../api/draftDeviations';

import  ApprobatorModal  from './modals/ApprobatorModal';
import  ExpertiseModal  from './modals/ExpertiseModal';
import  MissingInfoModal  from './modals/MissingInfoModal';
import  DiscardDeviationModal  from './modals/DiscardDeviationModal';


// App component - represents the whole app

export  class NewDeviation extends Component {


  constructor(props){
    super(props);
    //console.log(Meteor.user());
    this.draft = {
      _id:'',
      //userId: Meteor.userId(),
      title : '',
      creationDate : new Date(),
      detectionDate : '',
      detectionTime : '',
      type : '',
      planification : '',
      process : '',
      locality : '',
      equipment : '',
      product : '',
      batch : [],
      what : '',
      how : '',
      impact : '',
      immediateActions : '',
      suspectedRootCauses : '',
      state : '',
      user: Meteor.user(),
      approbatorId : '',
      approbation : '',
    }

    this.state = {...this.draft};

    this.save = this.save.bind(this);
    this.sendForApproval = this.sendForApproval.bind(this);
    this.sendForExpertise = this.sendForExpertise.bind(this);
    this.requestInfo = this.requestInfo.bind(this);
    this.discardDeviation = this.discardDeviation.bind(this);
    //console.log(this.props.draft);
  }

  componentWillReceiveProps(nextProps) {
    //console.log(this.draft);
    // This lifecycle resets the "draft" email value in state,
    // Whenever a new "commited" email value is passed in props.
    // The downside of this is that the parent component has
    // no way to reset state back to the original props value
    // (at least not without rendering multiple times).
    // if (nextProps.email !== this.props.email) {
    //   this.setState({ email: nextProps.email });
    // }
    if(nextProps.draft != '' )
    {
      this.setState(nextProps.draft)
    }
    else{
      this.setState(this.draft);
    }
    //console.log(nextProps.draft);
  }



  save(page){
    Meteor.call('draftDeviation.insert',this.state,(error,result)=>{
      if(result){
        Alert.info('Deviation saved.');
        this.setState({_id:result})
      }
    })
  }

  sendForApproval(){
    //console.log(this.state.approbator);
    Meteor.call('draftDeviation.sendForApproval',this.state,(error,result)=>{
      if(result){
        Alert.success('Deviation is waiting for Approval.');
        //this.setState({_id:result})
        Meteor.call('draftDeviation.sendApprovalEmail',this.state._id,this.state.approbation.approbatorId,(err,res)=>{
          if(res)
          Alert.info('Email sent to "'+res+'" for Approval.');
        })
        this.props.onClick('sent'); // navigate to sent deviations interface
      }
    })


  }

  sendForExpertise(){
    //console.log(this.state.approbator);
    Meteor.call('devsDeclarations.sendForExpertise',this.state,(error,result)=>{
      if(result){
        Alert.success('Deviation is waiting for Expertise.');
        //this.setState({_id:result})
        Meteor.call('devsDeclarations.sendExpertiseEmail',this.state._id,this.state.expertise.expertId,(err,res)=>{
          if(res)
          Alert.info('Email sent to "'+res+'" for Expertise.');
        })
        this.props.onClick('approvals'); // navigate to deviations approval interface
      }
    })


  }

  requestInfo(){

    Meteor.call('draftDeviation.requestInfo',this.state,(error,result)=>{
      if(result){
        Alert.success('Deviation is sent back for completion.');
        //this.setState({_id:result})
        Meteor.call('draftDeviation.sendRequestInfoEmail',this.state._id,this.state.user._id,(err,res)=>{
          if(res)
          Alert.info('Email sent to "'+res+'".');
        })
        this.props.onClick('approvals'); // navigate to deviations approval interface

      }
    })

  }

  discardDeviation(){
    Meteor.call('draftDeviation.discardDeviation',this.state,(err,res)=>{
      if(res){
        Alert.success('Deviation discarded.');
        this.props.onClick('approvals');
      }
    })
  }

  render() {

    return (

      <div className="col-10 core   ">

          <div className="row p-4 border text-center mb-3">
              <div className="col-6   ">
              <label className="col-7">Creation date</label>
              <p className="col-12">
                {moment(this.state.creationDate).format('DD-MM-YYYY') }
              </p>
            </div>
            <div className="col-6   ">
              <label className="col-7">Creation time</label>
              <p className="col-12">
                {moment(this.state.creationDate).format('HH:mm') }
              </p>
            </div>
          </div>
          {(()=>{

            if(this.state.requestInfo){
              return <div className="row border border-warning p-3 mb-3">
                      <label className='col-3 text-secondary' >Incomplete declaration : </label>
                      <div className='col-9'>{this.state.requestInfo}</div>
                    </div>
            }
          })()
          }
        <h3 className="collapseLabel col-md-12" data-toggle="collapse" data-target="#genInfo">General information </h3>
        <div id="genInfo" className={'collapse form_field row no-gutters '+   ((this.state.state === 'Approval') ? 'show':'') } >
          <div className="col-12 mb-3  ">
            <label className="col-7">Deviation title</label>
            <input
              className="col-7 ml-3"
              type = "text"
              value = {this.state.title}
              onChange={ event =>  this.setState({title : event.target.value}) }
            />
          </div>
          <div className="col-3  ">
            <label className="col-12" >Detection date</label>
            <DatePicker
              className="col-12 "
              onChange={detectionDate => this.setState({detectionDate})}
              value={this.state.detectionDate}
              locale='FR'
            />
          </div>
          <div className="col-3  ">
            <label className="col-12" >Detection time</label>
            <TimePicker
              className="col-12 "
              onChange={detectionTime => this.setState({detectionTime})}
              value={this.state.detectionTime}
              locale='FR'
            />
          </div>
          <div className="col-3  ">
            <label className="col-12" >Type</label>
            <Select
              className="col-12 "
              onSelect={type => this.setState({type})}
              value = {this.state.type}
            >
              <Option value="Quality">Quality</Option>
              <Option value="HSE">HSE</Option>
            </Select>
          </div>
          <div className="col-3  ">
            <label className="col-12" >Planification</label>
            <Select
              className="col-12 "
              onSelect={planification => this.setState({planification})}
              value = {this.state.planification}
            >
              <Option value="Deviation not Planned">Deviation not Planned</Option>
              <Option value="Deviation Planned">Deviation Planned</Option>
            </Select>
          </div>
        </div>

        {/* targetting */}
        <h3 className="collapseLabel col-md-12  mt-5" data-toggle="collapse" data-target="#targetting">Targetting </h3>
        <div id="targetting" className={'collapse form_field row no-gutters '+   ((this.state.state === 'Approval') ? 'show':'') }>
          <div className="col-3 ">
            <label className="col-12" >Process</label>
            <Select
              className="col-12 "
              onSelect={process => this.setState({process})}
              value = {this.state.process}
            >
              {
                this.props.processes.map((proc,index)=>{
                return  <Option key={index} value={proc.name}>{proc.name}</Option>

                })
              }
            </Select>
          </div>
          <div className="col-3 ">
            <label className="col-12" >Locality</label>
            <Select
              className="col-12 "
              onSelect={locality => this.setState({locality})}
              value = {this.state.locality}
            >
              {
                this.props.localities.map((loc,index)=>{
                return  <Option key={index} value={loc.name}>{loc.name}</Option>

                })
              }
            </Select>
          </div>
          <div className="col-3 ">
            <label className="col-12" >Equipment</label>
            <Select
              className="col-12 "
              onSelect={equipment => this.setState({equipment})}
              value = {this.state.equipment}
            >
              {
                this.props.equipments.map((equip,index)=>{
                return  <Option key={index} value={equip.name}>{equip.name}</Option>

                })
              }
            </Select>
          </div>
          <div className="col-3 ">
            <label className="col-12" >Product Code</label>
            <Select
              className="col-12 "
              onSelect={product => this.setState({product})}
              value = {this.state.product}
            >
              {
                this.props.products.map((prod,index)=>{
                return  <Option key={index} value={prod.name}>{prod.name}</Option>

                })
              }
            </Select>
          </div>
          <div className="col-3 ">
            <label className="col-12" >Batch Number</label>
            <Select
              //onChange={ event =>  this.setState({batch : event.target.value}) }
              className="col-12 "
              onChange={ batch => {
                //console.log(batch);
                //let batch = this.state.batch;
                //batch.push(btc);
                this.setState({batch})
                }
              }
              // onDeselect={btc => {
              //   let batch = this.state.batch;
              //   batch = _.without(batch, _.findWhere(batch, btc));
              //   this.setState({batch})}
              // }
              value = {this.state.batch}
              tags = {true}
              tokenSeparators={[' ', ',']}
            >
              {/* <Option value={''}></Option> */}
            </Select>
          </div>
        </div>

        {/* details */}
        <h3 className="collapseLabel col-md-12  mt-5" data-toggle="collapse" data-target="#detail">Details </h3>
        <div id="detail" className={'collapse form_field row  '+   ((this.state.state === 'Approval') ? 'show':'') }>
          <div className="col-6 mb-3 ">
            <label className="col-12">What</label>
            <textarea
              className="col-12 ml-3"
              type = "text"
              value = {this.state.what}
              onChange={event => this.setState({what: event.target.value})}
            />
          </div>
          <div className="col-6 mb-3 ">
            <label className="col-12 ">How</label>
            <textarea
              className="col-12 ml-3"
              type = "text"
              value = {this.state.how}
              onChange={event => this.setState({how: event.target.value})}
            />
          </div>
          <div className="col-6 mb-3 ">
            <label className="col-12 ">Impact</label>
            <textarea
              className="col-12 ml-3"
              type = "text"
              value = {this.state.impact}
              onChange={event => this.setState({impact: event.target.value})}
            />
          </div>
          <div className="col-6 mb-3 ">
            <label className="col-12 ">Immediate Actions</label>
            <textarea
              className="col-12 ml-3"
              type = "text"
              value = {this.state.immediateActions}
              onChange={event => this.setState({immediateActions: event.target.value})}
            />
          </div>
          <div className="col-6 mb-3 ">
            <label className="col-12 ">Suspected root cause</label>
            <textarea
              className="col-12 ml-3"
              type = "text"
              value = {this.state.suspectedRootCauses}
              onChange={event => this.setState({suspectedRootCauses: event.target.value})}
            />
          </div>
        </div>
        {this.state.state != 'Approval' ?
        <div className="row mb-5 mt-5">
          <div className="col-4 offset-2" >
            <button className="btn btn-success  col-12 p-3" data-toggle="modal" data-target="#approvalModal">Launch for Approval</button>
          </div>
          <div className="col-4" >
            <button className="btn btn-primary col-12 p-3" onClick={this.save} >Save</button>
          </div>
        </div>
        :
        <div className="row mb-5 mt-5 ">
          <div className="col-4" >
            <button className="btn btn-success col-12 p-3  " data-toggle="modal" data-target="#expertiseModal">Approve Deviation</button>
            </div>
          <div className="col-4" >
            <button className="btn btn-primary col-12 p-3  " data-toggle="modal" data-target="#missingInfoModal"  >Request more information</button>
            </div>
          <div className="col-4" >
            <button className="btn btn-danger col-12 p-3 " data-toggle="modal" data-target="#discardDeviationModal"  >Close Deviation</button>
            </div>
        </div>
        }

        {/* <!-- Modal --> */}

                <ApprobatorModal onSelect={ approbator => {
                  let appro = { ...this.state.approbation };
                  appro.approbatorId = approbator._id;
                  this.setState({approbation : appro })}
                } onClick={ this.sendForApproval } />
                <ExpertiseModal onSelect={ expert => {
                  let expertise = { ...this.state.approbation };
                  expertise.expertId = expert._id;
                  this.setState({expertise : expertise })}
                } onClick={ this.sendForExpertise }
                  onLvl={ level => this.setState({level, estimatedLevel: level}) } />
                <MissingInfoModal onChange={ event => this.setState({requestInfo : event.target.value}) } onClick={ this.requestInfo } />
                <DiscardDeviationModal onChange={ event => this.setState({discardDev : event.target.value}) } onClick={ this.discardDeviation } />
                {/* <Select
                  className="col-12 "
                  onSelect={approbator => this.setState({approbator : JSON.parse(approbator)})}
                  value = {this.state.approbator.username}
                >
                  {
                    Meteor.users.find({}).fetch().map((user,index)=>{
                    return  <Option key={index} value={JSON.stringify(user)}>{user.username}</Option>

                    })
                  }
                </Select> */}


      </div>

    );

  }

}


export default NewDeviationContainer = withTracker( ({match}) => {
const user=Meteor.userId();
//console.log(Meteor.user());
Meteor.subscribe('process');
Meteor.subscribe('locality');
Meteor.subscribe('equipment');
Meteor.subscribe('method');
Meteor.subscribe('product');

//Meteor.subscribe('users');
Meteor.subscribe('usersInRoles',['approve-deviation']);
Meteor.subscribe('draftDeviations');

const draftId = match.params._id;
//console.log(Meteor.users.find({}).fetch());
//console.log(Roles.getUsersInRole(['approve-deviation'],null).fetch());
return {
user: user,
processes : Processes.find({}).fetch(),
localities : Localities.find({}).fetch(),
methods : Methods.find({}).fetch(),
equipments : Equipments.find({}).fetch(),
products : Products.find({}).fetch(),
draft: DraftDeviations.findOne({_id : new Mongo.ObjectID(draftId)}),
};
}
)(NewDeviation);
