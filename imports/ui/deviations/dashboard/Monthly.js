import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import ReactDOM from 'react-dom';

import Chart from 'chart.js';

//import { CategoriesCount } from '../../../api/dataDevs/categoriesAgg.js';
//import {DevsDeclarations} from '../../../api/devsDeclarations.js';
import ChartComponent from './Chart.js' ;

export const CategoryAgg = new Meteor.Collection('categoryAgg');
export const LevelAgg = new Meteor.Collection('levelAgg');
export const PlanificationAgg = new Meteor.Collection('planificationAgg');
export const ProcessAgg = new Meteor.Collection('processAgg');
export const EquipmentAgg = new Meteor.Collection('equipmentAgg');
export const LocalityAgg = new Meteor.Collection('localityAgg');
export const ProductAgg = new Meteor.Collection('productAgg');

export const MethodAgg = new Meteor.Collection('methodAgg');
export const IshikawaAgg = new Meteor.Collection('ishikawaAgg');
export const DecisionAgg = new Meteor.Collection('decisionAgg');

export const ProcessAvgCls = new Meteor.Collection('processAvgCls');
export const ProductAvgCls = new Meteor.Collection('productAvgCls');
export const LocalityAvgCls = new Meteor.Collection('localityAvgCls');
export const EquipmentAvgCls = new Meteor.Collection('equipmentAvgCls');
//export const LevelsAgg = new Meteor.Collection('levelAgg');


export class Monthly  extends Component {

  componentDidMount() {

  }

  render() {
    return (
      <div className="row">
        {/* <ChartComponent id={'chart-'+index} refrence={'chart-'+index} key={'chart-'+index} chart={elem} handleChartClick={this.handleChartClick} /> */}
        <ChartComponent  chart={this.props.lvlAgg} name="Criticality" reference="level"  chartType="bar"/>
        <ChartComponent  chart={this.props.catAgg} name="Type" reference="category"  chartType="bar"/>
        <ChartComponent  chart={this.props.prodAgg} name="Product" reference="product"  chartType="bar"/>
        <ChartComponent  chart={this.props.procAgg} name="Process" reference="process"  chartType="bar"/>
        <ChartComponent  chart={this.props.equAgg} name="Equipment" reference="equipment"  chartType="bar"/>
        <ChartComponent  chart={this.props.locAgg} name="Locality" reference="locality"  chartType="bar"/>
        <ChartComponent  chart={this.props.palnAgg} name="Planification" reference="planification"  chartType="bar"/>
        <ChartComponent  chart={this.props.methAgg} name="Method" reference="method"  chartType="bar"/>
        <ChartComponent  chart={this.props.ishiAgg} name="5M" reference="ishikawa"  chartType="bar"/>
        <ChartComponent  chart={this.props.decAgg} name="Decision" reference="decision"  chartType="bar"/>
        <ChartComponent  chart={this.props.procAvgCls} name="Average closing / process" reference="processAvgCls"  chartType="bar"/>
        <ChartComponent  chart={this.props.prodAvgCls} name="Average closing / product" reference="productAvgCls"  chartType="bar"/>
        <ChartComponent  chart={this.props.equipAvgCls} name="Average closing / equipment" reference="equipmentAvgCls"  chartType="bar"/>
        <ChartComponent  chart={this.props.locAvgCls} name="Average closing / locality" reference="localityAvgCls"  chartType="bar"/>
      </div>
    );
  }

}

export default withTracker(()=>{

  Meteor.subscribe('categoriesCount');
  Meteor.subscribe('levelsCount');
  Meteor.subscribe('equipmentsCount');
  Meteor.subscribe('planificationsCount');
  Meteor.subscribe('localitiesCount');
  Meteor.subscribe('productsCount');
  Meteor.subscribe('processCount');
  Meteor.subscribe('methodsCount');
  Meteor.subscribe('ishikawaCount');
  Meteor.subscribe('decisionCount');
  Meteor.subscribe('processAvgCls');
  Meteor.subscribe('productAvgCls');
  Meteor.subscribe('equipmentAvgCls');
  Meteor.subscribe('localityAvgCls');
  //Meteor.subscribe('levelsCount');

  const catAgg = CategoryAgg.find({}).fetch();
  const lvlAgg = LevelAgg.find({}).fetch();
  const palnAgg = PlanificationAgg.find({}).fetch();
  const procAgg = ProcessAgg.find({}).fetch();
  const prodAgg = ProductAgg.find({}).fetch();
  const equAgg = EquipmentAgg.find({}).fetch();
  const locAgg = LocalityAgg.find({}).fetch();

  const methAgg = MethodAgg.find({}).fetch();
  const ishiAgg = IshikawaAgg.find({}).fetch();
  const decAgg = DecisionAgg.find({}).fetch();

  const procAvgCls = ProcessAvgCls.find({}).fetch();
  const prodAvgCls = ProductAvgCls.find({}).fetch();
  const equipAvgCls = EquipmentAvgCls.find({}).fetch();
  const locAvgCls = LocalityAvgCls.find({}).fetch();

  //console.log(decAgg);

  return {
    catAgg,
    lvlAgg,
    palnAgg,
    procAgg,
    prodAgg,
    equAgg,
    locAgg,
    methAgg,
    ishiAgg,
    decAgg,
    procAvgCls,
    prodAvgCls,
    equipAvgCls,
    locAvgCls,
  }

})(Monthly);
