import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';



//import Header from './Header.js';
//import Login from './Login.js';
import {DevsDeclarations} from '../../../api/devsDeclarations.js';
import {Objectives} from '../../../api/objectives.js';



// App component - represents the whole app

export  class Indicators extends Component {


  constructor(props){
    super(props);

    this.state = {
      page : 'modules',
    }

    this.loadPage = this.loadPage.bind(this);
  }

  loadPage(page){
    this.setState({page});
  }

  isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
  }

  render() {
    const maxDevs = this.props.maxDevs,
      maxDevs_obj = this.props.maxDevs_obj,
      clsRate = this.props.clsRate,
        clsRate_obj = this.props.clsRate_obj,
      avgClsTime = this.props.avgClsTime,
        avgClsTime_obj = this.props.avgClsTime_obj,
      devsOver = this.props.devsOver,
        devsOver_obj = this.props.devsOver_obj;

    return (
      <div className="col-12  ">
        {
          maxDevs_obj &&
        <div className="col-12 row">

          <div className="row col-6 p-4 indicator-div">
            <div className="col-9 text-center">
              <h4>Number of deviations</h4>
              <div className="col-12 fs-3">{maxDevs}</div>
            </div>
            <div className="col-3 ">
              <div className="col-8 mx-auto indicator-light-container">
                <div className={" indicator-light "+ (maxDevs>maxDevs_obj.obj? "":"bg-success")}></div>
                <div className={" indicator-light "+ (maxDevs>maxDevs_obj.obj? "bg-danger":"")}></div>
              </div>

            </div>
          </div>

          <div className="row col-6 p-4 indicator-div">
            <div className="col-9 text-center">
              <h4>Closing rate</h4>
              <div className="col-12 fs-3">{ this.isNumeric(clsRate)? clsRate+' %' : '-'}  </div>
            </div>
            <div className="col-3 ">
              <div className="col-8 mx-auto indicator-light-container">
                <div className={" indicator-light "+ (clsRate<clsRate_obj.obj? " ":"bg-success")}></div>
                <div className={" indicator-light "+ (clsRate<clsRate_obj.obj? "bg-danger":" ")}></div>
              </div>
            </div>
          </div>

          <div className="row col-6 p-4 indicator-div">
            <div className="col-9 text-center">
              <h4>Average closing time</h4>
              <div className="col-12 fs-3">{this.isNumeric(avgClsTime)? avgClsTime+' days' : '-'} </div>
            </div>
            <div className="col-3 ">
              <div className="col-8 mx-auto indicator-light-container">
                <div className={" indicator-light "+ (avgClsTime>avgClsTime_obj.obj? " ":"bg-success")}></div>
                <div className={" indicator-light "+ (avgClsTime>avgClsTime_obj.obj? "bg-danger":" ")}></div>
              </div>
            </div>
          </div>

          <div className="row col-6 p-4 indicator-div">
            <div className="col-9 text-center">
              <h4>Deviations overdues</h4>
              <div className="col-12 fs-3">{devsOver}</div>
            </div>
            <div className="col-3 ">
              <div className="col-8 mx-auto indicator-light-container">
                <div className={" indicator-light "+ (devsOver>devsOver_obj.obj? "":"bg-success")}></div>
                <div className={" indicator-light "+ (devsOver>devsOver_obj.obj? "bg-danger":"")}></div>
              </div>
            </div>
          </div>
        </div>
      }
      </div>

    );

  }

}


export default withTracker( () => {

Meteor.subscribe("devsDeclarations");
Meteor.subscribe("objectives");

const objectives = Objectives.find({}).fetch();
//console.log(moment().startOf('month').toDate());
//console.log(_.findWhere(objectives,{indicator : "Number of deviations"}));
//console.log(DevsDeclarations.find({ creationDate : {$gt : moment().subtract(30, 'days').toISOString()} ,state : {$ne : 'Closed' }}).count());

const monthDevs = DevsDeclarations.find({creationDate : {$gt : moment().startOf('month').toDate() , $lt : moment().endOf('month').toDate() } }).count(),
monthDevsClosed = DevsDeclarations.find({state: 'Closed' ,creationDate : {$gt : moment().startOf('month').toDate() , $lt : moment().endOf('month').toDate() } }).fetch(),
monthDevsClsTime = _.reduce(monthDevsClosed,(mem,obj)=>{ return mem + obj.closing.duration; },0);

// console.log(monthDevsClsTime);
//
// console.log(monthDevs);
// console.log(monthDevsClosed);
// console.log(monthDevsClosed.length/monthDevs);
// console.log(monthDevsClsTime/monthDevsClosed.length);


return {
  maxDevs : monthDevs,
  maxDevs_obj : _.findWhere(objectives,{indicator : "Number of deviations"}),
  clsRate: Math.round(monthDevsClosed.length/monthDevs*100),
  clsRate_obj : _.findWhere(objectives,{indicator : "Closing rate"}),
  avgClsTime: Math.round(monthDevsClsTime/monthDevsClosed.length),
  avgClsTime_obj : _.findWhere(objectives,{indicator : "Average closing time"}),
  devsOver : DevsDeclarations.find({ creationDate : {$lt : moment().subtract(30, 'days').toDate()} ,state : {$ne : 'Closed' }}).count(),
  devsOver_obj : _.findWhere(objectives,{indicator : "Deviations overdues"}),
};
}
)(Indicators);
