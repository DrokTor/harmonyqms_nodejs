import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';


import { Route,Switch,Link,Redirect } from 'react-router-dom'
//import Header from './Header.js';
//import Login from './Login.js';
import Indicators from './Indicators.js';
import Monthly from './Monthly.js';
import Annual from './Annual.js';



// App component - represents the whole app

export  class Dashboard extends Component {


  constructor(props){
    super(props);

    this.state = {
      page : 'modules',
    }

    this.loadPage = this.loadPage.bind(this);
  }

  loadPage(page){
    this.setState({page});
  }

  render() {

    return (

      <div className="col-9 mt-5  ">
        <div className="col-12 mb-3">
          <button className="btn mr-2 btn-outline-secondary" onClick={()=>this.props.history.push('/deviations/dashboard/objectives')} >Indicators</button>
          <button className="btn mr-2 btn-outline-secondary" onClick={()=>this.props.history.push('/deviations/dashboard/monthly-data')} >Monthly analysis</button>
          <button className="btn btn-outline-secondary" onClick={()=>this.props.history.push('/deviations/dashboard/annual-data')} >Annual analysis</button>
          <button className="btn btn-outline-secondary float-right"><i className="fa fa-expand"></i></button>

        </div>
        <Switch>
          <Route exact path="/deviations/dashboard/" component={Indicators}/>
          <Route path="/deviations/dashboard/objectives" component={Indicators}/>
          <Route path="/deviations/dashboard/monthly-data" component={Monthly}/>
          <Route path="/deviations/dashboard/annual-data" component={Annual}/>
        </Switch>
        {/* { (()=>{
          switch (this.state.dataType) {
            case 'indicators':
              return <Indicators />;
              break;
            default:
            case 'indicators':
              return <Indicators />;
              break;

          }})()
        } */}
      </div>

    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();

return {
user: user,
};
}
)(Dashboard);
