import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import ReactDOM from 'react-dom';

import Chart from 'chart.js';

//import { CategoriesCount } from '../../../api/dataDevs/categoriesAgg.js';
//import {DevsDeclarations} from '../../../api/devsDeclarations.js';
import ChartComponent from './Chart.js' ;

export const DeviationYAgg = new Meteor.Collection('deviationYAgg');
export const LevelYAgg = new Meteor.Collection('levelYAgg');
export const ProcessYAgg = new Meteor.Collection('processYAgg');
export const LocalityYAgg = new Meteor.Collection('localityYAgg');
export const EquipmentYAgg = new Meteor.Collection('equipmentYAgg');
export const ProductYAgg = new Meteor.Collection('productYAgg');
export const CategoryYAgg = new Meteor.Collection('typeYAgg');
export const MethodYAgg = new Meteor.Collection('methodYAgg');
export const IshikawaYAgg = new Meteor.Collection('ishikawaYAgg');
export const DecisionYAgg = new Meteor.Collection('decisionYAgg');
export const AvgClosingYAgg = new Meteor.Collection('avgClosingYAgg');
export const AvgClosingProcYAgg = new Meteor.Collection('avgClosingProcYAgg');
export const AvgClosingProdYAgg = new Meteor.Collection('avgClosingProdYAgg');
export const AvgClosingLocYAgg = new Meteor.Collection('avgClosingLocYAgg');
export const AvgClosingEquipYAgg = new Meteor.Collection('avgClosingEquipYAgg');


//export const LevelsAgg = new Meteor.Collection('levelAgg');


export class Annual  extends Component {

  componentDidMount() {

  }

  render() {
    return (
      <div className="row">
        {/* <ChartComponent id={'chart-'+index} refrence={'chart-'+index} key={'chart-'+index} chart={elem} handleChartClick={this.handleChartClick} /> */}
        <ChartComponent  chart={this.props.devYAgg} name="Deviations / year" reference="deviationYAgg"  chartType="bar" annual = {true} />
        <ChartComponent  chart={this.props.devYAcc} name="Deviations increment / year" reference="deviationYAcc"  chartType="bar" annual = {true} />
        <ChartComponent  chart={this.props.lvlYAgg} name="Deviations / level / year" reference="levelYAcc"  chartType="bar" annual = {true} dataSet={true}/>
        <ChartComponent  chart={this.props.procYAgg} name="Deviations / process / year" reference="procYAcc"  chartType="bar" annual = {true} dataSet={true}/>
        <ChartComponent  chart={this.props.locYAgg} name="Deviations / locality / year" reference="locYAcc"  chartType="bar" annual = {true} dataSet={true}/>
        <ChartComponent  chart={this.props.equpYAgg} name="Deviations / equipment / year" reference="equpYAcc"  chartType="bar" annual = {true} dataSet={true}/>
        <ChartComponent  chart={this.props.prodYAgg} name="Deviations / product / year" reference="prodYAcc"  chartType="bar" annual = {true} dataSet={true}/>
        <ChartComponent  chart={this.props.catYAgg} name="Deviations / category / year" reference="catYAcc"  chartType="bar" annual = {true} dataSet={true}/>
        <ChartComponent  chart={this.props.methYAgg} name="Deviations / method / year" reference="methYAcc"  chartType="bar" annual = {true} dataSet={true}/>
        <ChartComponent  chart={this.props.ishiYAgg} name="Deviations / 5M / year" reference="ishiYAcc"  chartType="bar" annual = {true} dataSet={true}/>
        <ChartComponent  chart={this.props.decYAgg} name="Deviations / decision / year" reference="decYAcc"  chartType="bar" annual = {true} dataSet={true}/>
        <ChartComponent  chart={this.props.avgClsYAgg} name="Deviations / avg closing / year" reference="avgClsYAcc"  chartType="bar" annual = {true} dataSet={false}/>
        <ChartComponent  chart={this.props.avgClsProcYAgg} name="Avg closing / process / year" reference="avgClsProcYAgg"  chartType="bar" annual = {true} dataSet={true}/>
        <ChartComponent  chart={this.props.avgClsProdYAgg} name="Avg closing / product / year" reference="avgClsProdYAgg"  chartType="bar" annual = {true} dataSet={true}/>
        <ChartComponent  chart={this.props.avgClsLocYAgg} name="Avg closing / locality / year" reference="avgClsLocYAgg"  chartType="bar" annual = {true} dataSet={true}/>
        <ChartComponent  chart={this.props.avgClsEquipYAgg} name="Avg closing / equipment / year" reference="avgClsEquipYAgg"  chartType="bar" annual = {true} dataSet={true}/>
      </div>
    );
  }

}

export default Annual = withTracker(()=>{

  const annual = (obj)=>{
    let data = [];
    obj.map((elem,index)=>{
      //console.log(elem);
      data[elem._id]=obj[index].count;
    })
    //console.log(data);
    return data;
  }

  const calcAgg = (obj)=>{
    let  data =  {};
     obj.map((elem)=>{
      !data[elem.target] ? data[elem.target]=[] : '';
      data[elem.target][elem.month] = elem.count;
      //console.log(elem);
    });
    return data;
  }

  Meteor.subscribe('deviationYAgg');
  Meteor.subscribe('levelYAgg');
  Meteor.subscribe('processYAgg');
  Meteor.subscribe('localityYAgg');
  Meteor.subscribe('equipmentYAgg');
  Meteor.subscribe('productYAgg');
  Meteor.subscribe('typeYAgg');
  Meteor.subscribe('ishikawaYAgg');
  Meteor.subscribe('methodYAgg');
  Meteor.subscribe('decisionYAgg');
  Meteor.subscribe('avgClosingYAgg');
  Meteor.subscribe('avgClosingProcYAgg');
  Meteor.subscribe('avgClosingProdYAgg');
  Meteor.subscribe('avgClosingLocYAgg');
  Meteor.subscribe('avgClosingEquipYAgg');

  //Meteor.subscribe('levelsCount');

  const devYAgg = DeviationYAgg.find({}).fetch();
  let lvlYAgg = LevelYAgg.find({}).fetch();
  let procYAgg = ProcessYAgg.find({}).fetch();
  let locYAgg = LocalityYAgg.find({}).fetch();
  let equpYAgg = EquipmentYAgg.find({}).fetch();
  let prodYAgg = ProductYAgg.find({}).fetch();
  let catYAgg = CategoryYAgg.find({}).fetch();
  let ishiYAgg = IshikawaYAgg.find({}).fetch();
  let methYAgg = MethodYAgg.find({}).fetch();
  let decYAgg = DecisionYAgg.find({}).fetch();
  let avgClsYAgg = AvgClosingYAgg.find({}).fetch();
  let avgClsProcYAgg = AvgClosingProcYAgg.find({}).fetch();
  let avgClsProdYAgg = AvgClosingProdYAgg.find({}).fetch();
  let avgClsLocYAgg = AvgClosingLocYAgg.find({}).fetch();
  let avgClsEquipYAgg = AvgClosingEquipYAgg.find({}).fetch();

  //console.log(avgClsProcYAgg);
  let count=0;
  let devYAcc = devYAgg.map(( elem)=> {
    return {_id: elem._id, count : count = count+=elem.count};
  })



  devYAgg.count = annual(devYAgg);
  devYAcc.count = annual(devYAcc);
  avgClsYAgg.count = annual(avgClsYAgg);

  lvlYAgg = calcAgg(lvlYAgg);
  procYAgg = calcAgg(procYAgg);
  locYAgg = calcAgg(locYAgg);
  equpYAgg = calcAgg(equpYAgg);
  prodYAgg = calcAgg(prodYAgg);
  catYAgg = calcAgg(catYAgg);
  methYAgg = calcAgg(methYAgg);
  ishiYAgg = calcAgg(ishiYAgg);
  decYAgg = calcAgg(decYAgg);

  avgClsProcYAgg = calcAgg(avgClsProcYAgg);
  avgClsProdYAgg = calcAgg(avgClsProdYAgg);
  avgClsLocYAgg = calcAgg(avgClsLocYAgg);
  avgClsEquipYAgg = calcAgg(avgClsEquipYAgg);

  return {
    devYAgg,
    devYAcc,
    lvlYAgg,
    procYAgg,
    locYAgg,
    equpYAgg,
    prodYAgg,
    catYAgg,
    ishiYAgg,
    methYAgg,
    decYAgg,
    avgClsYAgg,
    avgClsProcYAgg,
    avgClsProdYAgg,
    avgClsLocYAgg,
    avgClsEquipYAgg,
  }

})(Annual);
