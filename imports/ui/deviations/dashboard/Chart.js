import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Assets from 'meteor/meteor';
import Chart from 'chart.js';
import palette from 'google-palette';
//import alertify from 'alertify.js';

export default class ChartComponent extends Component {

  constructor(props){
    super(props);
    //this.handleChartClick = this.handleChartClick.bind(this);
    //this.handleChartClick = this.handleChartClick.bind(this);
    //this.deleteChart = this.deleteChart.bind(this);
    this.state = {
      chart : '',
      data:'',
      options:'',
    }

    this.chart = '';
  }
  drawCharts(){
    //console.log(this.props.chart.chart);
     let labels = _.pluck(this.props.chart,'_id');
     let counts;
     if(!this.props.dataSet && !this.props.annual ){
       counts = _.pluck(this.props.chart,'count');
     }else{
       counts = this.props.chart.count;
     }
    //if(typeof chart != 'undefined' && typeof chart[this.props.reference] != 'undefined') chart[this.props.reference].destroy();
    const elem = ReactDOM.findDOMNode(this.refs[this.props.reference]);
    //elem.getContext('2d').clearRect(0, 0, elem.width, elem.height);
     //elem = document.getElementById("state");
     //console.log(_.pluck(this.props.chart,'_id'));
     //console.log(counts);
     if(this.props.annual){
       // labels = labels.map( (elem,index) => {
       //   return moment.monthsShort(elem);
       // })
       labels=['Jan','Feb','Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ];

     }



     let ColorScheme;
      if(this.props.chart.focus != "state"){

        ColorScheme = this.generateColors(labels.length);
     }else {
        ColorScheme = [];
       labels.map( (elem)=> {
         switch (elem) {
           case 'Safe':
             ColorScheme.push('#1eda69');
             break;
           case 'Risky':
             ColorScheme.push('#ffbd29');
             break;
           case 'Critical':
             ColorScheme.push('#ff4545');
             break;
           case 'Unknown':
             ColorScheme.push('#a4a4a4');
             break;

         }
       })

       //console.log( );
     }
    //console.log(counts);

    let data = (!this.props.dataSet)? {
        datasets: [{
            data: counts,
            backgroundColor: ColorScheme  ,
        }],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: labels,
    } :'';

    if(this.props.dataSet){
      let datasets = [];
      const setChart = this.props.chart;
      Object.keys(setChart).map((elem,index)=>{
        let colors = [] ;
        ColorScheme.map((elemC,indexC)=>{
          if(!colors[index]) colors[index] = [];
          colors[index][indexC]=ColorScheme[index];
        })
        //console.log(colors);
        const set = {
          data : setChart[elem],
          backgroundColor: colors[index] ,//ColorScheme  ,
          label : elem,
        }

        datasets.push(set);
      });
      //console.log(datasets);

      data =  {
            datasets: datasets,

            // These labels appear in the legend and in the tooltips when hovering different arcs
            labels: labels,
        } ;
    }
    //console.log(data);
    const options = {
      //onClick : this.props.handleChartClick,
      legend : {
        position : (this.props.chartType=='bar')? false:'right',
      },
      scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
    //console.log(data);
    // if(typeof chart == 'undefined')
    // {
    //   let chart = [];
    //   chart[this.props.reference] = new Chart(elem.getContext("2d"),{
    //    type: this.props.chartType || 'doughnut',
    //    data: data,
    //    options: options,
    //    });
    //    console.log(chart);
    //    //this.setState({chart:chart[this.props.reference]});
    // }
    // else {
    //   //console.log(typeof chart);
    //   console.log(chart);
    //
    // //console.log(this.props.chart.type);
    //  chart[this.props.reference] = new Chart(elem.getContext("2d"),{
    //   type: this.props.chart.type || 'doughnut',
    //   data: data,
    //   options: options,
    //   });
    //
    //   //this.setState({chart:chart[this.props.reference]});
    // }

    const chart =  new Chart(elem.getContext("2d"),{
     type: this.props.chartType || 'doughnut',
     data: data,
     options: options,
     });
    //this.setState({chart,data,options})
    this.chart = chart ;
    //const elem = ReactDOM.findDOMNode(this.refs[this.props.reference]);

  }

  componentDidMount(){
    this.drawCharts();
  }

  componentDidUpdate(){
    this.chart && this.chart.destroy()
    this.drawCharts();
    //this.state.chart && this.state.chart.destroy()
    // const elem = ReactDOM.findDOMNode(this.refs[this.props.reference]);
    // //console.log(elem);
    // const chart =  new Chart(elem.getContext("2d"),{
    //  type: this.props.chart.type || 'doughnut',
    //  data: this.state.data,
    //  options: this.state.options,
    //  });
    //  //this.setState({chart})
    // console.log(this.state.data);
  }

  componentWillUnmount() {
    this.chart && this.chart.destroy()
  }


  generateColors(length){
    const color = (length>8)? 'tol-rainbow':'cb-Set2'; //tol-rainbow
    const seq = palette(color, length);
    const ColorScheme = seq.map((hex) => {
      return '#'+hex;
    });
    return ColorScheme;
  }

  // handleChartClick(event, array){
  //   //console.log(array[0]['_chart']);
  //   if(array[0]){
  //     let activePoints = array[0]['_chart'].getElementsAtEvent(event);
  //     let chartData = activePoints[0]['_chart'].config.data;
  //     let idx = activePoints[0]['_index'];
  //     let label = chartData.labels[idx];
  //     let value = chartData.datasets[0].data[idx];
  //     //console.log(label);
  //     this.props.chartClicked(label);
  //   }
  // }

  // deleteChart(e){
  //   const id = e.target.dataset.id;
  //   alertify.confirm("êtes-vous sûr de vouloir supprimer ce graph ?",() => {
  //     Meteor.call('charts.deleteChart',id,()=>{
  //       alertify.success('Le graph vient d\'être supprimé.');
  //     });
  //   }, () =>{
  //     alertify.log('action annulé.');
  //   });
  // }

  expandDashboard(){
      $('#dashboard-interface').toggleClass('fullscreen');
  }

  render(){
      return(
              <div className="mb-5 mx-auto col-5">
                <label className="col-12">{this.props.name}{/*<i className="fa fa-trash float-right pointer" data-id={this.props.chart.id} onClick={this.deleteChart} ></i>*/}</label>
              <hr/>
               <canvas id={this.props.id} ref={this.props.reference} width="400" height="200"></canvas>
              </div>
            );
  }

}
