import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';


import Select, {Option, OptGroup} from 'rc-select';
import 'rc-select/assets/index.css';

//import Modules from './Modules.js';



// App component - represents the whole app

export  class ExpertiseModal extends Component {


  constructor(props){
    super(props);

    this.state = {
      expert : '',
      experts : [],
    }

   }


   componentDidMount(){
     Meteor.call('role.getUsersInRole',['launch-investigation'],(err,result)=>{
       this.setState({experts: result});
     })
   }

  render() {

    return (
      <div className="modal fade" id="expertiseModal" tabIndex="-1" role="dialog" aria-labelledby="expertiseModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="expertiseModalLabel">Request quality expertise</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body mb-3">
              <div className="offset-1 col-9">
                <label className="ml-3">Estimated level</label>
              <Select
                className="col-12 "
                onSelect={ level  =>{
                  this.setState({level});
                  this.props.onLvl(level);
                } }
                value = {this.state.level}
              >
                <Option  value={"Minor"}>{'Minor'}</Option>
                <Option  value={"Major"}>{'Major'}</Option>
                <Option  value={"Critical"}>{'Critical'}</Option>

              </Select>
              </div>
              <div className="offset-1 col-9 mt-4">
                <label className="ml-3">Select quality expert</label>
              <Select
                className="col-12 "
                onSelect={ expert  => {
                  this.setState({expert : JSON.parse(expert)})
                  this.props.onSelect(JSON.parse(expert))
                  }
                  }
                value = {this.state.expert.username}
              >
                {
                  this.state.experts.map((user,index)=>{
                  return  <Option key={index} value={JSON.stringify(user)}>{user.username}</Option>

                  })
                }
              </Select>
              </div>
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={ this.props.onClick } >Send</button>
        </div>
      </div>
    </div>
    </div>
    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();


//Meteor.subscribe('usersInRoles',['launch-investigation']);
//console.log(Roles.getUsersInRole(['launch-investigation']).fetch());
//console.log(experts)

return {
user: user,
//experts : experts,
};
}
)(ExpertiseModal);
