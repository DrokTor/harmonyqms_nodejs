import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';


import Select, {Option, OptGroup} from 'rc-select';
import 'rc-select/assets/index.css';
import Alert from 'react-s-alert';

//import Modules from './Modules.js';



// App component - represents the whole app

export  class SigningModal extends Component {


  constructor(props){
    super(props);

    this.state = {
      password : '',
      comment : '',
    }

    this.check = this.check.bind(this);
   }

   check(){
     //alert(this.props.signatoryType)
     //console.log(Meteor.user().username)
     Meteor.loginWithPassword(Meteor.user().username, this.state.password, (err)=>{
         if(err){
           Alert.warning('Wrong password !');
         }else {
           Meteor.call('devsConclusions.sign',this.props.signatoryType,this.props.devId,this.state.comment,(err,res)=>{
             if(err){
               alert(err)
             }else {
               //console.log(res);
               this.props.onClick && this.props.onClick();
             }

           })
         }
     })

   }

  render() {

    return (

      <div className="modal fade" id="closeConclusionModal" tabIndex="-1" role="dialog" aria-labelledby="closeDevModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="closeModalLabel">Close Conclusion</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body mb-3">
              <div className="offset-1 col-9">
                <label>Password</label>
                  <input
                    type = "password"
                    className="col-12"
                    //value = {this.state.suspectedRootCauses}
                    onChange={event => this.setState({password : event.target.value})}
                  />
                </div>
              <div className="offset-1 col-9">
              <label>Comment</label>
                <textarea
                  className="col-12"
                  //value = {this.state.suspectedRootCauses}
                  onChange={event => this.setState({comment : event.target.value})}
                />
              </div>
            </div>

            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={ this.check } >Send</button>
            </div>
          </div>
        </div>
      </div>

    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();


Meteor.subscribe('usersInRoles',['approve-deviation']);


return {
user: user,

};
}
)(SigningModal);
