import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';


import Select, {Option, OptGroup} from 'rc-select';
import 'rc-select/assets/index.css';

//import Modules from './Modules.js';



// App component - represents the whole app

export  class LaunchInvestigationModal extends Component {


  constructor(props){
    super(props);

    this.state = {
      investigator : '',
      investigators : [],
      preliminaryLevel : {},
    }
    this.calculateLevel = this.calculateLevel.bind(this);
   }


   componentDidMount(){
     Meteor.call('role.getUsersInRole',['investigate-deviation'],(err,result)=>{
       this.setState({investigators: result});
     })
   }

   calculateLevel(){
     const {severity, occurence, detectability} = this.state.preliminaryLevel;
     if(severity && occurence && detectability  ){
       let risk,level,sev_occ=severity+'-'+occurence,risk_det;
       //console.log(sev_occ);
       switch (sev_occ) {
         case 'Low-Low':
         case 'Medium-Low':
         case 'Low-Medium':
           risk = 'Low';
           break;
         case 'High-Low':
         case 'Medium-Medium':
         case 'Low-High':
           risk = 'Medium';
           break;
         case 'High-Medium':
         case 'Medium-High':
         case 'High-High':
           risk = 'High';
           break;
       }

       risk_det = risk+'-'+detectability;

       switch (risk_det) {
         case 'Low-High':
         case 'Medium-High':
         case 'Low-Medium':
           level = 'Minor';
           break;
         case 'High-High':
         case 'Medium-Medium':
         case 'Low-Low':
           level = 'Major';
           break;
         case 'High-Medium':
         case 'Medium-Low':
         case 'High-Low':
           level = 'Critical';
           break;
       }
       // console.log(risk);
       // console.log(level);
       //alert(level)
       //console.log({severity, occurence, detectability});
       let preliminaryLevel = this.state.preliminaryLevel;
       preliminaryLevel.level = level;
       this.setState({preliminaryLevel},()=>{
         this.props.onLvl(preliminaryLevel);
       });

     }

   }

  render() {

    return (
      <div className="modal fade" id="investigatorModal" tabIndex="-1" role="dialog" aria-labelledby="investigatorModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="investigatorModalLabel">Request investigator</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body mb-3">
              {/* <div className="offset-1 col-9">
              <label className="ml-3">Preliminary rating</label>
              </div> */}

              <div className="row mb-5 border-bottom border-ligh pt-3 pb-5">
                <div className=" col-4">
                  <label className="ml-3">Severity</label>
                <Select
                  className="col-12 "
                  onSelect={ level  =>{
                    let preliminaryLevel = this.state.preliminaryLevel;
                    preliminaryLevel.severity = level;
                    this.setState({preliminaryLevel});
                    //this.props.onLvl(preliminaryLevel);
                    this.calculateLevel();
                  } }
                  value = {this.state.preliminaryLevel.severity}
                >
                  <Option  value={"Low"}>Low</Option>
                  <Option  value={"Medium"}>Medium</Option>
                  <Option  value={"High"}>High</Option>

                </Select>
                </div>
                <div className=" col-4">
                  <label className="ml-3">Occurence</label>
                <Select
                  className="col-12 "
                  onSelect={ level  =>{
                    let preliminaryLevel = this.state.preliminaryLevel;
                    preliminaryLevel.occurence = level;
                    this.setState({preliminaryLevel});
                    //this.props.onLvl(preliminaryLevel);
                    this.calculateLevel();
                  } }
                  value = {this.state.preliminaryLevel.occurence}
                >
                  <Option  value={"Low"}>Low</Option>
                  <Option  value={"Medium"}>Medium</Option>
                  <Option  value={"High"}>High</Option>

                </Select>
                </div>
                <div className=" col-4">
                  <label className="ml-3">Detectability</label>
                <Select
                  className="col-12 "
                  onSelect={ level  =>{
                    let preliminaryLevel = this.state.preliminaryLevel;
                    preliminaryLevel.detectability = level;
                    this.setState({preliminaryLevel});
                    //this.props.onLvl(preliminaryLevel);
                    this.calculateLevel();
                  } }
                  value = {this.state.preliminaryLevel.detectability}
                >
                  <Option  value={"Low"}>Low</Option>
                  <Option  value={"Medium"}>Medium</Option>
                  <Option  value={"High"}>High</Option>

                </Select>
                </div>
              </div>
              {/* <div className="offset-1 col-9">
                <label className="ml-3">Preliminary rating</label>
              <Select
                className="col-12 "
                onSelect={ level  =>{
                  let preliminaryLevel = {};
                  preliminaryLevel.level = level;
                  this.setState({preliminaryLevel});
                  this.props.onLvl(preliminaryLevel);
                } }
                value = {this.state.preliminaryLevel.level}
              >
                <Option  value={"Minor"}>Minor</Option>
                <Option  value={"Medium"}>Medium</Option>
                <Option  value={"Major"}>Major</Option>

              </Select>
              </div> */}
              <div className="offset-1 col-9 mt-4 pb-3">
                <label className="ml-3">Select investigator</label>
              <Select
                className="col-12 "
                onSelect={ investigator  => {
                  this.setState({investigator : JSON.parse(investigator)})
                  this.props.onSelect(JSON.parse(investigator))
                  }
                  }
                value = {this.state.investigator.username}
              >
                {
                  this.state.investigators.map((user,index)=>{
                  return  <Option key={index} value={JSON.stringify(user)}>{user.username}</Option>

                  })
                }
              </Select>
              </div>
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={ this.props.onClick } >Send</button>
        </div>
      </div>
    </div>
    </div>
    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();


//Meteor.subscribe('usersInRoles',['launch-investigation']);
//console.log(Roles.getUsersInRole(['launch-investigation']).fetch());
//console.log(investigators)

return {
user: user,
//investigators : investigators,
};
}
)(LaunchInvestigationModal);
