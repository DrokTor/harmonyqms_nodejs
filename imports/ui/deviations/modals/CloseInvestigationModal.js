import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';


import Select, {Option, OptGroup} from 'rc-select';
import 'rc-select/assets/index.css';

//import Modules from './Modules.js';



// App component - represents the whole app

export  class CloseInvestigationModal extends Component {


  constructor(props){
    super(props);

    this.state = {
      investigator : '',
      investigators : [],
      finalLevel : {},
    }
      this.calculateLevel = this.calculateLevel.bind(this);
   }


   componentDidMount(){
     Meteor.call('role.getUsersInRole',['investigate-deviation'],(err,result)=>{
       this.setState({investigators: result});
     })
   }
   calculateLevel(){
     const {severity, occurence, detectability} = this.state.finalLevel;
     if(severity && occurence && detectability  ){
       let risk,level,sev_occ=severity+'-'+occurence,risk_det;
       //console.log(sev_occ);
       switch (sev_occ) {
         case 'Low-Low':
         case 'Medium-Low':
         case 'Low-Medium':
           risk = 'Low';
           break;
         case 'High-Low':
         case 'Medium-Medium':
         case 'Low-High':
           risk = 'Medium';
           break;
         case 'High-Medium':
         case 'Medium-High':
         case 'High-High':
           risk = 'High';
           break;
       }

       risk_det = risk+'-'+detectability;

       switch (risk_det) {
         case 'Low-High':
         case 'Medium-High':
         case 'Low-Medium':
           level = 'Minor';
           break;
         case 'High-High':
         case 'Medium-Medium':
         case 'Low-Low':
           level = 'Major';
           break;
         case 'High-Medium':
         case 'Medium-Low':
         case 'High-Low':
           level = 'Critical';
           break;
       }
       // console.log(risk);
       // console.log(level);
       //alert(level)
       //console.log({severity, occurence, detectability});
       let finalLevel = this.state.finalLevel;
       finalLevel.level = level;
       this.setState({finalLevel},()=>{
         this.props.onLvl(finalLevel);
       });

     }

   }

  render() {

    return (
      <div className="modal fade" id="closeInvestigationModal" tabIndex="-1" role="dialog" aria-labelledby="closeInvestigationModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="closeInvestigationModalLabel">{this.props.onReason? 'Skip investigation' : 'Investigation closing'}</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body mb-3">
              <div className="row mb-5 mt-5 pt-3 pb-3">
                <div className=" col-4">
                  <label className="ml-3">Severity</label>
                <Select
                  className="col-12 "
                  onSelect={ level  =>{
                    let finalLevel = this.state.finalLevel;
                    finalLevel.severity = level;
                    this.setState({finalLevel});
                    //this.props.onLvl(finalLevel);
                    this.calculateLevel();
                  } }
                  value = {this.state.finalLevel.severity}
                >
                  <Option  value={"Low"}>Low</Option>
                  <Option  value={"Medium"}>Medium</Option>
                  <Option  value={"High"}>High</Option>

                </Select>
                </div>
                <div className=" col-4">
                  <label className="ml-3">Occurence</label>
                <Select
                  className="col-12 "
                  onSelect={ level  =>{
                    let finalLevel = this.state.finalLevel;
                    finalLevel.occurence = level;
                    this.setState({finalLevel});
                    //this.props.onLvl(finalLevel);
                    this.calculateLevel();
                  } }
                  value = {this.state.finalLevel.occurence}
                >
                  <Option  value={"Low"}>Low</Option>
                  <Option  value={"Medium"}>Medium</Option>
                  <Option  value={"High"}>High</Option>

                </Select>
                </div>
                <div className=" col-4">
                  <label className="ml-3">Detectability</label>
                <Select
                  className="col-12 "
                  onSelect={ level  =>{
                    let finalLevel = this.state.finalLevel;
                    finalLevel.detectability = level;
                    this.setState({finalLevel});
                    //this.props.onLvl(finalLevel);
                    this.calculateLevel();
                  } }
                  value = {this.state.finalLevel.detectability}
                >
                  <Option  value={"Low"}>Low</Option>
                  <Option  value={"Medium"}>Medium</Option>
                  <Option  value={"High"}>High</Option>

                </Select>
                </div>
              </div>
              {this.props.onReason &&
                <div className=" col-12">
                  <label className=" ">Reason</label>
                  <textarea
                    className=" col-12"
                    onChange = {event => this.props.onReason(event.target.value) }
                  />
                </div>
              }

        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={ this.props.onClick } >Send</button>
        </div>
      </div>
    </div>
    </div>
    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();


//Meteor.subscribe('usersInRoles',['launch-investigation']);
//console.log(Roles.getUsersInRole(['launch-investigation']).fetch());
//console.log(investigators)

return {
user: user,
//investigators : investigators,
};
}
)(CloseInvestigationModal);
