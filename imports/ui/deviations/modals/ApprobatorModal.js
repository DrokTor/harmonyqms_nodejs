import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';


import Select, {Option, OptGroup} from 'rc-select';
import 'rc-select/assets/index.css';

//import Modules from './Modules.js';



// App component - represents the whole app

export  class ApprobatorModal extends Component {


  constructor(props){
    super(props);

    this.state = {
      approbator : '',
      approbators : [],
    }

  }

  componentDidMount(){
    Meteor.call('role.getUsersInRole',['approve-deviation'],(err,result)=>{
      this.setState({approbators: result});
    })
  }

  render() {

    return (
      <div className="modal fade" id="approvalModal" tabIndex="-1" role="dialog" aria-labelledby="approvalModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="approvalModalLabel">Approval</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body mb-3">
              <div className="offset-1 col-10">
                <label className="ml-3">Select an Approbator</label>
                <Select
                  className="col-12 "
                  onSelect={ approbator  => {
                    this.setState({approbator : JSON.parse(approbator)})
                    this.props.onSelect(JSON.parse(approbator))
                    }
                    }
                  value = {this.state.approbator.username}
                >
                  {
                    this.state.approbators.map((user,index)=>{
                    return  <Option key={index} value={JSON.stringify(user)}>{user.username}</Option>

                    })
                  }
                </Select>
              </div>
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={ this.props.onClick } >Send</button>
        </div>
      </div>
    </div>
    </div>
    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();


//Meteor.subscribe('usersInRoles',['approve-deviation']);

return {
user: user,

};
}
)(ApprobatorModal);
