import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';


import Select, {Option, OptGroup} from 'rc-select';
import 'rc-select/assets/index.css';

//import Modules from './Modules.js';



// App component - represents the whole app

export  class DiscardDeviationModal extends Component {


  constructor(props){
    super(props);

    this.state = {
      approbator : '',
    }

    this.loadPage = this.loadPage.bind(this);
  }

  loadPage(page){
    this.setState({page});
  }

  render() {

    return (

      <div className="modal fade" id="discardDeviationModal" tabIndex="-1" role="dialog" aria-labelledby="discardDevModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="discardModalLabel">Discard deviation</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body mb-3">
              <label>Reason</label>
                <textarea
                  className="col-12"
                  //value = {this.state.suspectedRootCauses}
                  onChange={event => this.props.onChange(event)}
                />
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={ this.props.onClick } >Send</button>
            </div>
          </div>
        </div>
      </div>

    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();


Meteor.subscribe('usersInRoles',['approve-deviation']);


return {
user: user,

};
}
)(DiscardDeviationModal);
