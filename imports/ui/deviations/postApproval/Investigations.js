import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';



import {DevsDeclarations} from '../../../api/devsDeclarations.js';
//import Login from './Login.js';
//import Modules from './Modules.js';



// App component - represents the whole app

export  class Expertises extends Component {


  constructor(props){
    super(props);

    this.state = {
      page : 'modules',
    }

   }


  render() {

    return (

      <div className="col-9 core   ">
        <div className="row">
          <h3>Deviations waiting for your investigation ({this.props.investigations.length})</h3>
          <table className="col-12  mt-5 ">
            <thead><tr><th className="col-3">Title</th><th className="col-3">Detection date</th><th className="col-3">Type</th><th className="col-3">Planification</th></tr></thead>
            <tbody>
          {   this.props.investigations ?
            (() => {return this.props.investigations.map((item,index)=>{
              return <tr key={index} className="pointer" onClick={()=>{this.props.onClick('viewDeviation',item,item._id)}} ><td className="col-3">{item.title}</td><td className="col-3">{moment(item.detectionDate).format('DD-MM-YYYY')}</td><td className="col-3">{item.type}</td><td>{item.planification}</td></tr>
            })})()
            :''
          }
          </tbody>
          </table>
        </div>
      </div>

    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();

// if( Roles.userIsInRole(user,['launch-investigation']) )
// {
//   Meteor.subscribe('investigations');
// }

return {
user: user,
investigations:  Roles.userIsInRole(Meteor.userId(),['investigate-deviation'])? DevsDeclarations.find({"investigation.investigatorId":Meteor.userId(), state: 'Investigation'}).fetch():0,
};
}
)(Expertises);
