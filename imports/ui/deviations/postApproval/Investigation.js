import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';

import DatePicker from 'react-date-picker';
import TimePicker from 'react-time-picker';
import Select, {Option, OptGroup} from 'rc-select';
import 'rc-select/assets/index.css';

import Alert from 'react-s-alert';
import ReactLoading from 'react-loading';


//import Header from './Header.js';
//import Login from './Login.js';
//import Modules from './Modules.js';
import { Processes } from '../../../api/process';
import { Products } from '../../../api/products';
import { Equipments } from '../../../api/equipments';
import { Methods } from '../../../api/methods';
import { Localities } from '../../../api/localities';
import { DevsInvestigations } from '../../../api/devsInvestigations';

import  ApprobatorModal  from '../modals/ApprobatorModal';
import  ExpertiseModal  from '../modals/ExpertiseModal';
import  MissingInfoModal  from '../modals/MissingInfoModal';
import  CloseInvestigationModal  from '../modals/CloseInvestigationModal';
//import  CheckExpertModal  from '../modals/CheckExpertModal';
//import  CloseDeviationModal  from '../modals/CloseDeviationModal';
import  LaunchInvestigationModal  from '../modals/LaunchInvestigationModal';
import  SkipInvestigationModal  from '../modals/SkipInvestigationModal';

import  ViewInvestigation  from './ViewInvestigation';


// App component - represents the whole app

export  class InvestigationInterface extends Component {


  constructor(props){
    super(props);
    //console.log(Meteor.user());
    this.draft = {
      _id:'',
      //userId: Meteor.userId(),
      deviation_id : '',
      method : '',
      ishikawa : [],
      identifiedRootCauses : '',
      history : '',
      processReview : '',
      meterialReview : '',
      batchReview : '',
      equipmentReview : '',
      complianceReview : '',
      actionsReview : '',
      impactReview : '',
      gmpDocReview : '',
      productQualityReview  : '',
      additionalAnalysis : '',
      conclusion : '',

    }

    this.state = {...this.draft};

    this.launchInvestigation = this.launchInvestigation.bind(this);
    this.save = this.save.bind(this);
    this.closeInvestigation = this.closeInvestigation.bind(this);
    this.skipInvestigation = this.skipInvestigation.bind(this);
    this.checkExpert = this.checkExpert.bind(this);
    //console.log(this.props.declaration);
  }

  componentWillReceiveProps(nextProps) {
    //console.log(this.draft);
    // This lifecycle resets the "draft" email value in state,
    // Whenever a new "commited" email value is passed in props.
    // The downside of this is that the parent component has
    // no way to reset state back to the original props value
    // (at least not without rendering multiple times).
    // if (nextProps.email !== this.props.email) {
    //   this.setState({ email: nextProps.email });
    // }
    //console.log((new Date())+"---",nextProps.investigation );
    //console.log(this.props.investigation);
    if(nextProps.investigation != '' )
    {
      this.setState(nextProps.investigation)
      //this.setState({deviationId : this.props.declaration._id});
    }
    else{
      this.setState(this.draft);
    }
    //console.log(this.props.declaration);
    //this.props.log('ttttt')
    //console.log(nextProps.investigation);
  }



  save(){
    //console.log(this.state);
    Meteor.call('devsInvestigations.sendForInvestigation',this.state,null,true,(err,result)=>{
        if(result){
          Alert.info('Investigation saved.');
        }
      })
  }

  checkExpert(){
    //console.log(this.state);
    const qeId = this.props.investigation.investigation.qualityExpert;
    Meteor.call('devsInvestigations.checkExpert',qeId,(err,result)=>{
        if(result){
          Alert.info('Email sent to '+result+'.');
          this.setState({checkExpertLoading : false});
        }
      })
      this.setState({checkExpertLoading : true});
  }

  launchInvestigation(){
    Meteor.call('devsInvestigations.sendForInvestigation',this.state,this.props.declaration._id,(err,result)=>{
      //console.log(result);
      if(result){
        this.setState({_id : result});
        Meteor.call('devsDeclarations.setState',this.props.declaration._id,'Investigation',(err, result)=>{
          Alert.success('Investigation launched.');
          //this.setState({_id:result})
          Meteor.call('devsInvestigations.sendInvestigatorEmail',this.props.declaration._id,this.state.investigation.investigatorId,(err,res)=>{
            if(res)
            Alert.info('Email sent to "'+res+'".');
          })
          //console.log(result);
          // const declaration = this.props.declaration;
          // declaration.state = 'Investigation';
          // this.props.loadContent('viewDeviation',declaration);
        })
      }
    })
  }

  closeInvestigation(){
    //console.log(this.state);
    Meteor.call('devsInvestigations.closeInvestigation',this.state,(err,result)=>{
      //console.log(result);
      if(result){
        this.setState({_id : result});
        Meteor.call('devsDeclarations.setState',this.state.deviation_id,'Conclusion',(err, result)=>{
          Alert.success('Investigation closed.');
          Meteor.call('devsConclusions.launchConclusion',this.state.deviation_id,(err,res)=>{

          })
          //this.setState({_id:result})
            // Meteor.call('devsInvestigations.sendInvestigatorEmail',this.state.investigation.investigatorId,(err,res)=>{
            //   if(res)
            //   Alert.info('Email sent to "'+res+'".');
            // })

        })
      }
    })
  }

  skipInvestigation(){
    Meteor.call('devsInvestigations.skipInvestigation',this.props.devId,this.state,(err,result)=>{
      //console.log(result);
      if(result){
        //this.setState({_id : result});
        Meteor.call('devsDeclarations.setState',this.props.devId,'Conclusion',(err, result)=>{
          Alert.success('Investigation closed.');
          Meteor.call('devsConclusions.launchConclusion',this.props.devId,(err,res)=>{

          })
          //this.setState({_id:result})
            // Meteor.call('devsInvestigations.sendInvestigatorEmail',this.state.investigation.investigatorId,(err,res)=>{
            //   if(res)
            //   Alert.info('Email sent to "'+res+'".');
            // })

        })
      }
    })
  }

  render() {

    return (

      <div className="col-12  core">
        {(()=>{
          //console.log(this.props.declaration.state);
        if(this.props.declaration.state == 'Investigation')
        {
          return(
        <div>
        <h3 className="collapseLabel col-md-12" data-toggle="collapse" data-target="#identification">Identification </h3>
        <div id="identification" className={'collapse form_field row  '+   ((this.props.declaration.state === 'Approval') ? 'show':'') } >
          <div className="col-6 mb-3  ">
            <label className="col-7">Investigation method</label>
            <Select
              className="col-12 "
              onSelect={method => this.setState({method})}
              value = {this.state.method}
            >
              {
                this.props.methods.map((method,index)=>{
                return  <Option key={index} value={method.name}>{method.name}</Option>

                })
              }
            </Select>
          </div>
          <div className="col-6 ">
            <label className="col-12" >Ishikawa</label>
            <Select
              className="col-12 "
              onSelect={ishi => {
                let ishikawa = this.state.ishikawa;
                ishikawa.push(ishi);
                this.setState({ishikawa})}
              }
              onDeselect={ishi => {
                let ishikawa = this.state.ishikawa;
                ishikawa = _.without(ishikawa, _.findWhere(ishikawa, ishi));
                this.setState({ishikawa})}
              }
              value = {this.state.ishikawa}
              multiple = {true}
            >
              <Option  value={'Manpower'}>{'Manpower'}</Option>
              <Option  value={'Material'}>{'Material'}</Option>
              <Option  value={'Method'}>{'Method'}</Option>
              <Option  value={'Milieu'}>{'Milieu'}</Option>
              <Option  value={'Machine'}>{'Machine'}</Option>
            </Select>
          </div>
          <div className="col-6 mb-3 ">
            <label className="col-12">Identified root causes</label>
            <div className="col-12">
              <textarea
              className="col-12"
              type = "text"
              value = {this.state.identifiedRootCauses}
              onChange={event => this.setState({identifiedRootCauses: event.target.value})}
            />
            </div>
          </div>
          <div className="col-6 mb-3 ">
            <label className="col-12 ">History</label>
            <div className="col-12">
              <textarea
              className="col-12 "
              type = "text"
              value = {this.state.history}
              onChange={event => this.setState({history: event.target.value})}
            />
            </div>
          </div>
        </div>

        {/* reviews */}
        <h3 className="collapseLabel col-md-12  mt-5" data-toggle="collapse" data-target="#reviews">Reviews </h3>
        <div id="reviews" className={'collapse form_field row no-gutters '+   ((this.props.declaration.state === 'Approval') ? 'show':'') }>
          <div className="col-6 mb-3 ">
            <label className="col-12 ">Process Review</label>
            <div className="col-12">
              <textarea
              className="col-12 "
              type = "text"
              value = {this.state.processReview}
              onChange={event => this.setState({processReview: event.target.value})}
            />
            </div>
          </div>
          <div className="col-6 mb-3 ">
            <label className="col-12 ">Material Review</label>
            <div className="col-12">
              <textarea
              className="col-12 "
              type = "text"
              value = {this.state.meterialReview}
              onChange={event => this.setState({meterialReview: event.target.value})}
            />
            </div>
          </div>
          <div className="col-6 mb-3 ">
            <label className="col-12 ">Batch Review</label>
            <div className="col-12">
              <textarea
              className="col-12 "
              type = "text"
              value = {this.state.batchReview}
              onChange={event => this.setState({batchReview: event.target.value})}
            />
            </div>
          </div>
          <div className="col-6 mb-3 ">
            <label className="col-12 ">Equipment Review</label>
            <div className="col-12">
              <textarea
              className="col-12 "
              type = "text"
              value = {this.state.equipmentReview}
              onChange={event => this.setState({equipmentReview: event.target.value})}
            />
            </div>
          </div>
          <div className="col-6 mb-3 ">
            <label className="col-12 ">Regulatory compliance</label>
            <div className="col-12">
              <textarea
              className="col-12 "
              type = "text"
              value = {this.state.complianceReview}
              onChange={event => this.setState({complianceReview: event.target.value})}
            />
            </div>
          </div>
          <div className="col-6 mb-3 ">
            <label className="col-12 ">Actions Review</label>
            <div className="col-12">
              <textarea
              className="col-12 "
              type = "text"
              value = {this.state.actionsReview}
              onChange={event => this.setState({actionsReview: event.target.value})}
            />
            </div>
          </div>
          <div className="col-6 mb-3 ">
            <label className="col-12 ">Impact Review</label>
            <div className="col-12">
              <textarea
              className="col-12 "
              type = "text"
              value = {this.state.impactReview}
              onChange={event => this.setState({impactReview: event.target.value})}
            />
            </div>
          </div>
          <div className="col-6 mb-3 ">
            <label className="col-12 ">GMP documents Review</label>
            <div className="col-12">
              <textarea
              className="col-12 "
              type = "text"
              value = {this.state.gmpDocReview}
              onChange={event => this.setState({gmpDocReview: event.target.value})}
            />
            </div>
          </div>
          <div className="col-6 mb-3 ">
            <label className="col-12 ">Product Quality Review</label>
            <div className="col-12">
              <textarea
              className="col-12 "
              type = "text"
              value = {this.state.productQualityReview}
              onChange={event => this.setState({productQualityReview: event.target.value})}
            />
            </div>
          </div>


        </div>

        {/* Final analysis */}
        <h3 className="collapseLabel col-md-12  mt-5" data-toggle="collapse" data-target="#finalAnalysis">Final analysis </h3>
        <div id="finalAnalysis" className={'collapse form_field row  '+   ((this.props.declaration.state === 'Approval') ? 'show':'') }>
          <div className="col-6 mb-3 ">
            <label className="col-12 ">Addtional analysis</label>
            <div className="col-12">
              <textarea
              className="col-12 "
              type = "text"
              value = {this.state.additionalAnalysis}
              onChange={event => this.setState({additionalAnalysis: event.target.value})}
              />
            </div>
          </div>
          <div className="col-6 mb-3 ">
            <label className="col-12 ">Conclusion</label>
            <div className="col-12">
              <textarea
              className="col-12 "
              type = "text"
              value = {this.state.conclusion}
              onChange={event => this.setState({conclusion: event.target.value})}
              />
            </div>
          </div>
        </div>


        {this.state.state == 'Approval' ?
        <div className="row mb-5 mt-5">
          <div className="col-4 offset-2" >
            <button className="btn btn-success  col-12 p-3" data-toggle="modal" data-target="#approvalModal">Launch for Approval</button>
          </div>
          <div className="col-4" >
            <button className="btn btn-primary col-12 p-3" onClick={this.save} >Save</button>
          </div>
        </div>
        :
        ''}
        <div className="row mb-5 mt-5 ">
            { Roles.userIsInRole(Meteor.userId(),['close-investigation']) ?
              <div className="offset-2 col-4" >
                <button className="btn btn-success col-12 p-3  " data-toggle="modal" data-target="#closeInvestigationModal">Close investigation</button>
              </div>
              :''
            }
            { !Roles.userIsInRole(Meteor.userId(),['close-investigation']) ?
              <div className="offset-2 col-4" >
                <button className="btn btn-info col-12 p-3  " data-toggle="modal" data-target="#checkExpertModal" onClick={this.checkExpert} >
                  Check with Quality expert
                  { this.state.checkExpertLoading && <ReactLoading className="mx-auto" type={'spin'} color={'white'} height={20} width={20} /> }
                </button>
                </div>
                :''
              }
          <div className="col-4" >
            <button className="btn btn-primary col-12 p-3" onClick={this.save} >Save</button>
          </div>
        </div>

        {/* <!-- Modal --> */}


                {/* <CheckExpertModal
                  onClick={ this.checkExpert }
                  /> */}

                <CloseInvestigationModal /*onSelect={ expert => {
                  let expertise = { ...this.state.approbation };
                  expertise.expertId = expert._id;
                  this.setState({expertise : expertise })}
                }*/
                  onClick={ this.closeInvestigation }
                  onLvl={ level => this.setState({level, finalLevel: level}) } />

                </div>)
              }else if(this.props.declaration.state == 'Declaration')
              { return(
                 <div className="row mt-5 ">
                  <div className="offset-1 col-5  mt-5">
                    <button  className="offset-2 col-10 p-3  btn btn-primary btn-large" data-toggle="modal" data-target="#investigatorModal">
                      Launch investigation
                    </button>
                    <LaunchInvestigationModal onSelect={ investigator => {
                      let investigation = {}
                      investigation.investigatorId = investigator._id;
                      this.setState({investigation : investigation })}
                    } onClick={ this.launchInvestigation }
                      onLvl={ level => this.setState({ preliminaryLevel: level}) } />
                  </div>
                  <div className="  col-5 mt-5" >
                    <button className="col-10  p-3 btn btn-danger btn-large"  data-toggle="modal" data-target="#closeInvestigationModal">
                      Skip investigation
                    </button>
                    <CloseInvestigationModal
                      onClick={ this.skipInvestigation }
                      onLvl={ level => this.setState({level, finalLevel: level}) }
                      onReason = { reason => this.setState({reason}) }
                     />
                  </div>
                </div>)
              }else if(( ['Conclusion','Signing','Closed'].includes(this.props.declaration.state) ) && this.props.investigation) {
                        //console.log(this.state);
                return   <ViewInvestigation investigation = {this.props.investigation }/>
              }
            } )()
              }


      </div>

    );

  }

}


export default Investigation = withTracker( ({ id,loadContent }) => {

//console.log(id);
//console.log(loadContent);
  // const log = (log)=>{
  //   console.log(log);
  // }
const user=Meteor.userId();
//console.log(Meteor.user());
Meteor.subscribe('process');
Meteor.subscribe('locality');
Meteor.subscribe('equipment');
Meteor.subscribe('method');
Meteor.subscribe('product');

Meteor.subscribe('devsInvestigations',id);
Meteor.subscribe('usersInRoles',['approve-deviation']);
//console.log(id);
//console.log(DevsInvestigations.findOne({deviation_id : id}));
//console.log(Roles.getUsersInRole(['approve-deviation'],null).fetch());
return {
user: user,
devId: id,
processes : Processes.find({}).fetch(),
localities : Localities.find({}).fetch(),
methods : Methods.find({}).fetch(),
equipments : Equipments.find({}).fetch(),
products : Products.find({}).fetch(),
investigation : DevsInvestigations.findOne({deviation_id : id}),
};
}
)(InvestigationInterface);
