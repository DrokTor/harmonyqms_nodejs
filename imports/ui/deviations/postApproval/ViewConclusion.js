import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';

import DatePicker from 'react-date-picker';
import TimePicker from 'react-time-picker';
import Select, {Option, OptGroup} from 'rc-select';
import 'rc-select/assets/index.css';
import classNames from 'classnames';

import Alert from 'react-s-alert';

import {DevsConclusions} from '../../../api/devsConclusions';


// App component - represents the whole app

export  class ViewConclusionInterface extends Component {

  constructor(props){
    super(props);

    this.state = {
    }
  }

  // componentDidMount(){
  //   //console.log(this.props.investigation);
  //     if(typeof this.props.investigation.investigation != 'undefined')
  //     {
  //       Meteor.call('user.findOne',this.props.investigation.investigation.investigatorId,(err,result)=>{
  //       if(result)
  //       {
  //         this.setState({investigator : result.username});
  //       }
  //     })
  //     }
  //     // console.log(this.props.investigation);
  // }

  render() {

    return (

      <div className="col-12  core">

          <h3 className="collapseLabel col-md-12  mt-5"  >Decision </h3>

          <div className="row col-12" >
            {
              (()=>{
                let c = 0;
                const decision = this.props.conclusion.decision;
                //console.log(this.props.conclusion);
                return decision? decision.map((item,index)=>{
                  c++;
                  //console.log(item);
                  return(
                    <div key={index} className={'row col-12 mb-3 p-3'+(c%2==0? ' bg-light':'' )}>
                      <label className="col-4" >Decision for batch n° {item.batch}</label>
                      <div className="col-8" >{item.action}</div>
                    </div>
                  )
                }) : '';
              })()
            }
          </div>
          <h3 className="collapseLabel col-md-12  mt-5"  >Conclusion </h3>

          <div className="row p-3  col-12   ">
            <label className="col-4" >Comment</label>
            <p className="col-8">
              {this.props.conclusion.comment}
            </p>
          </div>
          <div className="row p-3  col-12   bg-light">
            <label className="col-4" >Final decision</label>
            <p className="col-8">
              {this.props.conclusion.finalDecision}
            </p>
          </div>


      </div>

     )


  }

}


export default ViewConclusion = withTracker( (devId) => {
const user=Meteor.userId();

//console.log(devId);
Meteor.subscribe('usersInRoles',['approve-deviation']);
Meteor.subscribe('devsConclusions',devId);

return {
user: user,
//conclusion : DevsConclusions.findOne({'deviation_id' : devId}),
};
}
)(ViewConclusionInterface);
