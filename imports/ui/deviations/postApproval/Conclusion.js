import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';

import { Link } from 'react-router-dom'

import DatePicker from 'react-date-picker';
import TimePicker from 'react-time-picker';
import Select, {Option, OptGroup} from 'rc-select';
import 'rc-select/assets/index.css';

import Alert from 'react-s-alert';
//import Header from './Header.js';
//import Login from './Login.js';
//import Modules from './Modules.js';
import { Processes } from '../../../api/process';
import { Products } from '../../../api/products';
import { Equipments } from '../../../api/equipments';
import { Methods } from '../../../api/methods';
import { Localities } from '../../../api/localities';
import { DevsConclusions } from '../../../api/devsConclusions';

import  ApprobatorModal  from '../modals/ApprobatorModal';
import  ExpertiseModal  from '../modals/ExpertiseModal';
import  MissingInfoModal  from '../modals/MissingInfoModal';
import  CloseInvestigationModal  from '../modals/CloseInvestigationModal';
//import  CloseDeviationModal  from '../modals/CloseDeviationModal';
import  SigningModal  from '../modals/SigningModal';

import  ViewConclusion  from './ViewConclusion';
import  Signatories  from './Signatories';


// App component - represents the whole app

export  class ConclusionInterface extends Component {


  constructor(props){
    super(props);
    //console.log(Meteor.user());
    this.draft = {
      _id:'',
      //userId: Meteor.userId(),
      deviation_id : '',
      decision : [],
      comment : '',
      finalDecision : '',

    }

    this.state = {...this.draft};

    this.save = this.save.bind(this);
    this.createCapa = this.createCapa.bind(this);
    this.closeConclusion = this.closeConclusion.bind(this);
    //console.log(this.props.declaration);
  }

  componentWillReceiveProps(nextProps) {
    //console.log(this.draft);
    // This lifecycle resets the "draft" email value in state,
    // Whenever a new "commited" email value is passed in props.
    // The downside of this is that the parent component has
    // no way to reset state back to the original props value
    // (at least not without rendering multiple times).
    // if (nextProps.email !== this.props.email) {
    //   this.setState({ email: nextProps.email });
    // }
    //console.log((new Date())+"---",nextProps.investigation );
    //console.log(this.props.investigation);
    if(nextProps.investigation != '' )
    {
      this.setState(nextProps.conclusion)
      //this.setState({deviationId : this.props.declaration._id});
    }
    else{
      this.setState(this.draft);
    }
    //console.log(this.props.declaration);
    //this.props.log('ttttt')
    //console.log(nextProps.conclusion);

  }



  save(){
    //console.log(this.state);
    Meteor.call('devsConclusions.saveConclusion',this.state,this.props.declaration._id,(err,result)=>{
        if(result){
          Alert.info('Conclusion saved.');
        }
      })
  }

  createCapa(){

  }


   closeConclusion(){
     // Meteor.call('devsConclusions.saveConclusion',this.state,this.props.declaration._id,(err,result)=>{
     // })
       Meteor.call('devsDeclarations.setState',this.props.declaration._id,'Signing',(err, result)=>{
         // Meteor.call('devsConclusions.sign','expert',this.props.devId,this.state,(err,res)=>{
         //
         // })
         Alert.success('Conclusion closed.');

       })
   }





  render() {
    //console.log(this.props.conclusion && this.props.conclusion.finalDecision);
    //console.log(this.state.finalDecision);
    if(this.props.declaration.state == 'Conclusion')
    {return (

      <div className="col-12  core">
        {
            this.state.relations &&
            <div>
              {Object.keys(this.state.relations).map((elem,index)=>{

                return <React.Fragment>
                 {/* <label className="mr-2">{elem} : </label> */}
                 {
                   this.state.relations[elem].map((el,ind)=>{
                     return <Link to={'/capas/view/'+el} target='_blank' className="mr-2"><span className="badge badge-info p-2">{'capa '+(ind+1)}</span></Link>
                    })
                  }
                </React.Fragment>
              })
            }
            </div>
        }



        {/* Final analysis */}
        <h3 className="collapseLabel col-md-12  mt-5" data-toggle="collapse" data-target="#finalAnalysis">Decision </h3>
        <div id="finalAnalysis" className={'collapse form_field row  '+   ((this.props.declaration.state === 'Conclusion') ? 'show':'') }>
          <div className="row col-12">
            { (()=>{

                return this.props.declaration.batch.map((item,index)=>{
                  return <div key={index} className="col-3 mb-3 ">
                    <label className="col-12 ">Decision for batch n°{item}</label>
                    <Select
                      className="col-12 "
                      onSelect={decis => {
                        let obj = this.state.decision;
                        //decision.item? (decision[item].decision = decis) : (decision[item] = {decision : decis});
                        //decision.push({batch:item, action : decis});

                        let decision  = obj.find((o, i) => {
                            if (o.batch === item) {
                                obj[i] = { batch: item, action: decis };
                                return true; // stop searching
                            }
                        });

                        !decision && obj.push({batch:item, action : decis});
                        this.setState({decision: obj})
                        //console.log(this.state.decision[item]);
                        }
                      }

                      value = {  (()=>{const elm = this.state.decision.find(obj => {
                                        return obj.batch === item ;
                                        })
                                  return elm && elm.action;
                                } )() }

                        >
                      <Option  value={'Accepted'}>{'Accepted'}</Option>
                      <Option  value={'Rejected'}>{'Rejected'}</Option>
                      <Option  value={'To be treated'}>{'To be treated'}</Option>
                      <Option  value={'Partially accepted'}>{'Partially accepted'}</Option>
                      <Option  value={'Not applicable'}>{'Not applicable'}</Option>
                    </Select>
                  </div>
                })
              })()
            }
          </div>

          <div className="col-12 mb-3 ">
            <label className="col-12 ">Comment</label>
            <div className="col-12">
              <input
              className="col-12 "
              type = "text"
              value = {this.state.comment}
              onChange={event => this.setState({comment: event.target.value})}
              />
            </div>
          </div>
          <div className="col-12 mb-3 ">
            <label className="col-12 ">Final decision</label>
            <div className="col-12">
              <input
              className="col-12 "
              type = "text"
              value = {this.state.finalDecision}
              onChange={event => this.setState({finalDecision: event.target.value})}
              />
            </div>
          </div>
        </div>



        <div className="row mb-5 mt-5">
          <div className="col-4  " >
            <button className="btn btn-success  col-12 p-3"
               data-toggle="modal" data-target="#closeConclusionModal"
               onClick={()=>this.save()} >Close & Sign</button>
          </div>
          <div className="col-4" >
            <button className="btn btn-primary col-12 p-3" onClick={this.save} >Save</button>
          </div>
          <div className="col-4" >
            <Link to={"/capas/new?source=Deviation&sourceId="+this.state.deviation_id} target='_blank'>
            <button className="btn btn-info col-12 p-3" onClick={this.createCapa} >Create CAPA</button>
            </Link>
          </div>
        </div>


        {/* <!-- Modal --> */}


                <SigningModal /*onSelect={ expert => {
                  let expertise = { ...this.state.approbation };
                  expertise.expertId = expert._id;
                  this.setState({expertise : expertise })}
                }*/
                  onClick={ this.closeConclusion }
                  //onLvl={ level => this.setState({level, finalLevel: level}) }
                  devId= {this.props.declaration._id}
                  signatoryType= {'expert'}
                  />

                </div>)
              }
              else if(this.props.conclusion) {
                //console.log(this.props.conclusion);
                return  <div>
                  <ViewConclusion conclusion={this.props.conclusion} devId = {this.props.declaration._id}/>
                  <Signatories  devId = {this.props.declaration._id}/>
                  </div>
              }else {
                return true;
              }



  }

}


export default Conclusion = withTracker( ({ id,loadContent }) => {

//console.log(id);
//console.log(loadContent);
  // const log = (log)=>{
  //   console.log(log);
  // }
const user=Meteor.userId();
//console.log(Meteor.user());
Meteor.subscribe('process');
Meteor.subscribe('locality');
Meteor.subscribe('equipment');
Meteor.subscribe('method');
Meteor.subscribe('product');

Meteor.subscribe('devsConclusions',id);
Meteor.subscribe('usersInRoles',['approve-deviation']);
//console.log(DevsConclusions.findOne({deviation_id : id}));
//console.log(DevsInvestigations.findOne({deviation_id : id}));
//console.log(Roles.getUsersInRole(['approve-deviation'],null).fetch());
return {
user: user,
processes : Processes.find({}).fetch(),
localities : Localities.find({}).fetch(),
methods : Methods.find({}).fetch(),
equipments : Equipments.find({}).fetch(),
products : Products.find({}).fetch(),
conclusion : DevsConclusions.findOne({deviation_id : id})  ,
};
}
)(ConclusionInterface);
