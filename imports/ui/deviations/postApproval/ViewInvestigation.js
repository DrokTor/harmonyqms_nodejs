import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';

import DatePicker from 'react-date-picker';
import TimePicker from 'react-time-picker';
import Select, {Option, OptGroup} from 'rc-select';
import 'rc-select/assets/index.css';
import classNames from 'classnames';

import Alert from 'react-s-alert';



// App component - represents the whole app

export  class ViewInvestigation extends Component {

  constructor(props){
    super(props);

    this.state = {
    }
  }

  componentDidMount(){
    //console.log(this.props.investigation);
      if(typeof this.props.investigation.investigation != 'undefined')
      {
        Meteor.call('user.findOne',this.props.investigation.investigation.investigatorId,(err,result)=>{
        if(result)
        {
          this.setState({investigator : result.username});
        }
        })
    }else if(this.props.investigation.skipped){

      Meteor.call('user.findOne',this.props.investigation.skipped.skippedBy,(err,result)=>{
      if(result)
      {
        this.setState({skippedBy : result.username});
      }
      })
    }
      // console.log(this.props.investigation);
  }

  render() {

    if(this.props.investigation.skipped){

        return(
          <div className="border p-5 mt-5 border-warning">
            <div className="row ">
              <div className=" col-4">
                <label>Skip date</label>
              </div>
              <div className=" col-4">
                <label>Skipped by</label>
              </div>
              <div className=" col-4">
                <label>Reason</label>
              </div>
            </div>
            <div className="row">
              <div className=" col-4">
                {moment(this.props.investigation.skipped.skipDate).format('DD-MM-YYYY')}
              </div>
              <div className=" col-4">
                {this.state.skippedBy}
              </div>
              <div className=" col-4">
                {this.props.investigation.skipped.reason}
              </div>
            </div>
          </div>
        )
    }

    const lvlClass = classNames({
      'lvlMinor': this.props.investigation.preliminaryLevel.level == 'Minor',
      'lvlMedium': this.props.investigation.preliminaryLevel.level == 'Major',
      'lvlMajor': this.props.investigation.preliminaryLevel.level == 'Critical',
      'lvlNull': this.props.investigation.preliminaryLevel.level == null,
    });

    return (

      <div className="col-12  core">

          <h3 className="collapseLabel col-md-12  mt-5"  >Identification </h3>
          <div className="row p-3 col-12    ">
            <label className="col-4">Preliminary level</label>
            <p className={"col-8 "+lvlClass }>

            </p>
          </div>
          <div className="row p-3 col-12 bg-light   ">
            <label className="col-4">Investigator</label>
            <p className="col-8">
              {this.state.investigator}
            </p>
          </div>
          <div className="row p-3 col-12    ">
            <label className="col-4">Investigation method</label>
            <p className="col-8">
              {this.props.investigation.method}
            </p>
          </div>
          <div className="row p-3  col-12  bg-light ">
            <label className="col-4" >Ishikawa</label>
            <p className="col-8">
               { this.props.investigation.ishikawa.join(', ') }
            </p>
          </div>
          <div className="row p-3  col-12   ">
            <label className="col-4" >Identified root causes</label>
            <p className="col-8">
              {this.props.investigation.identifiedRootCauses}
            </p>
          </div>
          <div className="row p-3  col-12  bg-light ">
            <label className="col-4" >History</label>
            <p className="col-8">
              {this.props.investigation.history}
            </p>
          </div>
          <div className="row p-3  col-12   ">
            <label className="col-4" >Process Review</label>
            <p className="col-8">
              {this.props.investigation.processReview}
            </p>
          </div>


          <h3 className="collapseLabel col-md-12  mt-5"  >Reviews </h3>
          <div className="row p-3 col-12   bg-light ">
            <label className="col-4">Material Review</label>
            <p className="col-8">
              {this.props.investigation.meterialReview}
            </p>
          </div>
          <div className="row p-3  col-12   ">
            <label className="col-4" >Batch Review</label>
            <p className="col-8">
              {this.props.investigation.batchReview}
            </p>
          </div>
          <div className="row p-3  col-12  bg-light ">
            <label className="col-4" >Equipment Review</label>
            <p className="col-8">
              {this.props.investigation.equipmentReview}
            </p>
          </div>
          <div className="row p-3  col-12   ">
            <label className="col-4" >Regulatory compliance</label>
            <p className="col-8">
              {this.props.investigation.complianceReview}
            </p>
          </div>
          <div className="row p-3  col-12  bg-light ">
            <label className="col-4" >Actions Review</label>
            <p className="col-8">
              {this.props.investigation.actionsReview}
            </p>
          </div>
          <div className="row p-3 col-12  ">
            <label className="col-4">Impact Review</label>
            <p className="col-8">
              {this.props.investigation.impactReview}
            </p>
          </div>
          <div className="row p-3  col-12    bg-light ">
            <label className="col-4" >GMP documents Review</label>
            <p className="col-8">
              {this.props.investigation.gmpDocReview}
            </p>
          </div>
          <div className="row p-3  col-12    ">
            <label className="col-4" >Product Quality Review</label>
            <p className="col-8">
              {this.props.investigation.productQualityReview}
            </p>
          </div>


          <h3 className="collapseLabel col-md-12  mt-5"  >Final analysis </h3>

          <div className="row p-3  col-12   ">
            <label className="col-4" >Addtional analysis and tests</label>
            <p className="col-8">
              {this.props.investigation.additionalAnalysis}
            </p>
          </div>
          <div className="row p-3  col-12  bg-light ">
            <label className="col-4" >Conclusion</label>
            <p className="col-8">
              {this.props.investigation.conclusion}
            </p>
          </div>


      </div>

     )


  }

}


export default withTracker( () => {
const user=Meteor.userId();

Meteor.subscribe('usersInRoles',['approve-deviation']);

return {
user: user,

};
}
)(ViewInvestigation);
