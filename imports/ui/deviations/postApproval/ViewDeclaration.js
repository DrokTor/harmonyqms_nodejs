import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';

import DatePicker from 'react-date-picker';
import TimePicker from 'react-time-picker';
import Select, {Option, OptGroup} from 'rc-select';
import 'rc-select/assets/index.css';
import classNames from 'classnames';

import Alert from 'react-s-alert';



// App component - represents the whole app

export  class ViewDeclaration extends Component {

  constructor(props){
    super(props);

    this.state = {
      approbator : '',
    }
  }

  componentDidMount(){
      Meteor.call('user.findOne',this.props.deviation.approbation.approbatorId,(err,result)=>{
        if(result)
        {
          this.setState({approbator : result.username});
        }
      })
  }

  render() {
    const lvlClass = classNames({
      'lvlMinor': this.props.deviation.estimatedLevel == 'Minor',
      'lvlMedium': this.props.deviation.estimatedLevel == 'Medium',
      'lvlMajor': this.props.deviation.estimatedLevel == 'Major',
      'lvlNull': this.props.deviation.estimatedLevel == null,
    });
    return (

      <div className="col-12  core">

          <h3 className="collapseLabel col-md-12  mt-5"  >General information </h3>
          <div className="row p-3 col-12    ">
            <label className="col-4">Estimated level</label>
            <p className={"col-8 "+lvlClass }>

            </p>
          </div>
          <div className="row p-3 col-12 bg-light   ">
            <label className="col-4">Approbator</label>
            <p className="col-8">
              {this.state.approbator}
            </p>
          </div>
          <div className="row p-3 col-12    ">
            <label className="col-4">Deviation title</label>
            <p className="col-8">
              {this.props.deviation.title}
            </p>
          </div>
          <div className="row p-3  col-12  bg-light ">
            <label className="col-4" >Detection date</label>
            <p className="col-8">
              {moment(this.props.deviation.detectionDate).format('DD-MM-YYYY')}
            </p>
          </div>
          <div className="row p-3  col-12   ">
            <label className="col-4" >Detection time</label>
            <p className="col-8">
              {this.props.deviation.detectionTime}
            </p>
          </div>
          <div className="row p-3  col-12  bg-light ">
            <label className="col-4" >Type</label>
            <p className="col-8">
              {this.props.deviation.type}
            </p>
          </div>
          <div className="row p-3  col-12   ">
            <label className="col-4" >Planification</label>
            <p className="col-8">
              {this.props.deviation.planification}
            </p>
          </div>


          <h3 className="collapseLabel col-md-12  mt-5"  >Targetting </h3>
          <div className="row p-3 col-12   bg-light ">
            <label className="col-4">Process</label>
            <p className="col-8">
              {this.props.deviation.process}
            </p>
          </div>
          <div className="row p-3  col-12   ">
            <label className="col-4" >Locality</label>
            <p className="col-8">
              {this.props.deviation.locality}
            </p>
          </div>
          <div className="row p-3  col-12  bg-light ">
            <label className="col-4" >Equipment</label>
            <p className="col-8">
              {this.props.deviation.equipment}
            </p>
          </div>
          <div className="row p-3  col-12   ">
            <label className="col-4" >Product Code</label>
            <p className="col-8">
              {this.props.deviation.product}
            </p>
          </div>
          <div className="row p-3  col-12  bg-light ">
            <label className="col-4" >Batch Number</label>
            <p className="col-8">
              {this.props.deviation.batch && this.props.deviation.batch.join(', ')}
            </p>
          </div>



          <h3 className="collapseLabel col-md-12  mt-5"  >Details </h3>
          <div className="row p-3 col-12   bg-light ">
            <label className="col-4">What</label>
            <p className="col-8">
              {this.props.deviation.what}
            </p>
          </div>
          <div className="row p-3  col-12   ">
            <label className="col-4" >How</label>
            <p className="col-8">
              {this.props.deviation.how}
            </p>
          </div>
          <div className="row p-3  col-12  bg-light ">
            <label className="col-4" >Impact</label>
            <p className="col-8">
              {this.props.deviation.impact}
            </p>
          </div>
          <div className="row p-3  col-12   ">
            <label className="col-4" >Immediate Actions</label>
            <p className="col-8">
              {this.props.deviation.immediateActions}
            </p>
          </div>
          <div className="row p-3  col-12  bg-light ">
            <label className="col-4" >Suspected root cause</label>
            <p className="col-8">
              {this.props.deviation.suspectedRootCauses}
            </p>
          </div>


      </div>

    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();

Meteor.subscribe('usersInRoles',['approve-deviation']);

return {
user: user,

};
}
)(ViewDeclaration);
