import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';

import DatePicker from 'react-date-picker';
import TimePicker from 'react-time-picker';
import Select, {Option, OptGroup} from 'rc-select';
import 'rc-select/assets/index.css';
import classNames from 'classnames';

import Alert from 'react-s-alert';

import {DevsDeclarations} from '../../../api/devsDeclarations';
import {DevsInvestigations} from '../../../api/devsInvestigations';
import {DevsConclusions} from '../../../api/devsConclusions';

import  SigningModal  from '../modals/SigningModal';

// App component - represents the whole app

export  class SignatoriesInterface extends Component {

  constructor(props){
    super(props);

    this.state = {
      approbator : {},
      expert : {},
      investigator : {},
    }
  }

  componentDidMount(){
    //console.log(this.props.investigation);
      // if(typeof this.props.investigation.investigation != 'undefined')
      // {
      // }
        Meteor.call('user.findOne',this.props.approbatorId,(err,result)=>{
        if(result)
        {
          this.setState({approbator : {username : result.username, signed : false}});
        }
      })
        if (this.props.investigatorId) {

          Meteor.call('user.findOne',this.props.investigatorId,(err,result)=>{
          if(result)
          {
            this.setState({investigator : {username : result.username, signed : false}});
          }
          })
        }
        Meteor.call('user.findOne',this.props.expertId,(err,result)=>{
        if(result)
        {
          this.setState({expert : {username : result.username, signed : false}});
        }
      })
       //console.log(this.props.investigatorId);
  }

  componentDidUpdate() {
    //console.log(nextProps);
      const signatories = this.props.conclusion.signatories;
      if(this.props.state != 'Closed' && signatories.expert && signatories.approbator && (this.props.investigatorId ? signatories.investigator : true)){
        Meteor.call('devsDeclarations.setState',this.props.devId,'Closed');
        Meteor.call('devsDeclarations.setClosing',this.props.devId,this.props.creationDate);
      }
  }

  render() {

    const signatories = this.props.conclusion.signatories;
    return (

      <div className="col-12  core">


          <h3 className="collapseLabel col-md-12  mt-5"  >Signatories </h3>
          <div className="row">
            <label className="col-3" >User</label>
            <label className="col-3" >Notice</label>
            <label className="col-3" >Comment</label>
          </div>
          <div className="row p-3  col-12   bg-light">
            <p className="col-3">
              {this.state.expert.username}
            </p>
            <p className="col-3">
              { signatories.expert && signatories.expert.notice}
            </p>
            <p className="col-3">
              { signatories.expert && signatories.expert.comment}
            </p>
            <div className="col-3">
              { signatories && signatories.expert && <div className="p-2 text-center">Signed <i className="fa fa-check fa-2x text-success"></i></div> }
              { !signatories.expert && <button className="btn btn-success p-2 col-12" data-toggle='modal' data-target="#closeConclusionModal"
                onClick={()=>this.setState({signatoryType: 'expert'})}
                >Sign</button>}
            </div>
          </div>
          <div className="row p-3  col-12    ">
            <p className="col-3">
              {this.state.approbator.username}
            </p>

            <p className="col-3">
              { signatories.approbator && signatories.approbator.notice}
            </p>
            <p className="col-3">
              { signatories.approbator && signatories.approbator.comment}
            </p>
            <div className="col-3">
              { signatories && signatories.approbator && <div className="p-2 text-center">Signed <i className="fa fa-check fa-2x text-success"></i></div> }
              { !signatories.approbator &&
                <button className="btn btn-success p-2 col-12" data-toggle="modal" data-target="#closeConclusionModal"
                  onClick={()=>this.setState({signatoryType: 'approbator'})}>Sign</button>}
            </div>
          </div>
          { this.state.investigator.username &&
          <div className="row p-3  col-12   bg-light">
            <p className="col-3">
              {this.state.investigator.username}
            </p>

            <p className="col-3">
              { signatories.investigator && signatories.investigator.notice}
            </p>
            <p className="col-3">
              { signatories.investigator && signatories.investigator.comment}
            </p>
            <div className="col-3">
              { signatories && signatories.investigator && <div className="p-2 text-center">Signed <i className="fa fa-check fa-2x text-success"></i></div> }
              { !signatories.investigator &&
                <button className="btn btn-success p-2 col-12" data-toggle="modal" data-target="#closeConclusionModal"
                  onClick={()=>this.setState({signatoryType: 'investigator'})}>Sign</button>}
            </div>
          </div>
          }
          <SigningModal
            //onLvl={ level => this.setState({level, finalLevel: level}) }
            devId= {this.props.devId}
            signatoryType= {this.state.signatoryType}
            />
      </div>

     )


  }

}


export default Signatories = withTracker( ({ devId }) => {
const user=Meteor.userId();

//console.log(devId);
Meteor.subscribe('usersInRoles',['approve-deviation']);
Meteor.subscribe('devsDeclarations',devId);
Meteor.subscribe('devsInvestigations',devId);
Meteor.subscribe('devsConclusions',devId);
//console.log(DevsConclusions.findOne({'deviation_id' : devId}));
const creationDate = DevsDeclarations.findOne({'_id' : devId}).creationDate;
const state = DevsDeclarations.findOne({'_id' : devId}).state;
const approbatorId = DevsDeclarations.findOne({'_id' : devId}).approbation.approbatorId;
const expertId = DevsDeclarations.findOne({'_id' : devId}).expertise.expertId;
const investigation = DevsInvestigations.findOne({'deviation_id' : devId});
investigatorId = investigation.investigation ? investigation.investigation.investigatorId : '';
//console.log(investigation);
return {
approbatorId,
investigatorId,
expertId,
devId,
state,
creationDate,
//declaration : DevsDeclarations.findOne({'_id' : devId}) || {},
//investigation : DevsInvestigations.findOne({'deviation_id' : devId}) || {},
conclusion : DevsConclusions.findOne({'deviation_id' : devId}) || {},
};
}
)(SignatoriesInterface);
