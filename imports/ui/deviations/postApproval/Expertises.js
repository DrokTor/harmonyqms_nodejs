import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';



import {DevsDeclarations} from '../../../api/devsDeclarations.js';
//import Login from './Login.js';
//import Modules from './Modules.js';



// App component - represents the whole app

export  class Expertises extends Component {


  constructor(props){
    super(props);

    this.state = {
      page : 'modules',
    }

   }


  render() {

    return (

      <div className="col-9 core   ">
        <div className="row">
          <h3>Deviations waiting for your expertise ({this.props.expertises.length})</h3>
          <table className="col-12  mt-5 ">
            <thead><tr><th className="col-3">Title</th><th className="col-3">Detection date</th><th className="col-3">Type</th><th className="col-3">Planification</th></tr></thead>
            <tbody>
          {   this.props.expertises ?
            (() => {return this.props.expertises.map((deviation,index)=>{
              return <tr key={index} className="pointer" onClick={()=>{this.props.onClick('viewDeviation',deviation,deviation._id)}} ><td className="col-3">{deviation.title}</td><td className="col-3">{moment(deviation.detectionDate).format('DD-MM-YYYY')}</td><td className="col-3">{deviation.type}</td><td>{deviation.planification}</td></tr>
            })})()
            :''
          }
          </tbody>
          </table>
        </div>
      </div>

    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();

if( Roles.userIsInRole(user,['launch-investigation']) )
{
  Meteor.subscribe('expertises');
}

return {
user: user,
expertises:  Roles.userIsInRole(Meteor.userId(),['launch-investigation'])? DevsDeclarations.find({"expertise.expertId":Meteor.userId(), state: 'Declaration'}).fetch():0,
};
}
)(Expertises);
