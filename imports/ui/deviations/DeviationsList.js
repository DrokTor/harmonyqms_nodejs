import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import classNames from 'classnames';
import { Session } from 'meteor/session';
import queryString from 'query-string';

import {DevsDeclarations} from '../../api/devsDeclarations.js';
//import Login from './Login.js';
//import Modules from './Modules.js';



// App component - represents the whole app

export  class DeviationsList extends Component {


  constructor(props){
    super(props);

    this.state = {
      page : 'modules',
    }
    // this.getUsername = this.getUsername.bind(this);
   }

   // getUsername(userId){
   //
   //      let user = _.find(this.props.users,(item)=>{
   //        return item._id == userId;
   //      });
   //      console.log(this.props.users);
   //      console.log(userId);
   //      console.log(user);
   //      return user && user.username;
   // }

  render() {

    return (

      <div className="col-9 core   ">
        <div className="row">
          <h3>Deviations list  ({this.props.deviations.length})</h3>
        <table className="col-12  mt-5 text-center">
            <thead><tr>
              <th className="col-1">ID</th>
              <th className="col-3">Title</th>
              <th  className="col-4">
                <div className="row">
                  <div className="col-6">Creation date</div>
                  <div className="col-6">Creation time</div>
                </div>
              </th>
              <th  className="col-4">
                <div className="row">
                  <div className="col-4">Creator</div>
                  <div className="col-5">State</div>
                  <div className="col-3">Level</div>
                </div>
              </th>
            </tr></thead>
            <tbody>
          {
            (() => {return this.props.deviations.map((deviation,index)=>{
              const lvlClass = classNames({
                'lvlMinor col-3': deviation.level == 'Minor',
                'lvlMedium col-3': deviation.level == 'Major',
                'lvlMajor col-3': deviation.level == 'Critical',
                'lvlNull col-3': deviation.level == null,
              });
              const stateClass = classNames({
                'fa fa-file-text  text-danger': deviation.state == 'Declaration',
                'fa fa-search text-warning': deviation.state == 'Investigation',
                'fa fa-dot-circle-o text-success': deviation.state == 'Conclusion',
                'fa fa-pencil-square-o  text-primary': deviation.state == 'Signing',
                'fa fa-check-square  text-info': deviation.state == 'Closed',
                'lvlNull': deviation.state == null,
              });
              //console.log(moment(deviation.creationDate).format('DD-MM-YYYY'));  this.props.onClick('viewDeviation',deviation,deviation._id
              return <tr key={index} className="pointer" onClick={()=>{this.props.history.push('/deviations/view/'+deviation._id)}} >
                <td className="col-1">-</td>
                <td className="col-3">{deviation.title}</td>
                <td  className="col-4">
                  <div className="row">
                    <div className="col-6">{moment(deviation.creationDate).format('DD-MM-YYYY')}</div>
                    <div className="col-6">{moment(deviation.creationDate).format('HH:mm')}</div>
                  </div>
                </td>
                <td  className="col-4">
                  <div className="row">
                    <div className="col-4">{deviation.user.username}</div>
                    <div className="col-5 ">{deviation.state} <i className={stateClass}></i> </div>
                    <div className={lvlClass} ></div>
                  </div>
                </td>
              </tr>
            })})()

          }
          </tbody>
          </table>
        </div>
      </div>

    );

  }

}


export default DeviationsListContainer = withTracker( ({match}) => {
const user=Meteor.userId();

const parsed = queryString.parse(location.search);

//console.log(parsed);

  Meteor.subscribe('devsDeclarations');
  //Meteor.subscribe('users');

//console.log(DevsDeclarations.find({}).fetch());
return {
user: user,
deviations:  DevsDeclarations.find(parsed, {sort : {creationDate : -1}}).fetch(),
//users: Meteor.users.find({}).fetch(),

};
}
)(DeviationsList);
