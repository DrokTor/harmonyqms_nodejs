import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';

import { Route,Switch,Link,Redirect } from 'react-router-dom'


import Header from './Header.js';
//import Body from './Body.js';
//import Modules from './Modules.js';
import Dashboard from './dashboard/Dashboard.js';
import Core from './Core.js';
import LeftMenu from './LeftMenu.js';
import DeviationsListContainer from './DeviationsList.js';
import ViewDeviation from './postApproval/ViewDeviation.js';
import NewDeviationContainer from './NewDeviation.js';
import Drafts from './preApproval/Drafts.js';
import DraftSealed from './preApproval/DraftSealed.js';
import MissingInfo from './preApproval/MissingInfo.js';
import Sent from './preApproval/Sent.js';
import Approvals from './preApproval/Approvals.js';
import Expertises from './postApproval/Expertises.js';
import Investigations from './postApproval/Investigations.js';

// App component - represents the whole app

export  class MainLayout extends Component {


  constructor(props){
    super(props);

    this.state = {
      content : 'dashboard',
      deviation : '',
      deviationId : '',
    }

    this.loadContent = this.loadContent.bind(this);
  }

  loadContent(content,deviation='',deviationId=''){
    this.setState({content,deviation,deviationId},()=>{
      //console.log(this.state.deviation);
    });
  }

  render() {

    return (

      <div className="row ">
        <Header onClick={ this.loadContent }/>
        {/* <Body/> */}
        <div className="col-12 ">
        <div className="row">
          <LeftMenu onClick={ this.loadContent }/>
          <Switch>
            {/* <Route exact path="/deviations/" component={Dashboard}/> */}
            <Route path="/deviations/dashboard" component={Dashboard}/>
            <Route path="/deviations/list/:search?" component={DeviationsListContainer}/>
            <Route path="/deviations/view/:_id" component={ViewDeviation}/>
            {/* <Route path="/deviations/investigations/" component={Investigations}/> */}
            <Route path="/deviations/expertises/" component={Expertises}/>
            <Route path="/deviations/drafts/" component={Drafts}/>
            <Route path="/deviations/incompletes/" component={MissingInfo}/>
            <Route path="/deviations/sents/" component={Sent}/>
            <Route path="/deviations/approvals/" component={Approvals}/>
            <Route path="/deviations/draftSealed/:_id" component={DraftSealed}/>
            <Route path="/deviations/new/:_id?" component={NewDeviationContainer}/>
            {/* <NewDeviation draft={this.props.deviation} onClick={ this.props.onClick } /> */}
            {/* <Route //this should be changed
              path='/deviations/draftSealed/'
              render={(props) => <DraftSealed {...props} draft={ this.state.deviation }  />}
            /> */}
            {/* <Route
              path='/deviations/'
              render={(props) => <Core {...props} onClick={ this.loadContent } content={this.state.content} deviation={this.state.deviation} deviationId={this.state.deviationId} />}
            /> */}
          {/* <Core onClick={ this.loadContent } content={this.state.content} deviation={this.state.deviation} deviationId={this.state.deviationId}   /> */}
          </Switch>
        </div>
        </div>
      </div>

    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();

return {
user: user,
};
}
)(MainLayout);
