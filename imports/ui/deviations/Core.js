import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';



import NewDeviation from './NewDeviation.js';
import Drafts from './preApproval/Drafts.js';
import DraftSealed from './preApproval/DraftSealed.js';
import MissingInfo from './preApproval/MissingInfo.js';
import Sent from './preApproval/Sent.js';
import Approvals from './preApproval/Approvals.js';
import Expertises from './postApproval/Expertises.js';
import Investigations from './postApproval/Investigations.js';
import DeviationsList from './DeviationsList.js';
import ViewDeviation from './postApproval/ViewDeviation.js';
import Dashboard from './dashboard/Dashboard.js';
//import Modules from './Modules.js';



// App component - represents the whole app

export  class Core extends Component {


  constructor(props){
    super(props);

    this.state = {
      page : 'modules',
    }

    this.loadPage = this.loadPage.bind(this);
  }

  loadPage(page){
    this.setState({page});
  }

  render() {
//console.log(this.props.draft);
    return (

      <div className="col-10   core">
        {
          (()=>{
            switch (this.props.content) {

              // case 'newDeviation':
              //   return <NewDeviation draft={this.props.deviation} onClick={ this.props.onClick } />
              //   break;
              // case 'draftSealed':
              // return <DraftSealed draft={ this.props.deviation }/>
              // break;
              // case 'drafts':
              //   return <Drafts onClick={ this.props.onClick }/>
              //   break;
              // case 'missingInfo':
              //   return <MissingInfo onClick={ this.props.onClick }/>
              //   break;
              // case 'sent':
              //   return <Sent onClick={ this.props.onClick }/>
              //   break;
              // case 'approvals':
              //   return <Approvals onClick={ this.props.onClick }/>
              //   break;
              // case 'expertises':
              //   return <Expertises onClick={ this.props.onClick }/>
              //   break;
              // case 'investigations':
              //   return <Investigations onClick={ this.props.onClick }/>
              //   break;
              // case 'listDeviations':
              //   return <DeviationsList onClick={ this.props.onClick }/>
              //   break;
              // case 'viewDeviation':
              //   return <ViewDeviation onClick={ this.props.onClick } /*deviation={this.props.deviation}*/ deviationId={this.props.deviationId}/>
              //   break;

              // default:
              // return <Dashboard/>

            }
          })()
        }
      </div>

    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();



return {
user: user,

};
}
)(Core);
