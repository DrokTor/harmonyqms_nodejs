import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';

import { Link } from 'react-router-dom';

import Header from './Header.js';
import Login from './Login.js';

import MainLayout from './deviations/MainLayout.js';


// App component - represents the whole app

export  class App extends Component {





  render() {

    return (

      <div className="container-fluid ">
        <div className="jumbotron">
            <h1> Welcome to Harmony </h1>
            <p className="lead"> Your quality management software </p>
        </div>
        <div className="body-content col-xs-12 no-float">
            <div className="row">
                <div className="col-lg-12">
                    {/* <h2>  Select a module </h2> */}
                    <div className="row">
                      <p className="modules d-inline col-3"><Link to="/deviations/dashboard"  className="btn btn-outline-secondary" > <i className="fa fa-code-fork fa-2x"></i>  <h3>Deviation management</h3>  </Link></p>
                      <p className="modules d-inline col-3"><Link to="/capas"  className="btn btn-outline-secondary" > <i className="fa fa-wrench fa-2x"></i>  <h3>CAPA management</h3>  </Link></p>
                      <p className="modules d-inline col-3"><Link to="/administration"  className="btn btn-outline-secondary" > <i className="fa fa-bar-chart fa-2x"></i>  <h3>Dashboard</h3>   </Link></p>
                      <p className="modules d-inline col-3"><Link to="/administration"  className="btn btn-outline-secondary" > <i className="fa fa-sliders fa-2x"></i>  <h3>Administration</h3>   </Link></p>
                    </div>
                </div>
            </div>
        </div>
      </div>

    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();

return {
user: user,
};
}
)(App);
