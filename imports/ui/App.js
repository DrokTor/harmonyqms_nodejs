import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';

import { BrowserRouter,Route } from 'react-router-dom'


import Header from './Header.js';
import Login from './Login.js';
import Modules from './Modules.js';

import Deviations from './deviations/MainLayout.js';
import Capas from './capas/MainLayout.js';
import Administration from './administration/MainLayout.js';


// App component - represents the whole app

export  class App extends Component {


  constructor(props){
    super(props);

    this.state = {
      page : 'modules',
    }

    this.loadPage = this.loadPage.bind(this);
  }

  loadPage(page){
    this.setState({page});
  }

  render() {

    return (
      <BrowserRouter>

      <div className="container-fluid ">
        <Header onClick={this.loadPage}/>
        <Route path="/capas" component={Capas}/>
        <Route path="/deviations" component={Deviations}/>
        <Route path="/administration" component={Administration}/>
        <Route path="/modules" component={Modules}/>
        <Route exact path="/" component={Modules}/>
        {/* {
          (()=>{
          switch (this.state.page) {
            case 'modules':
            return  <Modules onClick={this.loadPage}/>
              break;
            case 'deviations':
            return  <Deviations onClick={this.loadPage}/>
              break;
            case 'administration':
            return  <Administration onClick={this.loadPage}/>
              break;
            default:
            return  <Modules onClick={this.loadPage}/>

          }
          })()
        } */}
      </div>
    </BrowserRouter>

    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();

return {
user: user,
};
}
)(App);
