import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withTracker } from 'meteor/react-meteor-data';

//import moment from 'momentjs/moment';
import ReactLoading from 'react-loading';



// Client component - represents a single machine item
export class LoginInterface extends Component {

  constructor(props){
    super(props);

    this.state = {
      username: '',
      password: '',
    }
    this.login = this.login.bind(this)
    this.loggingIn = this.loggingIn.bind(this)
  }

  login(){
    Meteor.loginWithPassword(this.state.username, this.state.password)
  }

  loggingIn()
  {
    return Meteor.loggingIn()
  }
  render() {

    return (
       <div  className="loginContainer mt-5 col-4 offset-4">

           <div id="logo" className="offset-3 col-6  text-center mb-5">
             <img src=  "/assets/images/harmony_black_600.png" width="200"  />
          </div>
          {
            (this.props.loggingIn == true)?
            <ReactLoading className="mx-auto" type={'bubbles'} color={'red'} height={20} width={50} />
            :''
          }

         <div className="loginFields mt-5">
           <div className="row mb-3">
             <label>Utilisateur</label>
           <input
             type="text"
             className="col-12"
             onChange= {(e) => this.setState({ username: e.target.value })}
           />
           </div>
           <div className="row mb-5">
             <label>Mot de passe</label>
           <input
             type="password"
             className="col-12"
             onChange= {(e) => this.setState({ password: e.target.value })}
           />
           </div>
           <div className="row ">
             <div className="mx-auto loginButton pointer"
               onClick={ this.login }
               >
                 Se connecter</div>
           </div>
         </div>
       </div>
    );
  }
}

export default withTracker( () => {

return {
  loggingIn: Meteor.loggingIn(),
  }

} )(LoginInterface)
