import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';



//import Header from './Header.js';
//import Login from './Login.js';
import {CapaPlanning} from '../../../api/capaPlanning.js';
import {Objectives} from '../../../api/objectives.js';



// App component - represents the whole app

export  class Indicators extends Component {


  constructor(props){
    super(props);

    this.state = {

    }

  }

  isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
  }

  render() {
    const maxCaps = this.props.maxCaps,
      maxCaps_obj = this.props.maxCaps_obj,
      implemRate = this.props.implemRate,
        implemRate_obj = this.props.implemRate_obj,
      reviewRate = this.props.reviewRate,
        reviewRate_obj = this.props.reviewRate_obj,
      avgImplemTime = this.props.avgImplemTime,
        avgImplemTime_obj = this.props.avgImplemTime_obj,
      capsOver = this.props.capsOver,
        capsOver_obj = this.props.capsOver_obj,
      reviewOver = this.props.reviewOver,
        reviewOver_obj = this.props.reviewOver_obj;

    return (
      <div className="col-12  ">
        {
          maxCaps_obj &&
        <div className="col-12 row">

          <div className="row col-6 p-4 indicator-div">
            <div className="col-9 text-center">
              <h4>Number of CAPAs</h4>
              <div className="col-12 fs-3">{maxCaps}</div>
            </div>
            <div className="col-3 ">
              <div className="col-8 mx-auto indicator-light-container">
                <div className={" indicator-light "+ (maxCaps>maxCaps_obj.obj? "":"bg-success")}></div>
                <div className={" indicator-light "+ (maxCaps>maxCaps_obj.obj? "bg-danger":"")}></div>
              </div>

            </div>
          </div>

          <div className="row col-6 p-4 indicator-div">
            <div className="col-9 text-center">
              <h4>Implementation rate</h4>
              <div className="col-12 fs-3">{ this.isNumeric(implemRate)? implemRate+' %' : '-'}  </div>
            </div>
            <div className="col-3 ">
              <div className="col-8 mx-auto indicator-light-container">
                <div className={" indicator-light "+ (implemRate<implemRate_obj.obj? " ":"bg-success")}></div>
                <div className={" indicator-light "+ (implemRate<implemRate_obj.obj? "bg-danger":" ")}></div>
              </div>
            </div>
          </div>

          <div className="row col-6 p-4 indicator-div">
            <div className="col-9 text-center">
              <h4>Average implementation time</h4>
              <div className="col-12 fs-3">{this.isNumeric(avgImplemTime)? avgImplemTime+' days' : '-'} </div>
            </div>
            <div className="col-3 ">
              <div className="col-8 mx-auto indicator-light-container">
                <div className={" indicator-light "+ (avgImplemTime>avgImplemTime_obj.obj? " ":"bg-success")}></div>
                <div className={" indicator-light "+ (avgImplemTime>avgImplemTime_obj.obj? "bg-danger":" ")}></div>
              </div>
            </div>
          </div>

          <div className="row col-6 p-4 indicator-div">
            <div className="col-9 text-center">
              <h4>CAPAs overdues</h4>
              <div className="col-12 fs-3">{capsOver}</div>
            </div>
            <div className="col-3 ">
              <div className="col-8 mx-auto indicator-light-container">
                <div className={" indicator-light "+ (capsOver>capsOver_obj.obj? "":"bg-success")}></div>
                <div className={" indicator-light "+ (capsOver>capsOver_obj.obj? "bg-danger":"")}></div>
              </div>
            </div>
          </div>

          <div className="row col-6 p-4 indicator-div">
            <div className="col-9 text-center">
              <h4>CAPAs reviews</h4>
              <div className="col-12 fs-3">{ this.isNumeric(reviewRate)? reviewRate+' %' : '-'}  </div>
            </div>
            <div className="col-3 ">
              <div className="col-8 mx-auto indicator-light-container">
                <div className={" indicator-light "+ (reviewRate<reviewRate_obj.obj? " ":"bg-success")}></div>
                <div className={" indicator-light "+ (reviewRate<reviewRate_obj.obj? "bg-danger":" ")}></div>
              </div>
            </div>
          </div>


          <div className="row col-6 p-4 indicator-div">
            <div className="col-9 text-center">
              <h4>CAPAs reviews overdues</h4>
              <div className="col-12 fs-3">{reviewOver}</div>
            </div>
            <div className="col-3 ">
              <div className="col-8 mx-auto indicator-light-container">
                <div className={" indicator-light "+ (reviewOver>reviewOver_obj.obj? "":"bg-success")}></div>
                <div className={" indicator-light "+ (reviewOver>reviewOver_obj.obj? "bg-danger":"")}></div>
              </div>
            </div>
          </div>

        </div>
      }
      </div>

    );

  }

}


export default withTracker( () => {

Meteor.subscribe("capaPlanning");
Meteor.subscribe("objectives");

const objectives = Objectives.find({}).fetch();
//console.log(moment().startOf('month').toDate());
//console.log(_.findWhere(objectives,{indicator : "Number of CAPAs"}));
//console.log(DevsDeclarations.find({ creationDate : {$gt : moment().subtract(30, 'days').toISOString()} ,state : {$ne : 'Closed' }}).count());

const monthCapas = CapaPlanning.find({creationDate : {$gt : moment().startOf('month').toDate() , $lt : moment().endOf('month').toDate() } }).count(),
monthCapasImplemented = CapaPlanning.find({state: {$in : ['Effectiveness review','Implemented']} ,creationDate : {$gt : moment().startOf('month').toDate() , $lt : moment().endOf('month').toDate() } }).fetch(),
monthCapasReviews = CapaPlanning.find({state: {$in : ['Effectiveness review']} ,creationDate : {$gt : moment().startOf('month').toDate() , $lt : moment().endOf('month').toDate() } }).fetch(),
monthCapsClsTime = _.reduce(monthCapasImplemented,(mem,obj)=>{ return mem + obj.implementation.duration; },0);

//console.log(monthCapasImplemented);
//
// console.log(monthDevs);
// console.log(monthDevsClosed);
// console.log(monthDevsClosed.length/monthDevs);
// console.log(monthDevsClsTime/monthDevsClosed.length);


return {
  maxCaps : monthCapas,
  maxCaps_obj : _.findWhere(objectives,{indicator : "Number of CAPAs"}),
  implemRate: Math.round(monthCapasImplemented.length/monthCapas*100),
  implemRate_obj : _.findWhere(objectives,{indicator : "CAPAs implementation rate"}),
  reviewRate: Math.round(monthCapasReviews.length/monthCapas*100),
  reviewRate_obj : _.findWhere(objectives,{indicator : "CAPAs reviews rate"}),
  avgImplemTime: Math.round(monthCapsClsTime/monthCapasImplemented.length),
  avgImplemTime_obj : _.findWhere(objectives,{indicator : "CAPAs average implementation time"}),
  capsOver : CapaPlanning.find({ creationDate : {$lt : moment().subtract(30, 'days').toDate()} ,state : 'Planned' }).count(),
  capsOver_obj : _.findWhere(objectives,{indicator : "CAPAs overdues"}),
  reviewOver : CapaPlanning.find({ 'implementation.implementationDate' : {$lt : moment().subtract(30, 'days').toDate()} ,state : {$ne : 'Effectiveness review' }}).count(),
  reviewOver_obj : _.findWhere(objectives,{indicator : "CAPAs reviews overdues"}),
};
}
)(Indicators);
