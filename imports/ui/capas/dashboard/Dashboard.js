import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';



//import Header from './Header.js';
//import Login from './Login.js';
import Indicators from './Indicators.js';



// App component - represents the whole app

export  class Dashboard extends Component {


  constructor(props){
    super(props);

    this.state = {

    }

  }
 
  render() {

    return (

      <div className="col-12  ">
        <div className="col-12 mb-3">
          <button className="btn mr-2 btn-outline-secondary">Indicators</button>
          <button className="btn mr-2 btn-outline-secondary">Monthly analysis</button>
          <button className="btn btn-outline-secondary">Yearly analysis</button>
          <button className="btn btn-outline-secondary float-right"><i className="fa fa-expand"></i></button>

        </div>
        { (()=>{
          switch (this.state.dataType) {
            case 'indicators':
              return <Indicators />;
              break;
            default:
            case 'indicators':
              return <Indicators />;
              break;

          }})()
        }
      </div>

    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();

return {
user: user,
};
}
)(Dashboard);
