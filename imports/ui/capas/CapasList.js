import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import classNames from 'classnames';
import { Session } from 'meteor/session';
import queryString from 'query-string';

import {CapaPlanning} from '../../api/capaPlanning.js';
//import Login from './Login.js';
//import Modules from './Modules.js';



// App component - represents the whole app

export  class CapasList extends Component {


  constructor(props){
    super(props);

    this.state = {
      page : 'modules',
    }
    // this.getUsername = this.getUsername.bind(this);
   }

   // getUsername(userId){
   //
   //      let user = _.find(this.props.users,(item)=>{
   //        return item._id == userId;
   //      });
   //      console.log(this.props.users);
   //      console.log(userId);
   //      console.log(user);
   //      return user && user.username;
   // }

  render() {

    return (

      <div className="col-11   ">
        <div className="row">
          <h3>CAPAs list</h3>
        <table className="col-12  mt-5 text-center">
            <thead><tr>
              <th className="col-1">ID</th>
              <th className="col-3">Title</th>
              <th  className="col-4">
                <div className="row">
                  <div className="col-6">Creation date</div>
                  <div className="col-6">Creation time</div>
                </div>
              </th>
              <th  className="col-4">
                <div className="row">
                  <div className="col-6">Creator</div>
                  <div className="col-6">State</div>
                </div>
              </th>
            </tr></thead>
            <tbody>
          {
            (() => {return this.props.capas.map((capa,index)=>{

              const stateClass = classNames({
                'fa fa-calendar-check-o  text-danger': capa.state == 'Planned',
                'fa fa-wrench text-success': capa.state == 'Implemented',
                'fa fa-search text-warning': capa.state == 'Effectiveness review',
              });
              //console.log(moment(capa.creationDate).format('DD-MM-YYYY'));
              return <tr key={index} className="pointer" onClick={()=>{this.props.history.push('/capas/view/'+capa._id)}} >
                <td className="col-1">-</td>
                <td className="col-3">{capa.title}</td>
                <td  className="col-4">
                  <div className="row">
                    <div className="col-6">{moment(capa.creationDate).format('DD-MM-YYYY')}</div>
                    <div className="col-6">{moment(capa.creationDate).format('HH:mm')}</div>
                  </div>
                </td>
                <td  className="col-4">
                  <div className="row">
                    <div className="col-6">{capa.user.username}</div>
                    <div className="col-6 ">{capa.state} <i className={stateClass}></i> </div>

                  </div>
                </td>
              </tr>
            })})()

          }
          </tbody>
          </table>
        </div>
      </div>

    );

  }

}


export default CapasListContainer = withTracker( ({match}) => {
const user=Meteor.userId();

const parsed = queryString.parse(location.search);
//console.log(parsed);

  Meteor.subscribe('capaPlanning');
  //Meteor.subscribe('users');

//console.log(DevsDeclarations.find({}).fetch());
return {
user: user,
capas:  CapaPlanning.find(parsed, {sort : {creationDate : -1}}).fetch(),
//users: Meteor.users.find({}).fetch(),

};
}
)(CapasList);
