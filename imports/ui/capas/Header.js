import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';

import { Link } from 'react-router-dom';

import {CapaDrafts} from '../../api/capaDrafts.js';
import {CapaPlanning} from '../../api/capaPlanning.js';
//import Login from './Login.js';
import Drafts from './Drafts.js';



// App component - represents the whole app

export  class Header extends Component {


  constructor(props){
    super(props);

    this.state = {
      page : 'modules',
    }

    this.loadPage = this.loadPage.bind(this);
  }

  loadPage(page){
    this.setState({page});
  }

  render() {

    return (

      <div className="col-12 capa-header ">
        <div className="row">
          <span className="col-3 offset-1">CAPA management</span>
          <div className="col-5 offset-3">
            {/* {this.props.drafts? <button className="btn " onClick={()=>(this.props.onClick('drafts')) } >Drafts <span className="badge badge-secondary">{this.props.drafts}</span></button> : ''} */}
            {this.props.drafts? <Link to="/capas/drafts"><button className="btn " onClick={()=>(this.props.onClick('drafts')) } >Drafts <span className="badge badge-secondary">{this.props.drafts}</span></button></Link> : ''}
            {this.props.implementations? <Link to={"/capas/list?state=Planned&implementation.operatorId="+Meteor.userId()}><button className="btn ml-1"   >Implementations <span className="badge badge-secondary">{this.props.implementations}</span></button></Link> : ''}
            {/* {this.props.sent? <button className="btn ml-1" onClick={()=>(this.props.onClick('sent')) } >Sents <span className="badge badge-secondary">{this.props.sent}</span></button> : ''}
            {this.props.approvals? <button className="btn ml-1" onClick={()=>(this.props.onClick('approvals')) } >Approvals <span className="badge badge-secondary">{this.props.approvals}</span></button> : ''}
            {this.props.expertises? <button className="btn ml-1" onClick={()=>(this.props.onClick('expertises')) } >Expertises <span className="badge badge-secondary">{this.props.expertises}</span></button> : ''}
            {this.props.investigations? <button className="btn ml-1" onClick={()=>(this.props.onClick('investigations')) } >Investigations <span className="badge badge-secondary">{this.props.investigations}</span></button> : ''} */}
          </div>
        </div>
      </div>

    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();

  Meteor.subscribe('capaDrafts');
  Meteor.subscribe('capaPlanning');

  if( Roles.userIsInRole(user,['implement-capa']) )
  {
    Meteor.subscribe('capaImplementations');
  }

  if( Roles.userIsInRole(user,['launch-investigation']) )
  {
    Meteor.subscribe('expertises');
  }


  //console.log(DevsDeclarations.find({'expertise.expertId' : Meteor.userId(),state: 'Declaration'}).count());
//console.log(Roles.userIsInRole(user,['approve-deviation']));
//console.log(Roles.getRolesForUser(user));
//console.log(DraftDeviations.find({userId : Meteor.userId(),state: 'Approval'}).fetch());
//console.log(DevsDeclarations.find({'investigation.investigatorId' : Meteor.userId(),state: 'Investigation'}).count());
return {
user: user,
drafts:  CapaDrafts.find({'user._id' : Meteor.userId(),state: 'Draft'}).count(),
implementations:  CapaPlanning.find({'implementation.operatorId' : Meteor.userId(),state: 'Planned'}).count(),
// approvals:  Roles.userIsInRole(user,['approve-deviation'])? DraftDeviations.find({"approbation.approbatorId":Meteor.userId(), state: 'Approval'}).count():0,
// incomplete:  DraftDeviations.find({'user._id' : Meteor.userId(),state: 'MissingInfo'}).count(),
// sent:  DraftDeviations.find({'user._id' : Meteor.userId(),state: 'Approval'}).count(),
// expertises:  DevsDeclarations.find({'expertise.expertId' : Meteor.userId(),state: 'Declaration'}).count(),
// investigations:  DevsDeclarations.find({'investigation.investigatorId' : Meteor.userId(),state: 'Investigation'}).count(),
};
}
)(Header);
