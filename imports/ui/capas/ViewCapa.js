import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Session } from 'meteor/session';
import { ReactiveVar } from 'meteor/reactive-var';
import ReactLoading from 'react-loading';

import DatePicker from 'react-date-picker';
import TimePicker from 'react-time-picker';
import Select, {Option, OptGroup} from 'rc-select';
import 'rc-select/assets/index.css';
import classNames from 'classnames';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import Alert from 'react-s-alert';
//import Header from './Header.js';
//import Login from './Login.js';
//import Modules from './Modules.js';
// import { Processes } from '../../api/process';
// import { Products } from '../../api/products';
// import { Equipments } from '../../api/equipments';
// import { Methods } from '../../api/methods';
// import { Localities } from '../../api/localities';

// import  ApprobatorModal  from './ApprobatorModal';
// import  RequestInfoModal  from './RequestInfoModal';
// import  CloseDeviationModal  from './CloseDeviationModal';
import {CapaPlanning} from '../../api/capaPlanning.js';



import  ViewPlanning  from './ViewPlanning';
import  ImplementationContainer  from './ViewImplementation';
import  ReviewsContainer  from './ViewEffectivenessReview';
//import  Implementation  from './Implementation';
//import  Effectiveness  from './Effectiveness';




// App component - represents the whole app

export  class ViewCapaInterface extends Component {


  constructor(props){
    super(props);

    this.state = {
      approbator : '',
    }

    this.createPDF = this.createPDF.bind(this);

  }

  componentDidMount(){

    this.props.capa ?
    Meteor.call('user.findOne',this.props.capa.implementation.operatorId,(error, result)=>{
      if(result)
      this.setState({operator : result.username})
      //console.log(result);
      Meteor.call('user.findOne',this.props.capa.user._id,(error, result)=>{
        if(result)
        this.setState({creator : result.username})
        //console.log(result);
          // const qualityExpert = this.props.investigation.investigation ? this.props.investigation.investigation.qualityExpert : this.props.investigation.skipped.skippedBy;
          // Meteor.call('user.findOne',qualityExpert,(error, result)=>{
          //   if(result)
          //   this.setState({qualityExpert : result.username})
          //   if(this.props.investigation.investigation)
          //   {
          //   //console.log(result);
          //   Meteor.call('user.findOne',this.props.investigation.investigation.investigatorId,(error, result)=>{
          //     if(result)
          //     this.setState({investigator : result.username})
          //     //console.log(result);
          //   })
          //   }
          // })
      })
    })
    :'';
  }



  createPDF(){
    const dev = this.props.capa;
    const invest = this.props.investigation;
    const conc = this.props.conclusion;
    //console.log(invest);
    let doc = new jsPDF()
    let columns = ["Type", "Creation date", "Creation time", "Creator"];
    let rows = [
        [dev.type,
          moment(dev.creationDate).format('DD-MM-YYYY'),
          moment(dev.creationDate).format('HH:mm'),
          this.state.creator]
    ];

    doc.setFont("helvetica");
    doc.setFontType("bold");
    //doc.text(30, 30, 'Deviation management report n° '+dev._id);
    doc.text('Deviation management report n° '+dev._id, 100, 30, 'center');
    doc.text('- '+dev.title+' -', 100, 50, 'center');
    //doc.save('a4.pdf')

    doc.autoTable(columns, rows,{
        startY: 70,
        //margin: {horizontal: 7},
        styles: {cellPadding: 5},
        columnStyles: {text: {columnWidth: 'auto'}}
        });

    doc.text('Deviation level', 15, 120);

    columns = ["Severity", "Occurence", "Detectability", "Level"];
    rows = [
        ['Minor',
          'Minor',
          'Minor',
          dev.level]
        ];
    doc.autoTable(columns, rows,{
        startY: 130,
        //margin: {horizontal: 7},
        styles: {cellPadding: 5},
        columnStyles: {text: {columnWidth: 'auto'}}
        });

    doc.text('Declaration', 15, 170);

    columns = ["Estimated level", "Planification", "Detection date", "Detection time"];
    rows = [
        [ "Approbator",   this.state.approbator],
        [ "Approbation date",   moment(dev.expertise.dateSent).format('DD-MM-YYYY')],
        [ "Estimated level",     dev.estimatedLevel],
        [ "Planification",   dev.planification],
        [ "Detection date",   moment(dev.detectionDate).format('DD-MM-YYYY')],
        [ "Detection time",   dev.detectionTime],
        [ "Locality",   dev.locality],
        [ "Process",   dev.process],
        [ "Equipment",   dev.equipment],
        [ "Product",   dev.product],
        [ "Batch number",   dev.batch.join(', ')],
        [ "What",   dev.what],
        [ "How",   dev.how],
        [ "Impact",   dev.impact],
        [ "Immediate actions",   dev.immediateActions],
        [ "Suspected root causes",   dev.suspectedRootCauses],

        ];


    doc.autoTable(columns, rows,{
        startY: 180,
        //margin: {horizontal: 7},
        styles: {cellPadding: 5},
        showHeader: 'never',
        columnStyles: {
            0: {fillColor: [41, 128, 185], textColor: 255, fontStyle: 'bold'}
        }
        });

        //investigation
    doc.text('Investigation', 15, 180);
    if(invest.investigation){
    rows = [
        [ "Launched by",     this.state.qualityExpert],
        [ "Launch date",     moment(invest.investigation.creationDate).format('DD-MM-YYYY')],
        [ "Preliminary level",     invest.preliminaryLevel.level],
        [ "Investigated by",     this.state.investigator],
        [ "Method",     invest.method],
        [ "Ishikawa",   this.state.ishikawa],
        [ "Identified root causes",   invest.identifiedRootCauses],
        [ "History",   invest.history],
        [ "Process review",   invest.processReview],
        [ "Material review",   invest.meterialReview],
        [ "Batch review",   invest.batchReview],
        [ "Equipment review",   invest.equipmentReview],
        [ "Compliance review",   invest.complianceReview],
        [ "Actions review",   invest.actionsReview],
        [ "Impact review",   invest.impactReview],
        [ "GMP documents review",   invest.gmpDocReview],
        [ "Product quality review",   invest.productQualityReview],
        [ "Addtional analysis",   invest.additionalAnalysis],
        [ "Conclusion",   invest.conclusion],

        ];

        doc.autoTable(columns, rows,{
          startY: 190,
          //margin: {horizontal: 7},
          styles: {cellPadding: 5},
          showHeader: 'never',
          columnStyles: {
            0: {fillColor: [41, 128, 185], textColor: 255, fontStyle: 'bold'}
          }
        });

      }else {
        //console.log(doc.getFontSize());
        doc.setFontType("normal");
        doc.setFontSize(12);
        doc.text('Investigation skipped by '+this.state.qualityExpert+' on '+moment(invest.skipped.skipDate).format('DD-MM-YYYY'), 15, 190);
        doc.text('Reason: '+invest.skipped.reason, 15, 200);
        doc.setFontType("bold");
        doc.setFontSize(16);
      }


        //Conclusion
    doc.text('Conclusion', 15, 220);

    rows = [];
    Object.keys(conc.decision).map( (elem,index) => {
      rows.push([' Decision for batch n° '+elem, conc.decision[elem]]);
    })
    rows.push([ "Comment",     conc.comment]);
    rows.push([ "Final decision",     conc.finalDecision]);


    doc.autoTable(columns, rows,{
        startY: 230,
        //margin: {horizontal: 7},
        styles: {cellPadding: 5},
        showHeader: 'never',
        columnStyles: {
            0: {fillColor: [41, 128, 185], textColor: 255, fontStyle: 'bold'}
        }
        });


        //Signatories
        doc.text('Signatories', 15, 100);

        columns = ["Username", "Position", "Signing date", "Signature"];
        rows = [
            [this.state.qualityExpert,
              'Quality expert',
              moment(conc.signatories.expert.dateSignature).format('DD-MM-YYYY'),
              'Signed.'],
            [this.state.approbator,
              'Approbator',
              moment(conc.signatories.approbator.dateSignature).format('DD-MM-YYYY'),
              'Signed.'],
            (this.state.investigator?[this.state.investigator,
              'Investigator',
              moment(conc.signatories.investigator.dateSignature).format('DD-MM-YYYY'),
              'Signed.']:''),
        ];

        doc.autoTable(columns, rows,{
            startY: 110,
            //margin: {horizontal: 7},
            styles: {cellPadding: 5},
            columnStyles: {text: {columnWidth: 'auto'}}
            });
    doc.output('dataurlnewwindow');
  }

  render() {
    //console.log(this.props.capa.level);
    if(this.props.capa)
    {

      const stateClass = classNames({
        'fa fa-calendar-check-o  text-danger': this.props.capa.state == 'Planned',
        'fa fa-wrench text-success': this.props.capa.state == 'Implemented',
        'fa fa-search text-warning': this.props.capa.state == 'Effectiveness review',
      });
    return (

      <div className="col-12  ">
        <div className="col-12   border text-center mb-3">
            <div className="row headingLabels">
              <div className="col-2   ">
              <label className="col-7">ID</label>
              </div>
              <div className="row col-5">
                <div className="col-6   ">
                <label className="col-7">Creation date</label>
                </div>
                <div className="col-6   ">
                <label className="col-7">Creation time</label>
                </div>
              </div>
              <div className="row col-5">
                <div className="col-6   ">
                <label className="col-7">Creator</label>
                </div>
                <div className="col-6   ">
                <label className="col-7">State</label>
                </div>
              </div>
            </div>
            <div className="row pl-2 pr-2 pt-4 pb-4 bg-light">
              <div className="col-2   ">
                <span className="col-12">

                </span>
              </div>
              <div className="row col-5">
                <div className="col-6   ">
                  <span className="col-12">
                    {moment(this.props.capa.creationDate).format('DD-MM-YYYY') }
                  </span>
                </div>
                <div className="col-6   ">
                  <span className="col-12">
                    {moment(this.props.capa.creationDate).format('HH:mm') }
                  </span>
                </div>
              </div>
              <div className="row col-5">
                <div className="col-6   ">
                  <span className="col-12">
                    {this.props.capa.user.username}
                  </span>
                </div>
                <div className="col-6   ">
                  <span className="col-12">
                    {this.props.capa.state} <i className={stateClass}></i>
                  </span>
                </div>

              </div>
            </div>


        </div>
        <nav className="mt-5">
          <div className="nav nav-tabs" id="nav-tab" role="tablist">
            <span className=" col-3 p-3 nav-item nav-link active" id="nav-planning-tab" data-toggle="tab" href="#nav-planning" role="tab" aria-controls="nav-planning" aria-selected="true">Planning <i className="fa fa-calendar-check-o danger"></i></span>
            <span className=" col-3 p-3 nav-item nav-link" id="nav-implementation-tab" data-toggle="tab" href="#nav-implementation" role="tab" aria-controls="nav-implementation" aria-selected="false">Implementation <i className="fa fa-wrench success"></i></span>
            <span className=" col-3 p-3 nav-item nav-link" id="nav-review-tab" data-toggle="tab" href="#nav-review" role="tab" aria-controls="nav-review" aria-selected="false">Effectiveness review <i className="fa fa-search warning"></i></span>
            {
              this.props.capa.state == 'Effectiveness review' &&
              <div className=" col-1 p-3 "   role="tab"  ><i className="fa fa-file-pdf-o fa-2x text-info pointer" onClick={ this.createPDF }></i></div>
            }
          </div>
        </nav>
        <div className="tab-content" id="nav-tabContent">
          <div className="tab-pane fade show active" id="nav-planning" role="tabpanel" aria-labelledby="nav-planning-tab">
            <ViewPlanning capa = {this.props.capa}/>
          </div>
          <div className="tab-pane fade" id="nav-implementation" role="tabpanel" aria-labelledby="nav-implementation-tab">
            <ImplementationContainer capa= {this.props.capa} />
          </div>
          <div className="tab-pane fade" id="nav-review" role="tabpanel" aria-labelledby="nav-review-tab">
            <ReviewsContainer capa= {this.props.capa} />
          </div>
        </div>




      </div>

    );
    }
    else {
      return <ReactLoading type={'bars'} color={'lightgray'} className="mx-auto" height={60} width={100} />;
    }
  }

}


export default ViewCapa = withTracker( ({match}) => {
const user=Meteor.userId();

//console.log(match.params._id);
Meteor.subscribe('capaPlanning',match.params._id);
// Meteor.subscribe('devsInvestigations');
// Meteor.subscribe('devsConclusions');
// Meteor.subscribe('usersInRoles',['approve-capa']);
//console.log(DevsDeclarations.find({_id : _id}).fetch());
//console.log(Session.get('_id'));
return {
user: user,
capa: CapaPlanning.findOne({_id : match.params._id}),
//investigation: DevsInvestigations.findOne({capa_id : capaId}),
//conclusion: DevsConclusions.findOne({capa_id : capaId}),
//investigation: DevsInvestigations.findOne({capa_id : Session.get('capaId')}),
};
}
)(ViewCapaInterface);
