import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';

import DatePicker from 'react-date-picker';
import TimePicker from 'react-time-picker';
import Select, {Option, OptGroup} from 'rc-select';
import 'rc-select/assets/index.css';
import classNames from 'classnames';

import Alert from 'react-s-alert';



// App component - represents the whole app

export  class ViewPlanning extends Component {

  constructor(props){
    super(props);

    this.state = {
      operator : '',
    }
  }

  componentDidMount(){
      Meteor.call('user.findOne',this.props.capa.implementation.operatorId,(err,result)=>{
        if(result)
        {
          this.setState({operator : result.username});
        }
      })
  }

  render() {

    return (

      <div className="col-12   ">


          {/* <div className="row p-3 col-12 bg-light   ">
            <label className="col-4">Approbator</label>
            <p className="col-8">
              {this.state.approbator}
            </p>
          </div> */}
          <div className="row p-3 col-12    ">
            <label className="col-4">CAPA title</label>
            <p className="col-8">
              {this.props.capa.title}
            </p>
          </div>
          <div className="row p-3  col-12  bg-light ">
            <label className="col-4" >Creation date</label>
            <p className="col-8">
              {moment(this.props.capa.creationDate).format('DD-MM-YYYY')}
            </p>
          </div>

          <div className="row p-3  col-12 ">
            <label className="col-4" >Type</label>
            <p className="col-8">
              {this.props.capa.type}
            </p>
          </div>
          <div className="row p-3  col-12   bg-light  ">
            <label className="col-4" >Planned date</label>
            <p className="col-8">
              {moment(this.props.capa.plannedDate).format('DD-MM-YYYY')}
            </p>
          </div>


          <div className="row p-3  col-12   ">
            <label className="col-4" >Source</label>
            <p className="col-8">
              {this.props.capa.source}
            </p>
          </div>


          <div className="row p-3 col-12   bg-light ">
            <label className="col-4">Source Id</label>
            <p className="col-8">
              {this.props.capa.sourceId}
            </p>
          </div>
          <div className="row p-3  col-12   ">
            <label className="col-4" >Description</label>
            <p className="col-8">
              {this.props.capa.description}
            </p>
          </div>



      </div>

    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();



return {
user: user,

};
}
)(ViewPlanning);
