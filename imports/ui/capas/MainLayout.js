import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';

import { Route,Switch } from 'react-router-dom'


import Header from './Header.js';
//import Body from './Body.js';
//import Modules from './Modules.js';
//import Core from './Core.js';
import LeftMenu from './LeftMenu.js';
import NewCAPAContainer from './NewCAPA.js';
import Drafts from './Drafts.js';
import CapasList from './CapasList.js';
import ViewCapa from './ViewCapa.js';
import Dashboard from './dashboard/Dashboard.js';


// App component - represents the whole app

export  class MainLayout extends Component {


  constructor(props){
    super(props);

    this.state = {
      content : 'dashboard',
      deviation : '',
      deviationId : '',
    }

    this.loadContent = this.loadContent.bind(this);
  }

  loadContent(content,deviation='',deviationId=''){
    this.setState({content,deviation,deviationId},()=>{
      //console.log(this.state.deviation);
    });
  }

  render() {

    return (

      <div className="row ">
        <Header onClick={ this.loadContent }/>
        {/* <Body/> */}
        <div className="col-12 ">
        <div className="row">
        <LeftMenu onClick={ this.loadContent }/>
        {/*  <Core onClick={ this.loadContent } content={this.state.content} deviation={this.state.deviation} deviationId={this.state.deviationId}   />*/}
        <div className="col-10 core">
          <Switch>
            <Route path="/capas/new" component={NewCAPAContainer}/>
            <Route path="/capas/drafts/:_id" component={NewCAPAContainer}/>
            <Route path="/capas/drafts/" component={Drafts}/>
            <Route path="/capas/list/:search?" component={CapasListContainer}/>
            <Route path="/capas/view/:_id" component={ViewCapa}/>
            <Route path="/capas/" component={Dashboard}/>
          </Switch>
        </div>
        </div>
        </div>
      </div>

    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();

return {
user: user,
};
}
)(MainLayout);
