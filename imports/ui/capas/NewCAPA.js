import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';

import { Link,Redirect } from 'react-router-dom';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css' // Import css
import queryString from 'query-string';

import DatePicker from 'react-date-picker';
import TimePicker from 'react-time-picker';
import Select, {Option, OptGroup} from 'rc-select';
import 'rc-select/assets/index.css';

import Alert from 'react-s-alert';


import {CapaDrafts} from '../../api/capaDrafts.js';
//import Login from './Login.js';
//import Modules from './Modules.js';


// import  ApprobatorModal  from './modals/ApprobatorModal';
import  AssignModal  from './modals/AssignModal';
// import  MissingInfoModal  from './modals/MissingInfoModal';
// import  DiscardDeviationModal  from './modals/DiscardDeviationModal';


// App component - represents the whole app

export  class NewCAPA extends Component {


  constructor(props){
    super(props);
    //console.log(Meteor.user());
    this.draft = {
      _id:'',
      title : '',
      creationDate : new Date(),
      type : '',
      source : '',
      sourceId : '',
      plannedDate : '',
      description : '',
      state : '',
      user: '',
      approbatorId : '',
      implementation : '',
    }

    //console.log(props);
    this.state =  {...this.draft};

    this.save = this.save.bind(this);
    this.delete = this.delete.bind(this);
    this.sendForApproval = this.sendForApproval.bind(this);
    this.sendForImplementation = this.sendForImplementation.bind(this);
    this.requestInfo = this.requestInfo.bind(this);
    this.discardDeviation = this.discardDeviation.bind(this);
    //console.log(this.props.draft);
  }

  componentDidMount() {
    const parsed = queryString.parse(location.search);
    //console.log(!!parsed);
    if(parsed != {}){
      this.setState({source:parsed.source,sourceId:parsed.sourceId});
    }
  }
  static getDerivedStateFromProps(props, current_state) {
    if (props.draft && current_state._id !== props.draft._id) {
      const new_state = {... props.draft};
      //console.log(new_state);
      //this.setState(new_state);
      return new_state;
      // return {
      //   title : props.draft.title
      // };
      //console.log(props.draft);
    }
    return null;
  }
  // componentWillReceiveProps(nextProps) {
  //
  //   if(nextProps.draft != '' )
  //   {
  //     this.setState(nextProps.draft)
  //   }
  //   else{
  //     this.setState(this.draft);
  //   }
  //   //console.log(nextProps.draft);
  // }



  save(page){
    Meteor.call('capaDrafts.insert',this.state,(error,result)=>{
      if(result){
        Alert.info('CAPA saved.');
        this.setState({_id:result });
      }
    })
  }

  delete(page){
    confirmAlert({
      title: 'Confirm to delete',
      message: 'Are you sure to do this.',
      buttons: [
        {
          label: 'Yes',
          onClick: () => {
            Meteor.call('capaDrafts.delete',this.state,(error,result)=>{
              if(result){
                Alert.success('CAPA deleted successfully.');
                this.props.history.push("/capas/drafts/");
              }else {
                Alert.warning('Operation failed.');
              }
            })
          }
        },
        {
          label: 'No',
          onClick: () => Alert.info('Operation canceled.')
        }
      ]
    })

  }

  sendForApproval(){
    //console.log(this.state.approbator);
    Meteor.call('draftDeviation.sendForApproval',this.state,(error,result)=>{
      if(result){
        Alert.success('Deviation is waiting for Approval.');
        //this.setState({_id:result})
        Meteor.call('draftDeviation.sendApprovalEmail',this.state.approbation.approbatorId,(err,res)=>{
          if(res)
          Alert.info('Email sent to "'+res+'" for Approval.');
        })
        this.props.onClick('sent'); // navigate to sent deviations interface
      }
    })


  }

  sendForImplementation(){
    //console.log(this.state.approbator);
    Meteor.call('capaPlanning.sendForImplementation',this.state,(error,result)=>{
      if(result){
        Alert.success('CAPA is waiting to be implemented.');
        //this.setState({_id:result})
        Meteor.call('capaPlanning.sendImplementationEmail',result,this.state.implementation.operatorId,(err,res)=>{
          if(res)
          Alert.info('Email sent to "'+res+'" for implementation.');
        })
        this.props.history.push('/capas/list'); // navigate to capa list interface
      }
    })


  }

  requestInfo(){

    Meteor.call('draftDeviation.requestInfo',this.state,(error,result)=>{
      if(result){
        Alert.success('Deviation is sent back for completion.');
        //this.setState({_id:result})
        Meteor.call('draftDeviation.sendRequestInfoEmail',this.state.user._id,(err,res)=>{
          if(res)
          Alert.info('Email sent to "'+res+'".');
        })
        this.props.onClick('approvals'); // navigate to deviations approval interface

      }
    })

  }

  discardDeviation(){
    Meteor.call('draftDeviation.discardDeviation',this.state,(err,res)=>{
      if(res){
        Alert.success('Deviation discarded.');
        this.props.onClick('approvals');
      }
    })
  }

  render() {

    if(this.state._id != '' && this.props.match.url != '/capas/drafts/'+this.state._id){
      return <Redirect to={'/capas/drafts/'+this.state._id} />
    }
    return (

      <div key={this.props.draft ? this.props.draft._id : 'init'} className="col-11 ">

          <div className="row p-4 border text-center mb-3">
              <div className="col-6   ">
              <label className="col-7">Creation date</label>
              <p className="col-12">
                {moment(this.state.creationDate).format('DD-MM-YYYY') }
              </p>
            </div>
            <div className="col-6   ">
              <label className="col-7">Creation time</label>
              <p className="col-12">
                {moment(this.state.creationDate).format('HH:mm') }
              </p>
            </div>
          </div>

        <div className="row"  >
          <div className="col-6 mb-3  ">
            <label className="col-12">CAPA title</label>
            <input
              className="col-12 ml-3"
              type = "text"
              value = {this.state.title}
              onChange={ event =>  this.setState({title : event.target.value}) }
            />
          </div>
          <div className="col-6  ">
            <label className="col-12" >Type</label>
            <Select
              className="col-12 "
              onSelect={type => this.setState({type})}
              value = {this.state.type}
            >
              <Option value="Corrective action">Corrective action</Option>
              <Option value="Preventive action">Preventive action</Option>
            </Select>
          </div>
          <div className="col-6  ">
            <label className="col-12" >Source</label>
            <Select
              className="col-12 "
              onSelect={source => this.setState({source})}
              value = {this.state.source}
            >
              <Option value="Deviation ">Deviation</Option>
              <Option value="Change control">Change control</Option>
              <Option value="...">...</Option>
            </Select>
          </div>
          <div className="col-6 mb-3  ">
            <label className="col-12 " >Source Id</label>
            <input
              type = "text"
              className="col-12 ml-3"
              onChange={event => this.setState({sourceId : event.target.value})}
              value = {this.state.sourceId}
            />
          </div>
          <div className="col-6  ">
            <label className="col-12" >Planned implementation date</label>
            <DatePicker
              className="col-12 "
              onChange={plannedDate => this.setState({plannedDate})}
              value={this.state.plannedDate}
              locale='FR'
            />
          </div>
          <div className="col-6 mb-3 ">
            <label className="col-12 ">Description</label>
            <textarea
              className="col-12 ml-3"
              type = "text"
              value = {this.state.description}
              onChange={event => this.setState({description: event.target.value})}
            />
          </div>


        </div>



        {this.state.state != 'Approval' ?
        <div className="row mb-5 mt-5">
          <div className="col-4  " >
            <button className="btn btn-success  col-12 p-3" data-toggle="modal" data-target="#assignModal">Assign CAPA</button>
          </div>
          <div className="col-4" >
            <button className="btn btn-primary col-12 p-3" onClick={this.save} >Save</button>
          </div>
          <div className="col-4" >
            <button className="btn btn-danger col-12 p-3" onClick={this.delete} >Delete</button>
          </div>
        </div>
        :
        <div className="row mb-5 mt-5 ">
          <div className="col-4" >
            <button className="btn btn-success col-12 p-3  " data-toggle="modal" data-target="#expertiseModal">Approve Deviation</button>
            </div>
          <div className="col-4" >
            <button className="btn btn-primary col-12 p-3  " data-toggle="modal" data-target="#missingInfoModal"  >Request more information</button>
            </div>
          <div className="col-4" >
            <button className="btn btn-danger col-12 p-3 " data-toggle="modal" data-target="#discardDeviationModal"  >Close Deviation</button>
            </div>
        </div>
        }

        {/* <!-- Modal --> */}


                <AssignModal onSelect={ operator => {
                  let implementation = { ...this.state.implementation };
                  implementation.operatorId = operator._id;
                  this.setState({implementation : implementation })}
                } onClick={ this.sendForImplementation }
                   />
                {/*
                  <ApprobatorModal onSelect={ approbator => {
                     let appro = { ...this.state.approbation };
                     appro.approbatorId = approbator._id;
                     this.setState({approbation : appro })}
                   } onClick={ this.sendForApproval } />
                   <MissingInfoModal onChange={ event => this.setState({requestInfo : event.target.value}) } onClick={ this.requestInfo } />
                <DiscardDeviationModal onChange={ event => this.setState({discardDev : event.target.value}) } onClick={ this.discardDeviation } />
                {/* <Select
                  className="col-12 "
                  onSelect={approbator => this.setState({approbator : JSON.parse(approbator)})}
                  value = {this.state.approbator.username}
                >
                  {
                    Meteor.users.find({}).fetch().map((user,index)=>{
                    return  <Option key={index} value={JSON.stringify(user)}>{user.username}</Option>

                    })
                  }
                </Select>  } */}


      </div>

    );

  }

}


export default NewCAPAContainer = withTracker( ({match}) => {
const user=Meteor.userId();
//console.log(match.params._id);

Meteor.subscribe('capaDrafts');
//Meteor.subscribe('users');
Meteor.subscribe('usersInRoles',['approve-deviation']);
//console.log(CapaDrafts.findOne({"_id" : new Mongo.ObjectID("4e63da9b491ee4c7d83f3242")}));
//console.log(Roles.getUsersInRole(['approve-deviation'],null).fetch());
draft=CapaDrafts.findOne({"_id" : new Mongo.ObjectID(match.params._id)});
//console.log(draft);
return {
draft,
// processes : Processes.find({}).fetch(),
// localities : Localities.find({}).fetch(),
// methods : Methods.find({}).fetch(),
// equipments : Equipments.find({}).fetch(),
// products : Products.find({}).fetch(),
};
}
)(NewCAPA);
