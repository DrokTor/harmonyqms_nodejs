import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';

import { Link } from 'react-router-dom';

//import Header from './Header.js';
//import Login from './Login.js';
//import Modules from './Modules.js';



// App component - represents the whole app

export  class LeftMenu extends Component {


  constructor(props){
    super(props);

    this.state = {
      page : 'modules',
    }

    this.loadPage = this.loadPage.bind(this);
  }

  loadPage(page){
    this.setState({page});
  }

  render() {

    return (

      <div className="col-2  ">
        <ul className="col-8  left_menu mt-5">
          <Link to="/capas/new" className=""><li><div className="fa fa-3x  fa-file-text" ></div></li></Link>
          <Link to="/capas/list" className=""><li><div className="fa fa-3x  fa-list-ul" ></div></li></Link>
          <Link to="/capas/dashboard" className=""><li><div className="fa fa-3x  fa-area-chart" ></div></li></Link>
        </ul>

      </div>

    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();

return {
user: user,
};
}
)(LeftMenu);
