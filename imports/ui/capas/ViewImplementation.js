import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';

// import DayPickerInput from 'react-day-picker/DayPickerInput';
// import 'react-day-picker/lib/style.css';
//import moment from 'moment';
import DatePicker from 'react-date-picker';
// import TimePicker from 'react-time-picker';
import Select, {Option, OptGroup} from 'rc-select';
import 'rc-select/assets/index.css';
import classNames from 'classnames';
import ReactLoading from 'react-loading';
import Alert from 'react-s-alert';

import {CapaImplementations} from '../../api/capaImplementations.js';

// App component - represents the whole app

export  class ViewImplementation extends Component {

  constructor(props){
    super(props);

    this.state = {
      implementationDate : '',
    //  reviewDate : '',
      description : '',
      operator : '',
    }

    //this.operator = '';

    this.save = this.save.bind(this);
    this.close = this.close.bind(this);
  }

  componentDidMount(){
      Meteor.call('user.findOne',this.props.capaImplementation.operatorId,(err,result)=>{
        if(result)
        {
          this.setState({operator : result.username});
          //this.operator =  result.username;
          //console.log(this.operator);
        }
      })

      //console.log(this.props.implementation);
      // this.props.implementation &&
      // this.setState({
      //   implementationDate: this.props.implementation.implementationDate,
      //   description: this.props.implementation.description,
      // })
  }

  static getDerivedStateFromProps(props, current_state) {
    if (props.implementation && current_state._id !== props.implementation._id) {
      const new_state = {... props.implementation};
      //console.log(new_state);
      //this.setState(new_state);
      return new_state;

    }
    return null;
  }

  save(){
    Meteor.call('capaImplementations.saveImplementation',this.props.capa_id,this.state,(err,result)=>{
      //console.log(result);
      if(result)
      {
        Alert.success('CAPA saved.');
        //this.setState({_id:result})
        // Meteor.call('capaPlanning.sendImplementationEmail',this.state.implementation.operatorId,(err,res)=>{
        //   if(res)
        //   Alert.info('Email sent to "'+res+'" for implementation.');
        // })
        //this.props.history.push('/capas/list'); // navigate to capa list interface

      }
    })
  }

  close(){
    Meteor.call('capaImplementations.saveImplementation',this.props.capa_id,this.state,true,(err,result)=>{
      //console.log(result);
      if(result)
      {
        Alert.success('CAPA closed.');
        //this.setState({_id:result})
        // Meteor.call('capaPlanning.sendImplementationEmail',this.state.implementation.operatorId,(err,res)=>{
        //   if(res)
        //   Alert.info('Email sent to "'+res+'" for implementation.');
        // })
        //this.props.history.push('/capas/list'); // navigate to capa list interface

      }
    })
  }

  render() {

    return (

      <div className="col-12 row  mt-3 ">

        {
          (/*!this.props.implementation &&*/ this.props.state == 'Planned')?
          <React.Fragment>
          <div className="col-6   mb-3">
            <label className="col-12" >Implementation date</label>
            <DatePicker
              className="col-12 "
              onChange={implementationDate => this.setState({implementationDate})}
              value={this.state.implementationDate}
              locale="fr-FR"
            />
          </div>
          {/* <div className="col-6  ">
            <label className="col-12" >Effectiveness review date</label>
            <DatePicker
              className="col-12 "
              onChange={reviewDate => this.setState({reviewDate})}
              value={this.state.reviewDate}
              locale="fr-FR"
            />
          </div> */}
          <div className="col-6 mb-3 ">
            <label className="col-12 ">Description</label>
            <textarea
              className="col-12 ml-3"
              type = "text"
              value = {this.state.description}
              onChange={event => this.setState({description: event.target.value})}
            />
          </div>

          <div className="col-12 row mb-5 mt-5">

            <div className="col-4 offset-2" >
              <button className="btn btn-success col-12 p-3" onClick={this.close} >Close CAPA</button>
            </div>
            <div className="col-4" >
              <button className="btn btn-primary col-12 p-3" onClick={this.save} >Save</button>
            </div>
          </div>
        </React.Fragment>
          :
          <React.Fragment>
          { !this.props.implementation ?
          <ReactLoading type={'bars'} color={'lightgray'} className="mx-auto" height={60} width={100} />
          :
          <React.Fragment>
            <div className="row p-3 col-12    ">
              <label className="col-4">Operator</label>
              <p className="col-8">
                {this.state.operator}
              </p>
            </div>
            <div className="row p-3  col-12  bg-light ">
              <label className="col-4" >Implementation date</label>
              <p className="col-8">
                {moment(this.props.implementation.implementationDate).format('DD-MM-YYYY')}
              </p>
            </div>

            <div className="row p-3  col-12  ">
              <label className="col-4" >Description</label>
              <p className="col-8">
                {this.props.implementation.description}
              </p>
            </div>
            {/* <div className="row p-3  col-12   bg-light ">
              <label className="col-4" >Effectiveness review date</label>
              <p className="col-8">
                {moment(this.props.implementation.reviewDate).format('DD-MM-YYYY')}
              </p>
            </div> */}


          </React.Fragment>
          }
        </React.Fragment>
           }


      </div>

    );

  }

}


export default ImplementationContainer = withTracker( ({capa}) => {
const user=Meteor.userId();

Meteor.subscribe('capaImplementations');

implementation = CapaImplementations.findOne({capa_id : capa._id});
//console.log(implementation);
return {
implementation,
capa_id: capa._id,
state: capa.state,
capaImplementation: capa.implementation,
};
}
)(ViewImplementation);
