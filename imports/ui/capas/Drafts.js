import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';

import {Link} from 'react-router-dom';

import {CapaDrafts} from '../../api/capaDrafts.js';
//import Login from './Login.js';
//import Modules from './Modules.js';



// App component - represents the whole app

export  class Drafts extends Component {


  constructor(props){
    super(props);

    this.state = {
      page : 'modules',
    }

   }


  render() {

    return (

      <div className="col-11   ">
        <div className="row">
          <h3>Drafts ({this.props.drafts.length})</h3>
          <table className="col-12  mt-5 ">
            <thead><tr><th className="col-3">Title</th><th className="col-3">Creation date</th><th className="col-3">Type</th><th className="col-3">Planification</th></tr></thead>
            <tbody>
          {
            (() => {return this.props.drafts.map((draft,index)=>{
              return <tr key={index} className="pointer" onClick={()=>{this.props.history.push("/capas/drafts/"+draft._id)}} >
                <td className="col-3">{draft.title}</td>
                <td className="col-3">{ moment(draft.creationDate).format('DD-MM-YYYY') } </td>
                <td className="col-3">{draft.type}</td>
                <td className="col-2">{ moment(draft.plannedDate).format('DD-MM-YYYY') } </td>
            </tr>
            })})()
          }
          </tbody>
          </table>
        </div>
      </div>

    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();

  Meteor.subscribe('capaDrafts');


return {
user: user,
drafts:  CapaDrafts.find({'user._id': Meteor.userId(),state: 'Draft'}).fetch(),
};
}
)(Drafts);
