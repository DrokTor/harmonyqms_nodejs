import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';


import Select, {Option, OptGroup} from 'rc-select';
import 'rc-select/assets/index.css';

//import Modules from './Modules.js';



// App component - represents the whole app

export  class AssignModal extends Component {


  constructor(props){
    super(props);

    this.state = {
      operator : '',
      operators : [],
    }

   }


   componentDidMount(){
     Meteor.call('role.getUsersInRole',['implement-capa'],(err,result)=>{
       !this.isCancelled && this.setState({operators: result});
     })
   }
   componentWillUnmount() {
    this.isCancelled = true;
    }
    
  render() {

    return (
      <div className="modal fade" id="assignModal" tabIndex="-1" role="dialog" aria-labelledby="assignModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="assignModalLabel">Assign to CAPA operator</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body mb-3">

              <div className="offset-1 col-9 mt-4">
                <label className="ml-3">Select CAPA operator</label>
              <Select
                className="col-12 "
                onSelect={ operator  => {
                  this.setState({operator : JSON.parse(operator)})
                  this.props.onSelect(JSON.parse(operator))
                  }
                  }
                value = {this.state.operator.username}
              >
                {
                  this.state.operators.map((user,index)=>{
                  return  <Option key={index} value={JSON.stringify(user)}>{user.username}</Option>

                  })
                }
              </Select>
              </div>
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={ this.props.onClick } >Send</button>
        </div>
      </div>
    </div>
    </div>
    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();


//Meteor.subscribe('usersInRoles',['launch-investigation']);
//console.log(Roles.getUsersInRole(['launch-investigation']).fetch());
//console.log(operators)

return {
user: user,
//experts : experts,
};
}
)(AssignModal);
