import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';

import DatePicker from 'react-date-picker';
import TimePicker from 'react-time-picker';
import Select, {Option, OptGroup} from 'rc-select';
import 'rc-select/assets/index.css';
import classNames from 'classnames';
import ReactLoading from 'react-loading';
import Alert from 'react-s-alert';

import {CapaReviews} from '../../api/capaReviews.js';

// App component - represents the whole app

export  class ViewEffectivenessReview extends Component {

  constructor(props){
    super(props);

    this.state = {
      type : '',
      reviewDate : '',
      nbrBatches : '',
      review : '',
      conclusion : '',

    }

    this.capaReviewer = '';

    this.close = this.close.bind(this);
    this.save = this.save.bind(this);
  }

  componentDidMount(){
      // Meteor.call('user.findOne',this.props.capaReview.reviewerId,(err,result)=>{
      //   if(result)
      //   {
      //     //this.setState({operator : result.username});
      //     this.capaReviewer =  result.username;
      //     //console.log(this.operator);
      //   }
      // })

  }

  static getDerivedStateFromProps(props, current_state) {
    if (props.review && current_state._id !== props.review._id) {
      const new_state = {... props.review};
      //console.log(new_state);
      //this.setState(new_state);
      return new_state;

    }
    return null;
  }

  save(){
    Meteor.call('capaReviews.saveReview',this.props.capa_id,this.state,(err,result)=>{
      //console.log(result);
      if(result)
      {
        Alert.success('CAPA review saved.');
        //this.setState({_id:result})
        // Meteor.call('capaPlanning.sendImplementationEmail',this.state.implementation.operatorId,(err,res)=>{
        //   if(res)
        //   Alert.info('Email sent to "'+res+'" for implementation.');
        // })
        //this.props.history.push('/capas/list'); // navigate to capa list interface

      }
    })
  }

  close(){
    Meteor.call('capaReviews.saveReview',this.props.capa_id,this.state,true,(err,result)=>{
      //console.log(result);
      if(result)
      {
        Alert.success('CAPA review closed.');
        //this.setState({_id:result})
        // Meteor.call('capaPlanning.sendImplementationEmail',this.state.implementation.operatorId,(err,res)=>{
        //   if(res)
        //   Alert.info('Email sent to "'+res+'" for implementation.');
        // })
        //this.props.history.push('/capas/list'); // navigate to capa list interface

      }
    })
  }

  render() {

    return (

      <div className={"col-12 "+(this.props.state!="Effectiveness review"? " pt-5":"")}>

        {
          (/*!this.props.review &&*/ this.props.state == 'Implemented')?
          <React.Fragment>
          <div className="row mb-3">
          <div className="col-6  ">
            <label className="col-12" >Review type</label>
            <Select
              className="col-12 "
              onSelect={type => this.setState({type})}
              value = {this.state.type}
            >
              <Option value="Time period">Time period</Option>
              <Option value="Number of batches">Number of batches</Option>
            </Select>
          </div>
          {(this.state.type=="Time period") &&
          <div className="col-6  ">
            <label className="col-12" >Review date</label>
            <DatePicker
              className="col-12 "
              onChange={reviewDate => this.setState({reviewDate})}
              value={this.state.reviewDate}
              locale='FR'
            />
          </div>}
          {(this.state.type=="Number of batches") &&
          <div className="col-6  ">
            <label className="col-12" >Review after number of batches</label>
            <input
              type="number"
              className="col-12 ml-3"
              onChange={event => this.setState({nbrBatches : event.target.value})}
              value={this.state.nbrBatches}
            />
          </div>}
          </div>
          <div className="row">
            <div className="col-6 mb-3 ">
              <label className="col-12 ">Review</label>
              <textarea
                className="col-12 ml-3"
                type = "text"
                value = {this.state.review}
                onChange={event => this.setState({review: event.target.value})}
              />
            </div>
            <div className="col-6 mb-3 ">
              <label className="col-12 ">Review conclusion</label>
              <textarea
                className="col-12 ml-3"
                type = "text"
                value = {this.state.conclusion}
                onChange={event => this.setState({conclusion: event.target.value})}
              />
            </div>
          </div>


          <div className="row mb-5 mt-5">

            <div className="col-4 offset-2" >
              <button className="btn btn-success col-12 p-3" onClick={this.close} >Close Review</button>
            </div>
            <div className="col-4" >
              <button className="btn btn-primary col-12 p-3" onClick={this.save} >Save</button>
            </div>
          </div>
        </React.Fragment>
          :
          <React.Fragment>
          { !this.props.review ?
          <ReactLoading type={'bars'} color={'lightgray'} className="mx-auto" height={60} width={100} />
          :
          <React.Fragment>
            <div className="row p-3 col-12    ">
              <label className="col-4">Reviewer</label>
              <p className="col-8">
                {this.props.review.reviewer.username}
              </p>
            </div>
            <div className="row p-3  col-12  bg-light ">
              <label className="col-4" >Review type</label>
              <p className="col-8">
                {this.props.review.type}
              </p>
            </div>
            {this.props.review.type=="Time period" &&
            <div className="row p-3  col-12    ">
              <label className="col-4" >Review date</label>
              <p className="col-8">
                {moment(this.props.review.reviewDate).format('DD-MM-YYYY')}
              </p>
            </div>
            }
            {this.props.review.type=="Number of batches" &&
            <div className="row p-3  col-12   ">
              <label className="col-4" >Review after number of batches</label>
              <p className="col-8">
                {this.props.review.nbrBatches}
              </p>
            </div>
            }
            <div className="row p-3  col-12 bg-light ">
              <label className="col-4" >Review</label>
              <p className="col-8">
                {this.props.review.review}
              </p>
            </div>
            <div className="row p-3  col-12   ">
              <label className="col-4" >Review conclusion</label>
              <p className="col-8">
                {this.props.review.conclusion}
              </p>
            </div>


          </React.Fragment>
          }
        </React.Fragment>
           }


      </div>

    );

  }

}


export default ReviewsContainer = withTracker( ({capa}) => {
const user=Meteor.userId();

Meteor.subscribe('capaReviews');

review = CapaReviews.findOne({capa_id : capa._id});
//console.log(review);

return {
review,
capa_id: capa._id,
state: capa.state,
//capaImplementation: capa.implementation,
};
}
)(ViewEffectivenessReview);
