import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withTracker } from 'meteor/react-meteor-data';

//import moment from 'momentjs/moment';
import ReactLoading from 'react-loading';



// Client component - represents a single machine item
export class Logout extends Component {

  constructor(props){
    super(props);

    this.state = {
      username: '',
      password: '',
    }
    this.logout = this.logout.bind(this)
    this.loggingIn = this.loggingIn.bind(this)
  }

  logout(){
    Meteor.logout()
  }

  loggingIn()
  {
    return Meteor.loggingIn()
  }
  render() {

    return (
       <div  className="pointer offset-1">
            <span onClick={this.logout}>Logout ({this.props.user && this.props.user.username})</span>
       </div>
    );
  }
}

export default withTracker( () => {
const user=Meteor.user();

return {
  loggingIn: Meteor.loggingIn(),
  user: user,
  }

} )(Logout)
