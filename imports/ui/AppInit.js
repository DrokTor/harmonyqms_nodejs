import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';

import Alert from 'react-s-alert';
import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/slide.css';

import App from './App.js';
import Header from './Header.js';
import Login from './Login.js';

import { BrowserRouter } from 'react-router-dom'
// App component - represents the whole app

export  class AppInit extends Component {





  render() {

    return (
        <div>
        {( () => {
          return (this.props.user) ?  <App /> : <Login />;
        })()}
        <Alert stack={{limit: 3}} effect='slide'  position='bottom-right' timeout={5000} />
        </div>
    );

  }

}


export default withTracker( () => {
const user=Meteor.userId();

return {
user: user,
};
}
)(AppInit);
