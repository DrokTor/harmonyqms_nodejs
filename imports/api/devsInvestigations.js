import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
import {DevsDeclarations} from './devsDeclarations.js';

export const DevsInvestigations = new Mongo.Collection('devsInvestigations');

//console.log(DraftDeviations.insert);
if (Meteor.isServer) {

Meteor.methods({
  //receive request

  'devsInvestigations.sendForInvestigation'(investigation,devId,save=false) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    //console.log(investigation);
    if(investigation._id == '')
    {
      const id = new Meteor.Collection.ObjectID();
      investigation._id = id;
      investigation.investigation.creationDate = new Date();
      investigation.investigation.qualityExpert = Meteor.userId();
      investigation.deviation_id = devId;
    }

    //console.log(investigation);
    const w = DevsInvestigations.update({_id : investigation._id},investigation,{upsert : true});
    Meteor.call('devsDeclarations.setLevel',devId,investigation.preliminaryLevel.level);
    Meteor.call('devsDeclarations.setInvestigation',devId,investigation.investigation);
    //console.log(investigation._id);
    if(w){
      const record = {
      module : 'Deviation',
      info : save? 'Investigation saved by <'+Meteor.user().username+'>.':'Investigation launched by <'+Meteor.user().username+'>.',
      link : '/deviations/view/'+devId,
      }
      Meteor.call('logs.write',record);
    }
    return investigation._id;
  },
  'devsInvestigations.closeInvestigation'(investigation) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    //console.log(investigation);
    // if(investigation._id == '')
    // {
    //   const id = new Meteor.Collection.ObjectID();
    //   investigation._id = id;
    //   investigation.investigation.creationDate = new Date();
    //   investigation.investigation.qualityExpert = Meteor.userId();
    // }



    const w = DevsInvestigations.update({_id : investigation._id},investigation,{upsert : true});
    Meteor.call('devsDeclarations.setLevel',investigation.deviation_id,investigation.finalLevel.level);

    if(w){
      const record = {
      module : 'Deviation',
      info :  'Investigation closed by <'+Meteor.user().username+'>.',
      link : '/deviations/view/'+investigation.deviation_id,
      }
      Meteor.call('logs.write',record);
    }
    //console.log(investigation._id);
    return investigation._id;
  },
  'devsInvestigations.skipInvestigation'(devId,invest) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    let investigation = {};
    if(invest._id == '')
    {
      const id = new Meteor.Collection.ObjectID();
      investigation._id = id;
    }
      investigation.skipped = {skipDate : new Date()};
      investigation.skipped.skippedBy = Meteor.userId();
      investigation.skipped.reason = invest.reason;
      investigation.finalLevel = invest.finalLevel;
      investigation.deviation_id = devId;

    //console.log(investigation);
    const w = DevsInvestigations.update({deviation_id : devId},investigation,{upsert : true});
    Meteor.call('devsDeclarations.setLevel',devId,investigation.finalLevel.level);

    if(w){
      const record = {
      module : 'Deviation',
      info : 'Investigation skipped by <'+Meteor.user().username+'>, reason : <'+invest.reason+'> .',
      link : '/deviations/view/'+devId,
    }
    Meteor.call('logs.write',record);
    }
    //console.log(investigation._id);
    return true;
  },
  'devsInvestigations.sendInvestigatorEmail'(id,investigatorId) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    const investigator = Meteor.users.findOne({_id: investigatorId});

    //console.log(investigator);
    const from = 'Deviation <info@hamronyQMS.com>',
    to = investigator.emails[0].address,
    subject = 'Deviation investigation',
    text = 'A new deviation is waiting for you to investigate.';

    try {

      Email.send({from,to,subject,text});

      const record = {
      module : 'Deviation',
      info : 'Deviation sent to <'+to+'> for investigation.',
      link : '/deviations/view/'+id,
      }
      Meteor.call('logs.write',record);

    } catch (e) {
      console.log(e.reason);
      return false;
    } finally {

    }
    //console.log(deviation._id);
    //console.log(res);
    return investigator.username;
  },
  'devsInvestigations.checkExpert'(expertId) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    const expert = Meteor.users.findOne({_id: expertId});

    //console.log(expert);
    const from = 'Deviation <info@hamronyQMS.com>',
    to = expert.emails[0].address,
    subject = 'Investigation checking',
    text = 'An investigation is waiting for you to be checked and closed.';

    try {

      Email.send({from,to,subject,text});
    } catch (e) {
      console.log(e.reason);
      return false;
    } finally {

    }
    //console.log(deviation._id);
    //console.log(res);
    return expert.username;
  },




});


  // This code only runs on the server
  Meteor.publish('devsInvestigations', function (devId) {
    //console.log(id);
    //console.log(DevsInvestigations.find({deviation_id : id})).fetch();
    //return DevsInvestigations.find({deviation_id : id || null});
    return devId ? DevsInvestigations.find({deviation_id :devId}) : null;
  });

  // Meteor.publish('approveDeviations', function approveDeviationsPublication() {
  //
  //   if(Roles.userIsInRole(this.userId,['approve-deviation']))
  //   {
  //     return DevsDeclarations.find({"approbation.approbatorId" : Meteor.userId()});
  //   }else {
  //     return this.ready();
  //   }
  // });


}
