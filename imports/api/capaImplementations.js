import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const CapaImplementations = new Mongo.Collection('capaImplementations');

//console.log(DraftDeviations.insert);
if (Meteor.isServer) {

Meteor.methods({
  //receive request

  'capaImplementations.saveImplementation'(capa_id,implementation,close = false) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    // if(!implementation._id )
    // {
    //   const id = new Meteor.Collection.ObjectID();
    //   implementation._id = id;
    // }
    //console.log(implementation);
    delete implementation.operator;
    implementation.dateClosed = new Date();
    implementation.capa_id = capa_id;
    if(close)
    {
       Meteor.call('capaPlanning.setState',capa_id,'Implemented');
       Meteor.call('capaPlanning.setDuration',capa_id,implementation);
     }
    const w = CapaImplementations.update({capa_id},implementation, {upsert : true});
    if(w){
      const record = {
        module : 'CAPA',
        info : close? 'CAPA implemented.':'CAPA implementation saved.',
        link : '/capas/view/'+capa_id,
      }
      Meteor.call('logs.write',record);
    }
    //console.log(w);
    return w;
  },
  'capaImplementations.sendImplementationEmail'(expertId) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    const expert = Meteor.users.findOne({_id: expertId});

    //console.log(expert);
    const from = 'CAPA <info@hamronyQMS.com>',
    to = expert.emails[0].address,
    subject = 'CAPA implementation',
    text = 'A new CAPA is waiting for you to be implemented.';

    try {

      Email.send({from,to,subject,text});
    } catch (e) {
      console.log(e.reason);
      return false;
    } finally {

    }
    //console.log(deviation._id);
    //console.log(res);
    return expert.username;
  },




});


  // This code only runs on the server
  Meteor.publish('capaImplementations', function (_id=null) {
    //console.log(CapaImplementations.find({userId : Meteor.userId(), state : {$in : ['Draft','MissingInfo'] }}).fetch());
  //console.log(_id);
    return _id ? CapaImplementations.find({_id: _id}, {sort : {creationDate : -1}})
          : CapaImplementations.find({}, {sort : {creationDate : -1}});
  });

  // Meteor.publish('capaImplementations', function () {
  //   //console.log(CapaImplementations.find({"expertise.expertId" : Meteor.userId(), state : 'Declaration'}).fetch());
  //   if(Roles.userIsInRole(this.userId,['implement-capa']))
  //   {
  //     return CapaImplementations.find({"implementation.operatorId" : Meteor.userId()});
  //   }else {
  //     return this.ready();
  //   }
  // });


}
