import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
import {DevsDeclarations} from './devsDeclarations.js';

export const DevsConclusions = new Mongo.Collection('devsConclusions');

//console.log(DraftDeviations.insert);
if (Meteor.isServer) {

Meteor.methods({
  //receive request

  'devsConclusions.launchConclusion'(devId) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    //console.log(investigation);
      let conclusion = {};
      const id = new Meteor.Collection.ObjectID();
      conclusion._id = id;
      conclusion.deviation_id = devId;



    DevsConclusions.insert(conclusion);
    //console.log(investigation._id);
    return conclusion._id;
  },
  'devsConclusions.saveConclusion'(conclusion,devId) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    //console.log(investigation);
    // if(conclusion._id == '')
    // {
    //   const id = new Meteor.Collection.ObjectID();
    //   conclusion._id = id;
    //   //conclusion.investigation.creationDate = new Date();
    //   //conclusion.investigation.qualityExpert = Meteor.userId();
    //   conclusion.deviation_id = devId;
    // }


    const w = DevsConclusions.update({_id : conclusion._id},conclusion); //,{upsert : true}
    //console.log(investigation._id);
    if(w){
      const record = {
      module : 'Deviation',
      info :  'Conclusion saved by <'+Meteor.user().username+'>.',
      link : '/deviations/view/'+devId,
      }
      Meteor.call('logs.write',record);
    }

    return conclusion._id;
  },
  'devsConclusions.sign'(signatoryType,devId,comment) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    // if(conclusion._id == '')
    // {
    //   const id = new Meteor.Collection.ObjectID();
    //   conclusion._id = id;
    //   //conclusion.investigation.creationDate = new Date();
    //   //conclusion.investigation.qualityExpert = Meteor.userId();
    //   conclusion.deviation_id = devId;
    // }
    //console.log(signatoryType,devId);
    const signing =  {
      dateSignature : new Date(),
      signatoryId : Meteor.userId(),
      comment,

    };
    //console.log(signing);
    const res = DevsConclusions.update({deviation_id : devId},{$set : { ['signatories.'+signatoryType] : signing}}); //,{upsert : true}
    //let res = DevsConclusions.update({deviation_id : devId},{$set : { 'signatories' :   'XZsjGsmdGoAHdNei8' }}); //,{upsert : true}
    //console.log(signatoryType);
    //console.log(DevsConclusions.find({deviation_id : devId}).fetch());
    if(res){
      const record = {
      module : 'Deviation',
      info :  'Conclusion signed by <'+Meteor.user().username+'> as <'+signatoryType+'>.',
      link : '/deviations/view/'+devId,
      }
      Meteor.call('logs.write',record);
    }
    return res;
  },
  'devsConclusions.link'(type,capa) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    let conc = DevsConclusions.findOne({'deviation_id':capa.sourceId});
    let relations = conc.relations;
    if(relations){
      relations[type].push(capa._id);
    }else {
      relations = {};
      relations[type] = [];
      relations[type].push(capa._id);
    }
    console.log(capa._id);
    //console.log(signatoryType,devId);

    //console.log(signing);
    const res = DevsConclusions.update({deviation_id : capa.sourceId},{$set : { relations : relations}}); //,{upsert : true}
    //let res = DevsConclusions.update({deviation_id : devId},{$set : { 'signatories' :   'XZsjGsmdGoAHdNei8' }}); //,{upsert : true}
    //console.log(signatoryType);
    //console.log(DevsConclusions.find({deviation_id : devId}).fetch());
    // if(res){
    //   const record = {
    //   module : 'Deviation',
    //   info :  'Conclusion signed by <'+Meteor.user().username+'> as <'+signatoryType+'>.',
    //   link : '/deviations/view/'+devId,
    //   }
    //   Meteor.call('logs.write',record);
    // }
    return res;
  },
  'devsConclusions.closeInvestigation'(investigation) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    //console.log(investigation);
    // if(investigation._id == '')
    // {
    //   const id = new Meteor.Collection.ObjectID();
    //   investigation._id = id;
    //   investigation.investigation.creationDate = new Date();
    //   investigation.investigation.qualityExpert = Meteor.userId();
    // }


    DevsConclusions.update({_id : investigation._id},investigation,{upsert : true});
    //console.log(investigation._id);
    return investigation._id;
  },
  'devsConclusions.sendInvestigatorEmail'(investigatorId) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    const investigator = Meteor.users.findOne({_id: investigatorId});

    //console.log(investigator);
    const from = 'Deviation <info@hamronyQMS.com>',
    to = investigator.emails[0].address,
    subject = 'Deviation investigation',
    text = 'A new deviation is waiting for you to investigate.';

    try {

      Email.send({from,to,subject,text});
    } catch (e) {
      console.log(e.reason);
      return false;
    } finally {

    }
    //console.log(deviation._id);
    //console.log(res);
    return investigator.username;
  },




});


  // This code only runs on the server
  Meteor.publish('devsConclusions', function (devId) {
    //console.log(devId);
    //console.log(DevsConclusions.findOne({deviation_id : devId}));
    //return DevsConclusions.find({deviation_id : id || null});
    return devId ? DevsConclusions.find({deviation_id :devId}) : null ;
  });

  // Meteor.publish('approveDeviations', function approveDeviationsPublication() {
  //
  //   if(Roles.userIsInRole(this.userId,['approve-deviation']))
  //   {
  //     return DevsDeclarations.find({"approbation.approbatorId" : Meteor.userId()});
  //   }else {
  //     return this.ready();
  //   }
  // });


}
