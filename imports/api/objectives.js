import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Objectives = new Mongo.Collection('objectives');


if (Meteor.isServer) {

Meteor.methods({

  'objectives.update'(indicator,obj) {

    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

     Objectives.update({indicator},{$set : {obj} });
    return true;
  },


});


  // This code only runs on the server
  Meteor.publish('objectives', function objectivesPublication() {

    return Objectives.find({});
  });
}
