import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const CapaDrafts = new Mongo.Collection('capaDrafts');

if (Meteor.isServer) {

Meteor.methods({
  //receive request
  'capaDrafts.insert'(capa) {
    // check(client,{
    //   id : String,
    //   name : String,
    //   ip : String,
    // })




    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    if(capa._id == '')
    {
      const id = new Meteor.Collection.ObjectID();
      capa._id = id;
      capa.user = Meteor.user();
    }
    capa.state!= 'MissingInfo' ?  capa.state = "Draft" : "";
    //console.log(capa._id);
    //console.log(id);
     CapaDrafts.update({_id : capa._id},capa, {upsert : true});

     const record = {
       module : 'CAPA',
       info : 'Draft saved.',
       link : '/capas/drafts/'+capa._id,
     }
     Meteor.call('logs.write',record);

    return capa._id;
  },
  'capaDrafts.delete'(capa) {
    // check(client,{
    //   id : String,
    //   name : String,
    //   ip : String,
    // })




    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    if(capa._id == '')
    {
      const id = new Meteor.Collection.ObjectID();
      capa._id = id;
      capa.user = Meteor.user();
    }
    capa.state!= 'MissingInfo' ?  capa.state = "Draft" : "";
    //console.log(capa._id);
    //console.log(id);
     res = CapaDrafts.remove({_id : capa._id});

     if(res){
       const record = {
         module : 'CAPA',
         info : 'Draft deleted.'
       }
       Meteor.call('logs.write',record);
     }
    return res;
  },
  'capaDrafts.sendForApproval'(deviation) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    if(deviation._id == '')
    {
      const id = new Meteor.Collection.ObjectID();
      deviation._id = id;
    }
    deviation.state = "Approval";
    deviation.approbation.dateSent = new Date();

    CapaDrafts.update({_id : deviation._id},deviation, {upsert : true});

    return true;
  },
  'capaDrafts.sendApprovalEmail'(approbatorId) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    const approbator = Meteor.users.findOne({_id: approbatorId});

    //console.log(approbator);
    const from = 'Deviation <info@hamronyQMS.com>',
    to = approbator.emails[0].address,
    subject = 'Deviation approval',
    text = 'A new deviation is waiting for your approval.';

    try {

      Email.send({from,to,subject,text});
    } catch (e) {
      console.log(e);
    } finally {

    }
    //console.log(deviation._id);
    //console.log(res);
    return approbator.username;
  },
  'capaDrafts.requestInfo'(deviation) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    // if(deviation._id == '')
    // {
    //   const id = new Meteor.Collection.ObjectID();
    //   deviation._id = id;
    // }
    deviation.state = "MissingInfo";

    CapaDrafts.update({_id : deviation._id},deviation, {upsert : true});

    return true;
  },
  'capaDrafts.sendRequestInfoEmail'(userId) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    //onsole.log(user.emails);
    const user = Meteor.users.findOne({_id: userId});
    //console.log(user);
    //console.log(user.emails);
    const from = 'Deviation <info@hamronyQMS.com>',
    to = user.emails[0].address,
    subject = 'Deviation incomplete',
    text = 'The Deviation you opened is missing information.';

    Email.send({from,to,subject,text});
    //console.log(deviation._id);
    //console.log(res);
    return user.username;
  },
  'capaDrafts.discardDeviation'(draft) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    draft.discardDate =  new Date() ;
    draft.state =  'Discarded' ;

    //console.log(investigation);
    CapaDrafts.update({_id : draft._id},draft,{upsert : true});

    //console.log(investigation._id);
    return true;
  },

});


  // This code only runs on the server
  Meteor.publish('capaDrafts', function capaDraftsPublication() {
    //console.log(CapaDrafts.find({userId : Meteor.userId(), state : {$in : ['Draft','MissingInfo'] }}).fetch());
    return CapaDrafts.find({'user._id' : Meteor.userId(), state : {$in : ['Draft','Approval','MissingInfo'] }}, {sort: {creationDate: -1}});
  });

  // Meteor.publish('approveDeviations', function approveDeviationsPublication() {
  //
  //   if(Roles.userIsInRole(this.userId,['approve-deviation']))
  //   {
  //     return CapaDrafts.find({"approbation.approbatorId" : Meteor.userId()}, {sort: {creationDate: -1}});
  //   }else {
  //     return this.ready();
  //   }
  // });


}
