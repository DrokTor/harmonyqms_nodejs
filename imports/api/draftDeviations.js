import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const DraftDeviations = new Mongo.Collection('draftDeviations');

if (Meteor.isServer) {

Meteor.methods({
  //receive request
  'draftDeviation.insert'(deviation) {
    // check(client,{
    //   id : String,
    //   name : String,
    //   ip : String,
    // })




    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    if(deviation._id == '')
    {
      const id = new Meteor.Collection.ObjectID();
      deviation._id = id;
    }
    deviation.state!= 'MissingInfo' ?  deviation.state = "Draft" : "";
    //console.log(deviation._id);
    //console.log(id);
     const w = DraftDeviations.update({_id : deviation._id},deviation, {upsert : true});

     if(w){
       const record = {
         module : 'Deviation',
         info : 'Deviation draft saved.',
         link : '/deviations/drafts/'+deviation._id,
       }
       Meteor.call('logs.write',record);
     }

    return deviation._id;
  },
  'draftDeviation.sendForApproval'(deviation) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    if(deviation._id == '')
    {
      const id = new Meteor.Collection.ObjectID();
      deviation._id = id;
    }
    deviation.state = "Approval";
    deviation.approbation.dateSent = new Date();

    DraftDeviations.update({_id : deviation._id},deviation, {upsert : true});

    return true;
  },
  'draftDeviation.sendApprovalEmail'(id,approbatorId) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    const approbator = Meteor.users.findOne({_id: approbatorId});

    //console.log(approbator);
    const from = 'Deviation <info@hamronyQMS.com>',
    to = approbator.emails[0].address,
    subject = 'Deviation approval',
    text = '<a href='+Meteor.absoluteUrl()+'deviations/drafts/'+id+'>A new deviation is waiting for your approval.</a>';


    const record = {
      module : 'Deviation',
      info : 'Deviation sent to <'+to+'> for approval.',
      link : '/deviations/drafts/'+id,
    }
    Meteor.call('logs.write',record);


    try {

      Email.send({from,to,subject,text});
    } catch (e) {
      console.log(e);
    } finally {

    }
    //console.log(deviation._id);
    //console.log(res);
    return approbator.username;
  },
  'draftDeviation.requestInfo'(deviation) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    // if(deviation._id == '')
    // {
    //   const id = new Meteor.Collection.ObjectID();
    //   deviation._id = id;
    // }
    deviation.state = "MissingInfo";

    DraftDeviations.update({_id : deviation._id},deviation, {upsert : true});


    return true;
  },
  'draftDeviation.sendRequestInfoEmail'(id,userId) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    //onsole.log(user.emails);
    const user = Meteor.users.findOne({_id: userId});
    //console.log(user);
    //console.log(user.emails);
    const from = 'Deviation <info@hamronyQMS.com>',
    to = user.emails[0].address,
    subject = 'Deviation incomplete',
    text = 'The Deviation you opened is missing information.';

    Email.send({from,to,subject,text});
    //console.log(deviation._id);
    //console.log(res);
    const record = {
      module : 'Deviation',
      info : 'More details request sent to <'+to+'> .',
      link : '/deviations/drafts/'+id,
    }
    Meteor.call('logs.write',record);

    return user.username;
  },
  'draftDeviation.discardDeviation'(draft) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    draft.discardDate =  new Date() ;
    draft.state =  'Discarded' ;

    //console.log(investigation);
    DraftDeviations.update({_id : draft._id},draft,{upsert : true});

    const record = {
      module : 'Deviation',
      info : 'Deviation discarded.',
      //link : '/deviations/drafts/'+draft._id,
    }
    Meteor.call('logs.write',record);

    //console.log(investigation._id);
    return true;
  },

});


  // This code only runs on the server
  Meteor.publish('draftDeviations', function draftDeviationsPublication() {
    //console.log(DraftDeviations.find({userId : Meteor.userId(), state : {$in : ['Draft','MissingInfo'] }}).fetch());
    return DraftDeviations.find({'user._id' : Meteor.userId(), state : {$in : ['Draft','Approval','MissingInfo'] }}, {sort: {creationDate: -1}});
  });

  Meteor.publish('approveDeviations', function approveDeviationsPublication() {

    if(Roles.userIsInRole(this.userId,['approve-deviation']))
    {
      return DraftDeviations.find({"approbation.approbatorId" : Meteor.userId()}, {sort: {creationDate: -1}});
    }else {
      return this.ready();
    }
  });


}
