import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const CapaPlanning = new Mongo.Collection('capaPlanning');

//console.log(DraftDeviations.insert);
if (Meteor.isServer) {

Meteor.methods({
  //receive request

  'capaPlanning.sendForImplementation'(capa) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    if(capa._id == '')
    {
      const id = new Meteor.Collection.ObjectID();
      capa._id = id;
    }
    capa.user = Meteor.user();
    capa.state = "Planned";
    capa.implementation.dateSent = new Date();

    Meteor.call('capaDrafts.delete',capa);
    delete capa._id;
    const w = CapaPlanning.insert(capa);

    if(w){
      const record = {
        module : 'CAPA',
        info : 'A new CAPA planned.',
        link : '/capas/view/'+capa._id,
      }
      Meteor.call('logs.write',record);
      capa._id = w;
      Meteor.call('devsConclusions.link','CAPA',capa);
    }

    return w;
  },
  'capaPlanning.sendImplementationEmail'(id,expertId) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    const expert = Meteor.users.findOne({_id: expertId});

    //console.log(expert);
    const from = 'CAPA <info@hamronyQMS.com>',
    to = expert.emails[0].address,
    subject = 'CAPA implementation',
    html = '<a href="'+Meteor.absoluteUrl()+'capas/view/'+id+'" >A new CAPA is waiting for you to be implemented.</a>';

    try {

      Email.send({from,to,subject,html});

      const record = {
          module : 'CAPA',
          info : 'Email sent to ( '+to+' ) for CAPA implementation.',
        }
      Meteor.call('logs.write',record);


    } catch (e) {
      console.log(e);
      return false;
    } finally {

    }
    //console.log(deviation._id);
    //console.log(res);
    return expert.username;
  },
  'capaPlanning.setState'(_id,state) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    //console.log(devId);
    const update = CapaPlanning.update({_id },{$set : { state }});
    //console.log(deviation._id);
    //console.log('update');
    return update;
  },
  'capaPlanning.setDuration'(_id,implementation ) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    //console.log(devId);
    const capa=CapaPlanning.findOne({_id});
    const now = new Date(),
        crD = moment(capa.creationDate).startOf('day'),
        clD = moment(implementation.implementationDate).startOf('day'),
        duration = clD.diff(crD,'days');


    // capa.implementation.implementationDate = implementationDate;
    // capa.implementation.duration = duration;

    const update = CapaPlanning.update({_id },{$set : {'implementation.implementationDate' : implementation.implementationDate, 'implementation.duration' : duration  }});
    //console.log(deviation._id);
    //console.log('update');
    return update;
  },

  'capaPlanning.setInvestigation'(devId,investigation) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    //console.log(devId);
    const update = CapaPlanning.update({_id : devId},{$set : { investigation }});
    //console.log(deviation._id);
    //console.log(update);
    return update;
  },



});


  // This code only runs on the server
  Meteor.publish('capaPlanning', function (devId=null) {
    //console.log(CapaPlanning.find({userId : Meteor.userId(), state : {$in : ['Draft','MissingInfo'] }}).fetch());
  //console.log(devId);
    return devId ? CapaPlanning.find({_id: devId}, {sort : {creationDate : -1}})
          : CapaPlanning.find({}, {sort : {creationDate : -1}});
  });

  Meteor.publish('capaWaitingImplementations', function () {
    //console.log(CapaPlanning.find({"expertise.expertId" : Meteor.userId(), state : 'Declaration'}).fetch());
    if(Roles.userIsInRole(this.userId,['implement-capa']))
    {
      return CapaPlanning.find({"implementation.operatorId" : Meteor.userId()});
    }else {
      return this.ready();
    }
  });


}
