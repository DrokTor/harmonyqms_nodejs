import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Equipments = new Mongo.Collection('equipment');


if (Meteor.isServer) {

Meteor.methods({
  //receive request
  'equipment.insert'(equipment) {
    // check(client,{
    //   id : String,
    //   name : String,
    //   ip : String,
    // })



    //console.log(equipment);
    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

     Equipments.insert({name : equipment});
    //return equipment._id;
  },
  'equipment.update'(equipment) {
    // check(client,{
    //   id : String,
    //   name : String,
    //   ip : String,
    // })




    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

     Equipments.update({_id: equipment._id},{name : equipment.name});
    //return equipment._id;
  },
  'equipment.remove'(equipmentID) {
    // check(client,{
    //   id : String,
    //   name : String,
    //   ip : String,
    // })




    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

     Equipments.remove({_id: equipmentID});
    //return equipment._id;
  },

});


  // This code only runs on the server
  Meteor.publish('equipment', function equipmentPublication() {

    return Equipments.find({});
  });
}
