import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Products = new Mongo.Collection('product');


if (Meteor.isServer) {

Meteor.methods({
  //receive request
  'product.insert'(product) {
    // check(client,{
    //   id : String,
    //   name : String,
    //   ip : String,
    // })



    //console.log(product);
    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

     Products.insert({name : product});
    //return product._id;
  },
  'product.update'(product) {
    // check(client,{
    //   id : String,
    //   name : String,
    //   ip : String,
    // })




    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

     Products.update({_id: product._id},{name : product.name});
    //return product._id;
  },
  'product.remove'(productID) {
    // check(client,{
    //   id : String,
    //   name : String,
    //   ip : String,
    // })




    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

     Products.remove({_id: productID});
    //return product._id;
  },

});


  // This code only runs on the server
  Meteor.publish('product', function productPublication() {

    return Products.find({});
  });
}
