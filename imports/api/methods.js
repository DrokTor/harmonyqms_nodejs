import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Methods = new Mongo.Collection('method');


if (Meteor.isServer) {

Meteor.methods({
  //receive request
  'method.insert'(method) {
    // check(client,{
    //   id : String,
    //   name : String,
    //   ip : String,
    // })



    //console.log(method);
    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

     Methods.insert({name : method});
    //return method._id;
  },
  'method.update'(method) {
    // check(client,{
    //   id : String,
    //   name : String,
    //   ip : String,
    // })




    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

     Methods.update({_id: method._id},{name : method.name});
    //return method._id;
  },
  'method.remove'(methodID) {
    // check(client,{
    //   id : String,
    //   name : String,
    //   ip : String,
    // })




    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

     Methods.remove({_id: methodID});
    //return method._id;
  },

});


  // This code only runs on the server
  Meteor.publish('method', function methodPublication() {

    return Methods.find({});
  });
}
