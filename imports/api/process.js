import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Processes = new Mongo.Collection('process');


if (Meteor.isServer) {

Meteor.methods({
  //receive request
  'process.insert'(process) {
    // check(client,{
    //   id : String,
    //   name : String,
    //   ip : String,
    // })



    //console.log(process);
    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

     Processes.insert({name : process});
    //return process._id;
  },
  'process.update'(process) {
    // check(client,{
    //   id : String,
    //   name : String,
    //   ip : String,
    // })




    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

     Processes.update({_id: process._id},{name : process.name});
    //return process._id;
  },
  'process.remove'(processID) {
    // check(client,{
    //   id : String,
    //   name : String,
    //   ip : String,
    // })




    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

     Processes.remove({_id: processID});
    //return process._id;
  },

});


  // This code only runs on the server
  Meteor.publish('process', function processPublication() {

    return Processes.find({});
  });
}
