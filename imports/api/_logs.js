import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
import { Accounts } from 'meteor/accounts-base';

export const Logs = new Mongo.Collection('_logs');


if (Meteor.isServer) {

Meteor.methods({
  //receive request
  'logs.write'(record) {
    //console.log(user);
    //console.log(logs);
    let log = {...record};
    log.user = Meteor.user();
    log.date = new Date();
    log.host = this.connection.clientAddress;

    Logs.insert(log);
    //console.log('logged!');
  },

})



  // This code only runs on the server

  Meteor.publish('logs', function (){
    return Logs.find({})
  })

}
