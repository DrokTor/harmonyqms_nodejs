import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
import { Accounts } from 'meteor/accounts-base';
//export const Products = new Mongo.Collection('product');

// roles = [
//   "access-deviation-module",
//   "create-deviation",
//   "approve-deviation",
//   "launch-investigation",
//   "investigate-deviation",
//   "close-deviation",
//   "view-declaration",
//   "view-investigation",
//   "view-conclusion",
//   "print-deviation",
//   "analyse-data",
//   "do-all",
//   "admin-all",
//
// ]
//
// _.each(roles,(role)=> {
//   Roles.createRole(role);
// });

// Roles.createRole("review-capa");
// Roles.createRole("print-capa");
// Roles.createRole("analyse-capa");
 
Meteor.methods({
  //receive request
  'role.set'(user,roles) {
    //console.log(user);
    //console.log(roles);
     const res= Roles.setUserRoles(user,roles);
     //console.log(res);
  },
  'role.getUsersInRole'(roles) {
    //console.log(user);
    //console.log(roles);
     return Roles.getUsersInRole(roles).fetch();
  }
})

if (Meteor.isServer) {

  //Roles.setUserRoles("5b27c2adbb4103fa9607db43",['do-all']);

  Meteor.methods({

  })
  // This code only runs on the server
  Meteor.publish(null, function (){
    return Meteor.roles.find({})
  })
}
