import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

import {ReactiveAggregate} from 'meteor/jcbernack:reactive-aggregate';

import { DevsDeclarations } from '../devsDeclarations.js';
import { DevsInvestigations } from '../devsInvestigations.js';
import { DevsConclusions } from '../devsConclusions.js';



//export const CategoriesCount = new Mongo.Collection('categoriesCount');

//export const ClientsImport = "Clients";

if (Meteor.isServer) {

  Meteor.publish('categoriesCount', function  () {

    ReactiveAggregate(this, DevsDeclarations, [
      {"$group":{"_id":"$type","count":{$sum:1}}}
    ], { clientCollection: "categoryAgg" });

    // ReactiveAggregate(this, DevsDeclarations, [
    //   {"$group":{"_id":"$level","count":{$sum:1}}}
    // ], { clientCollection: "levelAgg" });

    // ReactiveAggregate(this, DevsDeclarations, [
    //   {"$group":{"_id":"$planification","count":{$sum:1}}}
    // ], { clientCollection: "planificationAgg" });

    // ReactiveAggregate(this, DevsDeclarations, [
    //   {"$group":{"_id":"$process","count":{$sum:1}}}
    // ], { clientCollection: "processAgg" });




  });

  Meteor.publish('levelsCount', function  () {

    ReactiveAggregate(this, DevsDeclarations, [
      {"$group":{"_id":"$level","count":{$sum:1}}}
    ], { clientCollection: "levelAgg" });

  });

  Meteor.publish('equipmentsCount', function  () {

    ReactiveAggregate(this, DevsDeclarations, [
      {"$group":{"_id":"$equipment","count":{$sum:1}}}
    ], { clientCollection: "equipmentAgg" });

  });

  Meteor.publish('productsCount', function  () {

    ReactiveAggregate(this, DevsDeclarations, [
      {"$group":{"_id":"$product","count":{$sum:1}}}
    ], { clientCollection: "productAgg" });

  });

  Meteor.publish('localitiesCount', function  () {

    ReactiveAggregate(this, DevsDeclarations, [
      {"$group":{"_id":"$locality","count":{$sum:1}}}
    ], { clientCollection: "localityAgg" });

  });
  Meteor.publish('processCount', function  () {

    ReactiveAggregate(this, DevsDeclarations, [
      {"$group":{"_id":"$process","count":{$sum:1}}}
    ], { clientCollection: "processAgg" });

  });

  Meteor.publish('planificationsCount', function  () {

    ReactiveAggregate(this, DevsDeclarations, [
      {"$group":{"_id":"$planification","count":{$sum:1}}}
    ], { clientCollection: "planificationAgg" });

  });

  Meteor.publish('methodsCount', function  () {

    ReactiveAggregate(this, DevsInvestigations, [
      {$match : {method : {$ne : null} } },
      {"$group":{"_id":"$method","count":{$sum:1}}}
    ], { clientCollection: "methodAgg" });

  });

  Meteor.publish('ishikawaCount', function  () {
    ReactiveAggregate(this, DevsInvestigations, [
      //{ $match : { ishikawa : {$ne : null} } } ,
      { $unwind : "$ishikawa" } ,
      {"$group":{"_id":"$ishikawa","count":{$sum:1}}}
    ], { clientCollection: "ishikawaAgg" });
  });

  Meteor.publish('decisionCount', function  () {
    ReactiveAggregate(this, DevsConclusions, [
      //{$match : {decision : {$not : {$in : [{},[],null] }} }},
      {$project : { decision : "$decision.action"} }, // to be deleted ???
      {$unwind: "$decision"},
      {$group: {_id : "$decision", count: {$sum:1} } }
    ], { clientCollection: "decisionAgg" });
  });

  Meteor.publish('processAvgCls', function  () {
    ReactiveAggregate(this, DevsDeclarations, [
      {$match : {closing : {$ne : null}} },
      {$group: {_id:"$process", count: {$avg:"$closing.duration"} } }
    ], { clientCollection: "processAvgCls" });
  });

  Meteor.publish('equipmentAvgCls', function  () {
    ReactiveAggregate(this, DevsDeclarations, [
      {$match : {closing : {$ne : null}} },
      {$group: {_id:"$equipment", count: {$avg:"$closing.duration"} } }
    ], { clientCollection: "equipmentAvgCls" });
  });

  Meteor.publish('localityAvgCls', function  () {
    ReactiveAggregate(this, DevsDeclarations, [
      {$match : {closing : {$ne : null}} },
      {$group: {_id:"$locality", count: {$avg:"$closing.duration"} } }
    ], { clientCollection: "localityAvgCls" });
  });

  Meteor.publish('productAvgCls', function  () {
    ReactiveAggregate(this, DevsDeclarations, [
      {$match : {closing : {$ne : null}} },
      {$group: {_id:"$product", count: {$avg:"$closing.duration"} } }
    ], { clientCollection: "productAvgCls" });
  });


  //Annual

  Meteor.publish('deviationYAgg', function  () {
    ReactiveAggregate(this, DevsDeclarations, [
      {$project : {month : { $month: '$creationDate' }} },
      {$group : {_id : "$month" , count : {$sum : 1}  } },
      { $sort: { "_id": 1 } }
    ], { clientCollection: "deviationYAgg" });
  });

  Meteor.publish('levelYAgg', function  () {
    ReactiveAggregate(this, DevsDeclarations, [
      {$project : {month : { $month: '$creationDate' }, level:1} },
      {$group : {_id : {month:"$month", target:'$level'} , count : {$sum : 1}  } },
      {$project : {month : '$_id.month', target : '$_id.target', count : '$count', _id : {$concat : [{ "$substr":["$_id.month",0,1]},".","$_id.target"]} }},
      { $sort: { "month": 1 } },
    ], { clientCollection: "levelYAgg" });
  });

  Meteor.publish('processYAgg', function  () {
    ReactiveAggregate(this, DevsDeclarations, [
      {$project : {month : { $month: '$creationDate' }, process:1} },
      {$group : {_id : {month:"$month", target:'$process'} , count : {$sum : 1}  } },
      {$project : {month : '$_id.month', target : '$_id.target', count : '$count', _id : {$concat : [{ "$substr":["$_id.month",0,1]},".","$_id.target"]} }},
      { $sort: { "month": 1 } },
    ], { clientCollection: "processYAgg" });
  });

  Meteor.publish('localityYAgg', function  () {
    ReactiveAggregate(this, DevsDeclarations, [
      {$project : {month : { $month: '$creationDate' }, locality:1} },
      {$group : {_id : {month:"$month", target:'$locality'} , count : {$sum : 1}  } },
      {$project : {month : '$_id.month', target : '$_id.target', count : '$count', _id : {$concat : [{ "$substr":["$_id.month",0,1]},".","$_id.target"]} }},
      { $sort: { "month": 1 } },
    ], { clientCollection: "localityYAgg" });
  });

  Meteor.publish('equipmentYAgg', function  () {
    ReactiveAggregate(this, DevsDeclarations, [
      {$project : {month : { $month: '$creationDate' }, equipment:1} },
      {$group : {_id : {month:"$month", target:'$equipment'} , count : {$sum : 1}  } },
      {$project : {month : '$_id.month', target : '$_id.target', count : '$count', _id : {$concat : [{ "$substr":["$_id.month",0,1]},".","$_id.target"]} }},
      { $sort: { "month": 1 } },
    ], { clientCollection: "equipmentYAgg" });
  });

  Meteor.publish('productYAgg', function  () {
    ReactiveAggregate(this, DevsDeclarations, [
      {$project : {month : { $month: '$creationDate' }, product:1} },
      {$group : {_id : {month:"$month", target:'$product'} , count : {$sum : 1}  } },
      {$project : {month : '$_id.month', target : '$_id.target', count : '$count', _id : {$concat : [{ "$substr":["$_id.month",0,1]},".","$_id.target"]} }},
      { $sort: { "month": 1 } },
    ], { clientCollection: "productYAgg" });
  });

  Meteor.publish('typeYAgg', function  () {
    ReactiveAggregate(this, DevsDeclarations, [
      {$project : {month : { $month: '$creationDate' }, type:1} },
      {$group : {_id : {month:"$month", target:'$type'} , count : {$sum : 1}  } },
      {$project : {month : '$_id.month', target : '$_id.target', count : '$count', _id : {$concat : [{ "$substr":["$_id.month",0,1]},".","$_id.target"]} }},
      { $sort: { "month": 1 } },
    ], { clientCollection: "typeYAgg" });
  });

  Meteor.publish('methodYAgg', function  () {
    ReactiveAggregate(this, DevsInvestigations, [
      {$project : {month : { $month: '$investigation.creationDate' }, method:1} },
      {$unwind : "$method"},
      {$group : {_id : {month:"$month", target:'$method'} , count : {$sum : 1}  } },
      {$project : {month : '$_id.month', target : '$_id.target', count : '$count', _id : {$concat : [{ "$substr":["$_id.month",0,1]},".","$_id.target"]} }},
      { $sort: { "month": 1 } },
    ], { clientCollection: "methodYAgg" });
  });

  Meteor.publish('ishikawaYAgg', function  () {
    ReactiveAggregate(this, DevsInvestigations, [
      {$project : {month : { $month: '$investigation.creationDate' }, ishikawa:1} },
      {$unwind : "$ishikawa"},
      {$group : {_id : {month:"$month", target:'$ishikawa'} , count : {$sum : 1}  } },
      {$project : {month : '$_id.month', target : '$_id.target', count : '$count', _id : {$concat : [{ "$substr":["$_id.month",0,1]},".","$_id.target"]} }},
      { $sort: { "month": 1 } },
    ], { clientCollection: "ishikawaYAgg" });
  });

  Meteor.publish('decisionYAgg', function  () {
    ReactiveAggregate(this, DevsConclusions, [
      {$project : {month : { $month: '$signatories.expert.dateSignature' }, decision:"$decision.action"} },
      {$unwind : "$decision"},
      {$group : {_id : {month:"$month", target:'$decision'} , count : {$sum : 1}  } },
      {$project : {month : '$_id.month', target : '$_id.target', count : '$count', _id : {$concat : [{ "$substr":["$_id.month",0,1]},".","$_id.target"]} }},
      { $sort: { "month": 1 } },
    ], { clientCollection: "decisionYAgg" });
  });

  Meteor.publish('avgClosingYAgg', function  () {
    ReactiveAggregate(this, DevsDeclarations, [
      {$match : {closing :{$ne : null}} },
      {$project : {month : {$month : "$creationDate"}  , closing : "$closing.duration"}},
      {$group : {_id : '$month' , count : {$avg : "$closing"}}},
      { $sort: { "month": 1 } },
    ], { clientCollection: "avgClosingYAgg" });
  });

  Meteor.publish('avgClosingProcYAgg', function  () {
    ReactiveAggregate(this, DevsDeclarations, [
      {$match : {closing :{$ne : null}} },
      {$project : {month : {$month : "$creationDate"}  , process:1 , closing : "$closing.duration"}},
      {$group : {_id : { month : '$month', target: '$process' } , count : {$avg : "$closing"}}},
      {$project : {month : '$_id.month', target : '$_id.target', count : '$count', _id : {$concat : [{ "$substr":["$_id.month",0,1]},".","$_id.target"]} }},
      { $sort: { "month": 1 } },
    ], { clientCollection: "avgClosingProcYAgg" });
  });

  Meteor.publish('avgClosingProdYAgg', function  () {
    ReactiveAggregate(this, DevsDeclarations, [
      {$match : {closing :{$ne : null}} },
      {$project : {month : {$month : "$creationDate"}  , product:1 , closing : "$closing.duration"}},
      {$group : {_id : { month : '$month', target: '$product' } , count : {$avg : "$closing"}}},
      {$project : {month : '$_id.month', target : '$_id.target', count : '$count', _id : {$concat : [{ "$substr":["$_id.month",0,1]},".","$_id.target"]} }},
      { $sort: { "month": 1 } },
    ], { clientCollection: "avgClosingProdYAgg" });
  });

  Meteor.publish('avgClosingLocYAgg', function  () {
    ReactiveAggregate(this, DevsDeclarations, [
      {$match : {closing :{$ne : null}} },
      {$project : {month : {$month : "$creationDate"}  , locality:1 , closing : "$closing.duration"}},
      {$group : {_id : { month : '$month', target: '$locality' } , count : {$avg : "$closing"}}},
      {$project : {month : '$_id.month', target : '$_id.target', count : '$count', _id : {$concat : [{ "$substr":["$_id.month",0,1]},".","$_id.target"]} }},
      { $sort: { "month": 1 } },
    ], { clientCollection: "avgClosingLocYAgg" });
  });

  Meteor.publish('avgClosingEquipYAgg', function  () {
    ReactiveAggregate(this, DevsDeclarations, [
      {$match : {closing :{$ne : null}} },
      {$project : {month : {$month : "$creationDate"}  , equipment:1 , closing : "$closing.duration"}},
      {$group : {_id : { month : '$month', target: '$equipment' } , count : {$avg : "$closing"}}},
      {$project : {month : '$_id.month', target : '$_id.target', count : '$count', _id : {$concat : [{ "$substr":["$_id.month",0,1]},".","$_id.target"]} }},
      { $sort: { "month": 1 } },
    ], { clientCollection: "avgClosingEquipYAgg" });
  });

}
