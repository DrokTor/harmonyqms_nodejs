import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

import {ReactiveAggregate} from 'meteor/jcbernack:reactive-aggregate';

import { DevsDeclarations } from '../devsDeclarations.js';


export const LevelsCount = new Mongo.Collection('levelsCount');

//export const ClientsImport = "Clients";

if (Meteor.isServer) {


  Meteor.publish('levelsCount', function () {

    ReactiveAggregate(this, DevsDeclarations, [
      {"$group":{"_id":"$level","count":{$sum:1}}}
    ], { clientCollection: "levelAgg" });

  });
}
