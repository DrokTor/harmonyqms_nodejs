import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Localities = new Mongo.Collection('locality');


if (Meteor.isServer) {

Meteor.methods({
  //receive request
  'locality.insert'(locality) {
    // check(client,{
    //   id : String,
    //   name : String,
    //   ip : String,
    // })



    //console.log(locality);
    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

     Localities.insert({name : locality});
    //return locality._id;
  },
  'locality.update'(locality) {
    // check(client,{
    //   id : String,
    //   name : String,
    //   ip : String,
    // })




    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

     Localities.update({_id: locality._id},{name : locality.name});
    //return locality._id;
  },
  'locality.remove'(localityID) {
    // check(client,{
    //   id : String,
    //   name : String,
    //   ip : String,
    // })




    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

     Localities.remove({_id: localityID});
    //return locality._id;
  },

});


  // This code only runs on the server
  Meteor.publish('locality', function localityPublication() {

    return Localities.find({});
  });
}
