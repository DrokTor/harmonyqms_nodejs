import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
import {DraftDeviations} from './draftDeviations.js';

export const DevsDeclarations = new Mongo.Collection('devsDeclarations');

//console.log(DraftDeviations.insert);
if (Meteor.isServer) {

Meteor.methods({
  //receive request
  'devsDeclarations.insert'(deviation) {
    // check(client,{
    //   id : String,
    //   name : String,
    //   ip : String,
    // })




    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    if(deviation._id == '')
    {
      const id = new Meteor.Collection.ObjectID();
      deviation._id = id;
    }
    deviation.state!= 'MissingInfo' ?  deviation.state = "Draft" : "";
    //console.log(deviation._id);
    //console.log(id);
     const w = DevsDeclarations.update({_id : deviation._id},deviation, {upsert : true});
     if(w){
       const record = {
         module : 'Deviation',
         info : 'Deviation approved by <'+Meteor.users.findOne(deviation.approbation.approbatorId).username+'>.',
         link : '/deviations/view/'+deviation._id,
       }
       Meteor.call('logs.write',record);
      }
    return deviation._id;
  },


  'devsDeclarations.sendForExpertise'(deviation) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    if(deviation._id == '')
    {
      const id = new Meteor.Collection.ObjectID();
      deviation._id = id;
    }
    deviation.state = "Declaration";
    deviation.expertise.dateSent = new Date();

    DraftDeviations.remove({_id : deviation._id});
    delete deviation._id;
    const w = DevsDeclarations.insert(deviation);

    if(w){
      const record = {
        module : 'Deviation',
        info : 'Deviation approved by <'+Meteor.users.findOne(deviation.approbation.approbatorId).username+'>.',
        link : '/deviations/view/'+deviation._id,
      }
      Meteor.call('logs.write',record);
     }

    return true;
  },
  'devsDeclarations.sendExpertiseEmail'(id,expertId) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    const expert = Meteor.users.findOne({_id: expertId});

    //console.log(expert);
    const from = 'Deviation <info@hamronyQMS.com>',
    to = expert.emails[0].address,
    subject = 'Deviation expertise',
    text = 'A new deviation is waiting for your expertise.';

    try {

      Email.send({from,to,subject,text});

        const record = {
          module : 'Deviation',
          info : 'Deviation sent to <'+to+'> for expertise.',
          link : '/deviations/view/'+id,
        }
        Meteor.call('logs.write',record);

    } catch (e) {
      console.log(e.reason);
      return false;
    } finally {

    }
    //console.log(deviation._id);
    //console.log(res);
    return expert.username;
  },
  'devsDeclarations.setState'(devId,state) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    //console.log(devId);
    const update = DevsDeclarations.update({_id : devId},{$set : { state }});
    //console.log(deviation._id);
    console.log('update');
    return update;
  },
  'devsDeclarations.setLevel'(devId,level) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    //console.log(devId);
    const update = DevsDeclarations.update({_id : devId},{$set : { level }});
    //console.log(deviation._id);
    //console.log(update);
    return update;
  },
  'devsDeclarations.setInvestigation'(devId,investigation) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    //console.log(devId);
    const update = DevsDeclarations.update({_id : devId},{$set : { investigation }});
    //console.log(deviation._id);
    //console.log(update);
    return update;
  },
  'devsDeclarations.setClosing'(devId,creationDate) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    const now = new Date(),
        crD = moment(creationDate).startOf('day'),
        clD = moment(now).startOf('day'),
        duration = clD.diff(crD,'days');

    // console.log(crD);
    // console.log(clD);
    // console.log(duration);


    let closing = {};
    closing.closingDate = now;
    closing.duration = duration;
    //console.log(moment(closing.closingDate));
    DevsDeclarations.update({_id : devId},{$set : { closing }});
    //console.log('closing');
    return true;
  },



});


  // This code only runs on the server
  Meteor.publish('devsDeclarations', function (devId=null) {
    //console.log(DevsDeclarations.find({userId : Meteor.userId(), state : {$in : ['Draft','MissingInfo'] }}).fetch());
  //console.log(devId);
    return devId ? DevsDeclarations.find({_id: devId}, {sort : {creationDate : -1}})
          : DevsDeclarations.find({}, {sort : {creationDate : -1}});
  });

  Meteor.publish('expertises', function expertisesPublication() {
    //console.log(DevsDeclarations.find({"expertise.expertId" : Meteor.userId(), state : 'Declaration'}).fetch());
    if(Roles.userIsInRole(this.userId,['launch-investigation']))
    {
      return DevsDeclarations.find({"expertise.expertId" : Meteor.userId()});
    }else {
      return this.ready();
    }
  });


}
