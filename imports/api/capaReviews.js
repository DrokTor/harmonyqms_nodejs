import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const CapaReviews = new Mongo.Collection('capaReviews');

//console.log(DraftDeviations.insert);
if (Meteor.isServer) {

Meteor.methods({
  //receive request

  'capaReviews.saveReview'(capa_id,review,close = false) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    if(review._id == '')
    {
      const id = new Meteor.Collection.ObjectID();
      review._id = id;
    }

    review.dateClosed = new Date();
    review.capa_id = capa_id;
    review.reviewer = Meteor.user();
    close && Meteor.call('capaPlanning.setState',capa_id,'Effectiveness review');
    const w = CapaReviews.update({capa_id : capa_id},review, {upsert : true});

    if(w){
      const record = {
        module : 'CAPA',
        info : close? 'CAPA review done.':'CAPA review saved.',
        link : '/capas/view/'+capa_id,
      }
      Meteor.call('logs.write',record);
    }

    return w;
  },
  'capaReviews.sendImplementationEmail'(expertId) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    const expert = Meteor.users.findOne({_id: expertId});

    //console.log(expert);
    const from = 'CAPA <info@hamronyQMS.com>',
    to = expert.emails[0].address,
    subject = 'CAPA implementation',
    text = 'A new CAPA is waiting for you to be implemented.';

    try {

      Email.send({from,to,subject,text});
    } catch (e) {
      console.log(e.reason);
      return false;
    } finally {

    }
    //console.log(deviation._id);
    //console.log(res);
    return expert.username;
  },




});


  // This code only runs on the server
  Meteor.publish('capaReviews', function (_id=null) {
    //console.log(CapaReviews.find({userId : Meteor.userId(), state : {$in : ['Draft','MissingInfo'] }}).fetch());
  //console.log(_id);
    return _id ? CapaReviews.find({_id: _id}, {sort : {creationDate : -1}})
          : CapaReviews.find({}, {sort : {creationDate : -1}});
  });

  // Meteor.publish('capaReviews', function () {
  //   //console.log(CapaReviews.find({"expertise.expertId" : Meteor.userId(), state : 'Declaration'}).fetch());
  //   if(Roles.userIsInRole(this.userId,['implement-capa']))
  //   {
  //     return CapaReviews.find({"implementation.operatorId" : Meteor.userId()});
  //   }else {
  //     return this.ready();
  //   }
  // });


}
