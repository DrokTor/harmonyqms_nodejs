import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';


import AppInit from '../imports/ui/AppInit.js';



Meteor.startup(() => {

  render(<AppInit />, document.getElementById('render-target'));

});
